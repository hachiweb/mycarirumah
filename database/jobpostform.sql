-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2019 at 12:00 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myclickonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobpostform`
--

CREATE TABLE `jobpostform` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `job_title` text NOT NULL,
  `job_type` text NOT NULL,
  `role` text NOT NULL,
  `salary_min` float NOT NULL,
  `salary_max` float NOT NULL,
  `experience_min` int(11) NOT NULL,
  `experience_max` int(11) NOT NULL,
  `city` text NOT NULL,
  `locality` text NOT NULL,
  `description` text NOT NULL,
  `company_name` text NOT NULL,
  `email` text NOT NULL,
  `number` bigint(20) NOT NULL,
  `job_plan` text NOT NULL,
  `include_mobile` text NOT NULL,
  `include_link` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `posted_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `parent_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobpostform`
--
ALTER TABLE `jobpostform`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobpostform`
--
ALTER TABLE `jobpostform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
