-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2019 at 12:00 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myclickonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `adsales`
--

CREATE TABLE `adsales` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` text NOT NULL,
  `company` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_title` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `source` varchar(50) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_title`, `parent_id`, `source`, `is_active`, `created_at`) VALUES
(1, 'Cars & Bikes', 0, '0', 1, '2019-07-10 05:56:51'),
(2, 'Mobiles & Tablets', 0, '0', 1, '2019-07-10 05:56:51'),
(3, 'Electronics Appliances', 0, '0', 1, '2019-07-10 05:56:51'),
(4, 'Real Estate', 0, '0', 1, '2019-07-10 05:56:51'),
(5, 'Home & Lifestyle', 0, '0', 1, '2019-07-10 05:56:51'),
(6, 'Jobs', 0, '0', 1, '2019-07-10 05:56:51'),
(7, 'Education & Training', 0, '0', 1, '2019-07-10 05:56:51'),
(8, 'Entertainment', 0, '0', 1, '2019-07-10 05:56:51'),
(9, 'Pets & Pet Care', 0, '0', 1, '2019-07-10 05:56:51'),
(10, 'Community', 0, '0', 1, '2019-07-10 05:56:51'),
(11, 'Events', 0, '0', 1, '2019-07-10 05:56:51'),
(12, 'Matrimonial', 0, '0', 1, '2019-07-10 05:56:51'),
(13, 'Bikes & Scooters', 1, 'Cars & Bikes', 1, '2019-07-10 06:01:03'),
(14, 'Cars', 1, 'Cars & Bikes', 1, '2019-07-10 06:01:03'),
(15, 'Commercial Vehicles', 1, 'Cars & Bikes', 1, '2019-07-10 06:01:03'),
(16, 'Spare Parts - Accessories', 1, 'Cars & Bikes', 1, '2019-07-10 06:01:03'),
(17, 'Other Vehicles', 1, 'Cars & Bikes', 1, '2019-07-10 06:01:03'),
(18, 'Mobile Phones', 2, 'Mobiles & Tablets', 1, '2019-07-10 06:06:24'),
(19, 'Accessories', 2, 'Mobiles & Tablets', 1, '2019-07-10 06:06:24'),
(20, 'Tablets', 2, 'Mobiles & Tablets', 1, '2019-07-10 06:06:24'),
(21, 'Wearables', 2, 'Mobiles & Tablets', 1, '2019-07-10 06:06:24'),
(22, 'Home Appliances', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(23, 'Laptops & Accessories', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(24, 'Kitchen Appliances', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(25, 'Audio, Video & Gaming', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(26, 'Camera & Accessories', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(27, 'Other Devices', 3, 'Electronics Appliances', 1, '2019-07-10 06:30:41'),
(28, 'Refrigerators / Fridge', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(29, 'Washing Machine', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(30, 'Air Coolers', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(31, 'Air Conditioners / AC', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(32, 'Water Heaters / Geysers', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(33, 'Sewing Machines', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(34, 'Vacuum Cleaners', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(35, 'Ceiling Fans', 22, 'Home Appliances', 1, '2019-07-10 06:40:46'),
(36, 'Laptops', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(37, 'Desktop / Computers', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(38, 'Monitor', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(39, 'Scanners', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(40, 'Printers', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(41, 'Printer Ink & Toners', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(42, 'Routers', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(43, 'CPU', 23, 'Laptops & Accessories', 1, '2019-07-10 06:46:29'),
(44, 'Water Purifier', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(45, 'Microwave Ovens', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(46, 'Mixer/Grinder/Juicer', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(47, 'Induction Cook Tops', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(48, 'Gas Stove', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(49, 'Oven Toaster Griller', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(50, 'Electric Cookers', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(51, 'Food Processors', 24, 'Kitchen Appliances', 1, '2019-07-10 06:51:11'),
(52, 'TV', 25, 'Audio, Video & Gaming', 1, '2019-07-10 06:56:02'),
(53, 'Video Games - Consoles', 25, 'Audio, Video & Gaming', 1, '2019-07-10 06:56:02'),
(54, 'Music Systems - Home Theatre', 25, 'Audio, Video & Gaming', 1, '2019-07-10 06:56:02'),
(55, 'iPods, MP3 Players', 25, 'Audio, Video & Gaming', 1, '2019-07-10 06:56:02'),
(56, 'DVD players', 25, 'Audio, Video & Gaming', 1, '2019-07-10 06:56:02'),
(57, 'Cameras - Digicams', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(58, 'Digital SLR', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(59, 'Digital Point & Shoot Cameras', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(60, 'Camera Accessories', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(61, 'Camcorders', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(62, 'Film Cameras', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(63, 'Binoculars, Telescopes', 26, 'Camera & Accessories', 1, '2019-07-10 06:59:37'),
(64, 'Tools - Machinery - Industrial', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(65, 'Inverters, UPS & Generators', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(66, 'Fax, EPABX, Office Equipment', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(67, 'Security Equipment - Products', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(68, 'Office Supplies', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(69, 'Everything Else', 27, 'Other Devices', 1, '2019-07-10 07:04:20'),
(70, 'Commercial Property for Rent', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(71, 'Commercial Property for Sale', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(72, 'Flatmates', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(73, 'Houses - Apartments for Rent', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(74, 'Land - Plot For Sale', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(75, 'Paying - Guest Hostel', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(76, 'Residential - Builder floors For Rent', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(77, 'Residential - Builder floors For Sale', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(78, 'Service Apartments', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(79, 'Vacation Rentals - Timeshare', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(80, 'Villas/Bungalows for Rent', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(81, 'Villas/Bungalows for Sale', 4, 'Real Estate', 1, '2019-07-10 07:28:19'),
(82, 'Furniture & Decor', 5, 'Home & Lifestyle', 1, '2019-07-10 08:53:11'),
(83, 'Sports, Books & Hobbies', 5, 'Home & Lifestyle', 1, '2019-07-10 08:53:11'),
(84, 'Fashion', 5, 'Home & Lifestyle', 1, '2019-07-10 08:53:11'),
(85, 'Kids & Toys', 5, 'Home & Lifestyle', 1, '2019-07-10 08:53:11'),
(86, 'Misc', 5, 'Home & Lifestyle', 1, '2019-07-10 08:53:11'),
(87, 'Furniture for Home & Office', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(88, 'Home Decor- Furnishings', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(89, 'Household', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(90, 'Kitchenware', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(91, 'Antiques - Handicrafts', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(92, 'Paintings', 82, 'Furniture & Decor', 1, '2019-07-10 09:00:07'),
(95, 'Clothing - Garments', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(96, 'Watches', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(97, 'Jewellery', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(98, 'Bags - Luggage', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(99, 'Footwear', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(100, 'Fashion Accessories', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(101, 'Health - Beauty Products', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(102, 'Gifts Stationary', 84, 'Fashion', 1, '2019-07-10 09:08:03'),
(103, 'Full Time Jobs', 6, 'Jobs', 1, '2019-07-10 09:10:40'),
(104, 'Internships', 6, 'Jobs', 1, '2019-07-10 09:10:40'),
(105, 'Part Time Jobs', 6, 'Jobs', 1, '2019-07-10 09:10:40'),
(106, 'Work Abroad', 6, 'Jobs', 1, '2019-07-10 09:10:40'),
(107, 'Work From Home', 6, 'Jobs', 1, '2019-07-10 09:10:40'),
(108, 'Career Counseling', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(109, 'Certifications & Training', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(110, 'Competitive Exams Coaching', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(111, 'Hobby Classes', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(112, 'Play Schools - Creche', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(113, 'Schools & School Tuitions', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(114, 'Study Abroad Consultants', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(115, 'Text books & Study Material', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(116, 'Vocational Skill Training', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(117, 'Workshops', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(118, 'Career Counseling', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(119, 'Acting Schools', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(120, 'Actor - Model Portfolios', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(121, 'Art Directors - Editors', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(122, 'Fashion Designers - Stylists', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(123, 'Make Up - Hair - Films & TV', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(124, 'Modeling Agencies', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(125, 'Musicians', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(126, 'Photographers - Cameraman', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(127, 'Script Writers', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(128, 'Set Designers', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(129, 'Sound Engineers', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(130, 'Studios - Locations for hire', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(131, 'Other Entertainment', 8, 'Entertainment', 1, '2019-07-10 09:42:18'),
(132, 'Pet Adoption', 9, 'Pets & Pet Care', 1, '2019-07-10 09:45:24'),
(133, 'Pet Care - Accessories', 9, 'Pets & Pet Care', 1, '2019-07-10 09:45:24'),
(134, 'Pet Clinics', 9, 'Pets & Pet Care', 1, '2019-07-10 09:45:24'),
(135, 'Pet Foods', 9, 'Pets & Pet Care', 1, '2019-07-10 09:45:24'),
(136, 'Pet Training & Grooming', 9, 'Pets & Pet Care', 1, '2019-07-10 09:45:24'),
(137, 'Announcements', 10, 'Community', 1, '2019-07-10 09:50:57'),
(138, 'Charity - Donate - NGO', 10, 'Community', 1, '2019-07-10 09:50:57'),
(139, 'Lost - Found', 10, 'Community', 1, '2019-07-10 09:50:57'),
(140, 'Tender Notices', 10, 'Community', 1, '2019-07-10 09:50:57'),
(141, 'Event Management', 11, 'Events', 1, '2019-07-10 09:50:57'),
(142, 'Live Events', 11, 'Events', 1, '2019-07-10 09:50:57'),
(143, 'Brides', 12, 'Matrimonial', 1, '2019-07-10 09:50:57'),
(144, 'Grooms', 12, 'Matrimonial', 1, '2019-07-10 09:50:57'),
(145, 'Wedding Planners', 12, 'Matrimonial', 1, '2019-07-10 09:50:57'),
(146, 'Sport - Fitness Equipment', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(147, 'Bicycle & Accessories', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(148, 'Books - Magazines', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(149, 'Musical Instruments', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(150, 'Coins - Stamps', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(151, 'Collectibles', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(152, 'Music - Movies', 83, 'Sports, Books & Hobbies', 1, '2019-07-29 10:26:32'),
(153, 'Toys - Games', 85, 'Kids & Toys', 1, '2019-07-29 10:55:28'),
(154, 'Baby - Infant Products', 85, 'Kids & Toys', 1, '2019-07-29 10:55:28'),
(155, 'Kids Learning', 85, 'Kids & Toys', 1, '2019-07-29 10:55:28'),
(156, 'Cameras - Digicams', 86, 'Misc', 1, '2019-07-29 10:57:38'),
(157, 'Everything Else', 86, 'Misc', 1, '2019-07-29 10:57:38'),
(158, 'Entrance Exam Coaching', 7, 'Education & Training', 1, '2019-07-10 09:20:32'),
(159, 'Car Pool - Bike Ride', 10, 'Community', 1, '2019-07-10 09:50:57'),
(160, 'Select Category', 0, '0', 1, '2019-07-10 05:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(11) NOT NULL,
  `category` text NOT NULL,
  `name` text NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` text NOT NULL,
  `subject` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forbusiness`
--

CREATE TABLE `forbusiness` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `locality` text NOT NULL,
  `subject` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `indexalert`
--

CREATE TABLE `indexalert` (
  `id` int(11) NOT NULL,
  `category` text NOT NULL,
  `sub_category` text NOT NULL,
  `city` text NOT NULL,
  `locality` text NOT NULL,
  `email` text NOT NULL,
  `mobile` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indexalert`
--

INSERT INTO `indexalert` (`id`, `category`, `sub_category`, `city`, `locality`, `email`, `mobile`) VALUES
(1, 'Select Category *', 'Select Sub Category *', 'Select City *', 'Select Locality *', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jobpostform`
--

CREATE TABLE `jobpostform` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `job_title` text NOT NULL,
  `job_type` text NOT NULL,
  `role` text NOT NULL,
  `salary_min` float NOT NULL,
  `salary_max` float NOT NULL,
  `experience_min` int(11) NOT NULL,
  `experience_max` int(11) NOT NULL,
  `city` text NOT NULL,
  `locality` text NOT NULL,
  `description` text NOT NULL,
  `company_name` text NOT NULL,
  `email` text NOT NULL,
  `number` bigint(20) NOT NULL,
  `job_plan` text NOT NULL,
  `include_mobile` text NOT NULL,
  `include_link` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `posted_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `parent_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ad_plan_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_plan_month` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ad_plan_price` float(10,2) NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `item_price_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` bigint(20) NOT NULL,
  `card_exp_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_year` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `username`, `ad_plan_name`, `ad_plan_month`, `ad_plan_price`, `paid_amount`, `item_price_currency`, `paid_amount_currency`, `payment_status`, `txn_id`, `card_number`, `card_exp_month`, `card_exp_year`, `created`, `modified`) VALUES
(1, 'My Click Online', 'testhachiweb@gmail.com', '18 Ads Pack', ' 1 Month', 1800.00, '1800', 'USD', 'usd', 'succeeded', 'txn_1F9qKzIPCgq6Iwc6bhNHYhr5', 4242424242424242, '12', '2020', '2019-08-21 09:42:42', '2019-08-21 09:42:42'),
(2, 'My Click Online', 'testhachiweb@gmail.com', '20 Ads Pack', ' 1 Month', 2000.00, '2000', 'USD', 'usd', 'succeeded', 'txn_1F9qRNIPCgq6Iwc6rEvQnhVY', 4242424242424242, '12', '2020', '2019-08-21 09:49:18', '2019-08-21 09:49:18'),
(3, 'My Click Online', 'testhachiweb@gmail.com', '40 Ads Pack', ' 1 Month', 4000.00, '4000', 'USD', 'usd', 'succeeded', 'txn_1F9qamIPCgq6Iwc6yvfmxQAI', 4242424242424242, '12', '2020', '2019-08-21 09:59:01', '2019-08-21 09:59:01'),
(4, 'My Click Online', 'testhachiweb@gmail.com', '18 Ads Pack', ' 1 Month', 1800.00, '1800', 'USD', 'usd', 'succeeded', 'txn_1F9qcZIPCgq6Iwc6F2FUzLIc', 4242424242424242, '12', '2023', '2019-08-21 10:00:52', '2019-08-21 10:00:52'),
(5, 'My Click Online', 'testhachiweb@gmail.com', '30 Ads Pack', ' 1 Month', 3000.00, '3000', 'USD', 'usd', 'succeeded', 'txn_1F9qkfIPCgq6Iwc6Zo5nIkPz', 4242424242424242, '12', '2020', '2019-08-21 10:09:13', '2019-08-21 10:09:13'),
(6, 'My Click Online', 'testhachiweb@gmail.com', '30 Ads Pack', ' 1 Month', 3000.00, '3000', 'USD', 'usd', 'succeeded', 'txn_1F9qvwIPCgq6Iwc6ZuXmosM6', 4242424242424242, '12', '2020', '2019-08-21 10:20:53', '2019-08-21 10:20:53'),
(7, 'My Click Online', 'testhachiweb@gmail.com', '10 Ads Pack', ' 1 Month', 1000.00, '1000', 'USD', 'usd', 'succeeded', 'txn_1F9r1HIPCgq6Iwc6e9GWtPHU', 4242424242424242, '12', '2020', '2019-08-21 10:26:24', '2019-08-21 10:26:24');

-- --------------------------------------------------------

--
-- Table structure for table `postadform`
--

CREATE TABLE `postadform` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ad_category` varchar(200) NOT NULL,
  `condition` text NOT NULL,
  `title` text NOT NULL,
  `price` text NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `negotiate` varchar(4) NOT NULL,
  `number_privacy` varchar(4) NOT NULL,
  `email` text NOT NULL,
  `pincode` int(11) NOT NULL,
  `saler_type` text NOT NULL,
  `gst_number` varchar(25) NOT NULL,
  `product_material` text NOT NULL,
  `quantity` varchar(25) NOT NULL,
  `brand` text NOT NULL,
  `description` text NOT NULL,
  `imgname` varchar(200) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `posted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postfreeadform`
--

CREATE TABLE `postfreeadform` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `education` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `board` text NOT NULL,
  `subject` text NOT NULL,
  `standard` text NOT NULL,
  `delivery_mode` text NOT NULL,
  `fees` text NOT NULL,
  `institute_name` text NOT NULL,
  `institute_adderess` text NOT NULL,
  `institute_website` text NOT NULL,
  `discount_percentage` text NOT NULL,
  `duration` text NOT NULL,
  `enable_offer_zone` text NOT NULL,
  `locality` text NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `maintain_my_privacy` text NOT NULL,
  `send_me_email` text NOT NULL,
  `ad_plan` text NOT NULL,
  `imgname` varchar(200) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `posted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `premium_plans_table`
--

CREATE TABLE `premium_plans_table` (
  `plan_id` int(11) NOT NULL,
  `ad_plan_name` varchar(50) NOT NULL,
  `ad_plan_month` varchar(50) NOT NULL,
  `ad_plan_price` float(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `premium_plans_table`
--

INSERT INTO `premium_plans_table` (`plan_id`, `ad_plan_name`, `ad_plan_month`, `ad_plan_price`) VALUES
(1, '3 Ads Pack', '1 Month', 3.00),
(2, '5 Ads Pack', '1 Month', 5.00),
(3, '10 Ads Pack', '1 Month', 10.00),
(4, '15 Ads Pack', '1 Month', 15.00),
(5, '18 Ads Pack', '1 Month', 18.00),
(6, '20 Ads Pack', '1 Month', 20.00),
(7, '22 Ads Pack', '1 Month', 22.00),
(8, '30 Ads Pack', '1 Month', 30.00),
(9, '40 Ads Pack', '1 Month', 40.00);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `alias` text NOT NULL,
  `gender` text NOT NULL,
  `age` int(11) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `locality` varchar(200) NOT NULL,
  `house` varchar(200) NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `landmark` varchar(200) NOT NULL,
  `address_type` varchar(50) NOT NULL,
  `role` text DEFAULT NULL,
  `oauth_uid` bigint(20) NOT NULL,
  `oauth_provider` text NOT NULL,
  `verify` varchar(5) NOT NULL,
  `token` varchar(500) NOT NULL,
  `password` text NOT NULL,
  `picture` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `last_loggedin` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `phone`, `alias`, `gender`, `age`, `city`, `locality`, `house`, `pincode`, `landmark`, `address_type`, `role`, `oauth_uid`, `oauth_provider`, `verify`, `token`, `password`, `picture`, `is_active`, `last_loggedin`, `created_at`, `modified`) VALUES
(1, 'testhachiweb@gmail.com', NULL, 'HachiWeb Test Account', '', NULL, '', '', '', NULL, '', '', NULL, 9223372036854775807, 'Google', 'true', '', '', 'https://lh3.googleusercontent.com/a-/AAuE7mBzqfX8sjabsXMoXx2n2Hv0FK1w4Swb5FS-aOsd=s96-c', 1, NULL, '2019-08-29 17:54:51', '2019-08-29 12:24:51'),
(2, 'xowiwe@mailinator.net', '9745632140', 'Jin Mason', '', NULL, '', '', '', NULL, '', '', 'Admin', 0, 'Custom', 'true', 'd14f2e21723a5c984e770ede2ea50074', '827ccb0eea8a706c4c34a16891f84e7b', '', 1, NULL, '2019-09-08 09:23:10', '2019-09-08 03:53:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adsales`
--
ALTER TABLE `adsales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forbusiness`
--
ALTER TABLE `forbusiness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indexalert`
--
ALTER TABLE `indexalert`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobpostform`
--
ALTER TABLE `jobpostform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postadform`
--
ALTER TABLE `postadform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postfreeadform`
--
ALTER TABLE `postfreeadform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `premium_plans_table`
--
ALTER TABLE `premium_plans_table`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adsales`
--
ALTER TABLE `adsales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forbusiness`
--
ALTER TABLE `forbusiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indexalert`
--
ALTER TABLE `indexalert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobpostform`
--
ALTER TABLE `jobpostform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `postadform`
--
ALTER TABLE `postadform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postfreeadform`
--
ALTER TABLE `postfreeadform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `premium_plans_table`
--
ALTER TABLE `premium_plans_table`
  MODIFY `plan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
