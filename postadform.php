<?php
include 'header.php';
// echo "<pre>";
// print_r($_GET);
// echo "</pre>";
// exit();
?>
<title>Post Ad Form</title>
      <form action="sub_postadform.php" method="post" enctype="multipart/form-data">
         <section class="container border mt-2 bg-white p-3">
            <h2 class="mt-3">Post Free Ad</h2>
            <div class="row">
               <div class="col-sm-8">
               <?php
               require_once 'dbconnect.php';
               $parent_id= $_GET["Category"];
               $subcategory = $_GET["Subcategory"];
               $db     = new DB();
               $sql    = "SELECT * FROM `category` WHERE `parent_id` = $parent_id"; 
               $result = $db->executeQuery($sql);
               while ($fetch = mysqli_fetch_assoc($result)) {
                  if($subcategory == $fetch['id']){
                  echo "<h6 class='mt-2' style=color:black><strong>" .$fetch['source']. "</strong></h6>";
                  }
               }
               ?>
                  <div class="row">
                     <div class="col-sm-6">
                        <?php
                        $db     = new DB();
                        $sql    = "SELECT * FROM `category` WHERE `parent_id` = $parent_id"; 
                        $result = $db->executeQuery($sql);
                        ?>
                        <select name="Category" class="custom-select mb-3">
                           
                        <?php
                           while ($fetch = mysqli_fetch_assoc($result)) {
                                 if($subcategory == $fetch['id']){
                                 echo '<option value="'.$fetch['category_title'].'" selected>'.$fetch['category_title'].'</option>';
                              }else{
                                 echo '<option value="'.$fetch['category_title'].'">'.$fetch['category_title'].'</option>';
                              }
                           }
                        ?>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <select name="Condition" class="custom-select mb-3">
                           <option selected>condition</option>
                           <option value="Almost Like New">Almost Like New</option>
                           <option value="Brand New">Brand New</option>
                           <option value="Genlty Used">Genlty Used</option>
                           <option value="Heaveyly Used">Heaveyly Used</option>
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label for="comment">Ad title</label>
                           <textarea name="Title" class="form-control" rows="1"></textarea>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Price:</label>
                           <input type="number" name="Price" class="form-control">
                           <input type="hidden" name="Parent" value="<?php echo $parent_id; ?>" class="form-control">
                        </div>
                        <div class="form-check-inline">
                           <label class="form-check-label">
                              <input type="checkbox" name="Negotiate" value="No" class="form-check-input">I do not want to negotiate
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Mobile Number:</label>
                           <input type="number" name="Mobile" class="form-control">
                        </div>
                        <div class="form-check-inline">
                           <label class="form-check-label">
                              <input type="checkbox" name="Privacy" class="form-check-input" value="Yes">Keep my phone number private
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Email ID:</label>
                           <input type="email" name="Email" class="form-control">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Pincode:</label>
                           <input type="number" name="Pincode" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-4 mt-4">
                        <form action="sub_postadform.php" method="post" enctype="multipart/form-data">
                           <select name="You_Are" class="custom-select mb-3">
                              <option selected>You Are*</option>
                              <option value="Individual">Individual</option>
                              <option value="Dealer">Dealer</option>
                           </select>
                        </form>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <div class="form-group">
                              <label>GST Number:</label>
                              <input type="text" name="GST_Number" class="form-control">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <!-- <div class="form-group">
                           <div class="form-group">
                              <label>Quantity:</label>
                              <input type="number" name="Quantity" class="form-control">
                           </div>
                        </div> -->
                     </div>
                  </div>
                  <!-- <div class="row">
                     <div class="col-sm-12 text-center">
                        <h4>Additional Ad details</h4>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6 mt-4">
                        <select name="Furniture_Materials" class="custom-select mb-3">
                           <option selected>Furniture Materials*</option>
                           <option value="Volvo">Volvo</option>
                           <option value="Fiat">Fiat</option>
                           <option value="Audi">Audi</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <div class="form-group">
                              <label>Quantity:</label>
                              <input type="number" name="Quantity" class="form-control">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <select name="Brand_Name" class="custom-select mb-3">
                           <option selected>Brand Name*</option>
                           <option value="Volvo">Volvo</option>
                           <option value="Fiat">Fiat</option>
                           <option value="Audi">Audi</option>
                        </select>
                     </div>
                     <div class="col-sm-6"></div>
                  </div> -->
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label for="comment">Ad Description*</label>
                           <textarea class="form-control" rows="5" name="Ad_Description"
                              laceholder="Ad Description...."></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="row">
                     <div class="col-sm-12">
                        <h6 class="text-center">You can add up to 15 photos</h6>
                        <!-- <div class="custom-file mb-3">
                           <input type="file" name="image[]" class="custom-file-input" id="files" multiple>
                           <label class="custom-file-label" name="Photos" for="customFile">Choose file</label>
                        </div>
                        <div class="field" ></div> -->
                        <div class="field custom-file" style="display:none;">
                        <label class="custom-file-label" name="Photos" for="files">Choose file</label>
                        <input type="file" id="files" name="image[]" class="" multiple />
                        </div>
                        <div class="dotted-box d-flex justify-content-start remove1" id="remove1">
                           <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                           <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                           <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                           <input type="hidden" id="files1" >
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                     <?php /* <div class="border">
                           <div style="background-color:#008bcf;color:white;">
                              <h5 class="text-center pt-2">Type of Ads</h5>
                              <p class="text-center text-white">Please choose the below</p>
                           </div>
                           <div class="accordion md-accordion mt-0" id="accordionEx" role="tablist"
                              aria-multiselectable="true">
                              <!-- Accordion card -->
                              <div class="card">
                                 <!-- Card header -->
                                 <div class="card-header" role="tab" id="headingOne1">
                                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                                       aria-expanded="true" aria-controls="collapseOne1">
                                       <input type="radio" class="form-check-input" name="Ads">
                                       <label for="defaultRadio">Free Ad</label>
                                       <span class="float-right">₹ 0 / Month</span>
                                    </a>
                                 </div>
                                 <!-- Card body -->
                                 <div id="collapseOne1" class="collapse show" role="tabpanel"
                                    aria-labelledby="headingOne1" data-parent="#accordionEx">
                                    <div class="card-body">
                                       <ul>
                                          <li>- Visible below Premium Ads & Gold Ads</li>
                                          <li>- Valid for 5 Weeks</li>
                                          <li>- 50 Ad views</li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <!-- Accordion card -->
                              <!-- Accordion card -->
                              <div class="card">
                                 <!-- Card header -->
                                 <div class="card-header" role="tab" id="headingTwo2">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx"
                                       href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                       <input type="radio" class="form-check-input" name="Ads">
                                       <label for="defaultRadio">Premium Ad</label>
                                       <span class="float-right">₹ 499 / Month</span>
                                    </a>
                                 </div>
                                 <!-- Card body -->
                                 <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                                    data-parent="#accordionEx">
                                    <div class="card-body">
                                       <ul>
                                          <li>- Visible above all Regular Ads</li>
                                          <li>- Highlighted as 'Premium'</li>
                                          <li>- Valid for 4 weeks as premium + 5 Weeks as free</li>
                                          <li>- Assured 100 views within 4 weeks or 100% Paytm Cashback</li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <!-- Accordion card -->
                              <!-- Accordion card -->
                              <div class="card">
                                 <!-- Card header -->
                                 <div class="card-header" role="tab" id="headingThree3">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx"
                                       href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                       <input type="radio" class="form-check-input" name="Ads">
                                       <label for="defaultRadio">Gold Ad</label>
                                       <span class="float-right">₹ 944 / Month</span>
                                    </a>
                                 </div>
                                 <!-- Card body -->
                                 <div id="collapseThree3" class="collapse" role="tabpanel"
                                    aria-labelledby="headingThree3" data-parent="#accordionEx">
                                    <div class="card-body">
                                       <ul>
                                          <li>- Feature in top 3 slots on website</li>
                                          <li>- Highlighted as 'Gold'</li>
                                          <li>- Valid for 1 week as Gold + 3 weeks as premium + 5 Weeks as free</li>
                                          <li>- 10X more Ad views</li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <!-- Accordion card -->
                           </div>
                        </div> */?>
                     </div>
                  </div>
                  <div class="row text-center mt-3">
                     <div clas="col-sm-12">
                        <button type='submit' value='Upload' name='upload' class="btn btn-primary btn-custom w-50">AD POST</button>
                        <div class="form-check-inline mt-2">
                           <label class="form-check-label">
                              <input type="checkbox" class="form-check-input text-center" value="Yes">Send me MyClickOnline Email/SMS Alerts
                           </label>
                        </div>
                        <p class="">By clicking "Post Ad", you agree to our<br> <a href="t&c.php" alt="any">"Terms of Uses"</a>
                           and
                           <a href="policies.php" alt="any">Privacy Policy</a> </p>
                     </div>
                  </div>
                  <p class="text-justify text-center">By clicking "Post Ad", you agree to our <a href="t&c.php" alt="any">"Terms of Uses"</a> and <a
                        href="policies.php" alt="any">Privacy Policy</a> </p>
               </div>
            </div>
      </form>
      </section>
      <div class="bg-white py-3 mt-3">
   <section class="container mt-4">
      <div class="row">
         <div class="col-sm-12">
            <ul class="nav">
               <li class="nav-item">
                  <a class="nav-link" href="aboutus.php">About Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">| All Cities</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="contactus.php">| Contact Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="policies.php">| Listing Policy</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="t&c.php">| Terms of Use</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="policies.php">| Privacy Policy</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="career.php">| Careers</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="sitemap.php">| Sitemap</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">| Popular Searches</a>
               </li>
         
               <li class="nav-item">
                  <a class="nav-link" href="adsales.php">| Advertise With Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">| Premium Ads</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">| Blog</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="help.php">| Help</a>
               </li>
            </ul>
         </div>
      </div>
   </section>
   </div>
   <script>
   $(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
       $("#remove1>span").hide();
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files1");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
            if($(".pip").length){
            }
            else{
               $("#remove1>span").show();

            }
          });
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
  $('.remove1>span').click(function() {
   $("#files").click();
    });
});
   </script>
   <!-- <script>
   function preview_image() 
{
 var total_file=document.getElementById("customFile").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"' width='70' height='70' class='mx-1'>");
 }
}
</script> -->
   <script>
      // Add the following code if you want the name of the file appear on select
      $(".custom-file-input").on("change", function () {
         var fileName = $(this).val().split("\\").pop();
         $(this).siblings(".custom-file-label").addClass("selected").php(fileName);
      });

   </script>
</body>

</html>