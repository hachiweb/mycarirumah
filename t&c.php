<?php
include 'header.php'
?>
<title>Terms Conditions</title>
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12"
            style="background-image: url('assets/images/15.jpg'); background-size:cover;height:250px;width:100%;">
            <h1 style="text-align:center;padding-top:100px;color:white">Terms of Uses</h1>
         </div>
      </div>
   </div>
   <section class="bg-white">
   <div class="container py-2">
      <nav class="navbar navbar-expand-sm static-nav anav">
         <ul class="navbar-nav">
            <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
            <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
            <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
            <li class="nav-item active"><a class="active" href="t&c.php">Terms & Conditions</a></li>
            <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
         </ul>
      </nav>
   </div>
   </section>
   <div class="border-top"></div>
   <div class="container">
      <div class="row mt-3">
         <div class="col-sm-3 left-col-fixed bg-white p-3">
            <h5 class="text-dark mx-2"><u> Terms & Conditions</u></h5>
            <!-- Accordian -->
            <h4 class="accordion bg-white">Terms of Uses</h4>
            <div class="panel">
               <ul>
                  <li><a href=""> Using MyClickOnline</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Abuse of MyClickOnline</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
               </ul>
            </div>
            <h4 class="accordion bg-white">Warranty only Protection<br>Plan</h4>
            <div class="panel">
               <ul>
                  <li><a href="">Definitions of words</a></li>
                  <li><a href="">General conditions</a></li>
                  <li><a href="">Scope of Contract</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href="">Special Conditions</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
               </ul>
            </div>
            <h4 class="accordion bg-white">Insurance</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <!-- End Accordian -->
         </div>
         <div class="col-sm-9 bg-white p-5 text-justify">
            <h2 class="font-weight-dark text-muted"><small>Terms of Use</small></h2>
            <div class="border border-top"></div>
            <div class="row ml-2 ">
               <strong class="mt-4">Introduction.</strong>
               <p>Welcome to <a class="text-decoration-none" alt="any" href="www.MyClickOnline.com">
                     www.MyClickOnline.com </a> ("MyClickOnline / Site"). These are the terms and conditions governing
                  your use of the Site ("herein after referred to as Acceptable Use Policy "AUP"). By accessing
                  MyClickOnline either through the website or any other electronic device, you acknowledge, accept and
                  agree to the following terms of the AUP, which are designed to make sure that MyClickOnline works for
                  everyone. This AUP is effective from the time you log in to MyClickOnline. By accepting this AUP, you
                  are also accepting and agreeing to be bound by the Privacy Policy and the Listing Policy.</p>
               <p>While making use of MyClickOnline classifieds and other services such as the discussion forums,
                  comments and feedback and other services interalia, you will post in the appropriate category or area
                  and you agree that your use of the Site shall be strictly governed by this AUP including the policy
                  for listing of your Classified which shall not violate the prohibited and restricted items policy
                  (herein after referred to as the Listing Policy.) (Listing Policy) The listing policy shall be read as
                  part of this AUP and is incorporated in this AUP by way of reference:</p>
               <div>
                  <span>1. </span><strong>Using MyClickOnline.</strong>
                  <p>You agree and understand that <a class="text-decoration-none text-secondry" alt="any"
                        href="www.MyClickOnline.com"> www.MyClickOnline.com </a> is an internet enabled electronic
                     platform that facilitates communication for the purposes of advertising and distributing
                     information pertaining to goods and/ or services. You further agree and understand that we do not
                     endorse, market or promote any of the listings, postings or information, nor do we at any point in
                     time come into possession of or engage in the distribution of any of the goods and/ or services,
                     you have posted, listed or provided information about on our site. While interacting with other
                     users on our site, with respect to any listing, posting or information we strongly encourage you to
                     exercise reasonable diligence as you would in traditional off line channels and practice judgment
                     and common sense before committing to or complete intended sale, purchase of any goods or services
                     or exchange of information. We recommend that you read our <a
                        class="text-decoration-none text-secondry" alt="any" href="#"> safity tips </a> before doing any
                     activity on our site.</p>
               </div>
               <span>2. </span><strong> Eligibility</strong>
               <p>Use of<a class="text-decoration-none text-secondry" alt="any" href="www.MyClickOnline.com">
                     www.MyClickOnline.com </a> ,either by registration or by any other means, is available only to
                  persons, who are Citizens of the Republic of India, who are 18 yrs of age and above and persons who
                  can enter into a legally binding contract, and or are not barred by any law for the time being in
                  force. If you access MyClickOnline, either by registration on the Site or by any other means, not as
                  an individual but on behalf of a legal entity, you represent that you are fully authorized to do so
                  and the listing, posting or information placed on the site on behalf of the legal entity is your
                  responsibility and you agree to be accountable for the same to other users of the Site.</p>
            </div>
            <span>3. </span><strong>Abuse of MyClickOnline</strong>
            <p>You agree to inform us if you come across any listing or posting that is offensive or violates our
               listing policy or infringes any intellectual property rights by clicking on the following link
               support@MyClickOnline.com to enable us to keep the site working efficiently and in a safe manner. We
               reserve the right to take down any posting, listing or information and or limit or terminate our services
               and further take all reasonable technical and legal steps to prevent the misuse of the Site in keeping
               with the letter and spirit of this AUP and the listing policy. In the event you encounter any problems
               with the use of our site or services you are requested to report the problem by clicking on this link
               support@MyClickOnline.com</p>
            <span>4. </span><strong>Violations by User</strong>
            <p>You agree that in the event your listing, posting or your information violates any provision of this AUP
               or the listing policy, we shall have the right to terminate and or suspend your membership of the Site
               and refuse to provide you or any person acting on your behalf, access to the Site.</p>
            <span>5. </span><strong>Content</strong>
            <p>The Site contains content which includes Your Information, MyClickOnline's information and information
               from other users. You agree not to copy, modify, or distribute such content (other than Your
               Information), MyClickOnline's copyrights or trademarks. When you give us any content as part of Your
               Information, you are granting us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free,
               sub-licensable right and license to use, reproduce, , publish, translate, distribute, perform and display
               such content (in whole or part) worldwide through the Site as well as on any of our affiliates or
               partners websites, publications and mobile platform. We need these rights with respect to the content in
               Your Information in order to host and display your content. If you believe that there is a violation,
               please notify us by clicking here support@MyClickOnline.com.</p>
            <span>6. </span><strong>Liability</strong>
            <p>You agree not to hold<a class="text-decoration-none text-secondry" alt="any"
                  href="www.MyClickOnline.com"> www.MyClickOnline.com </a> or any of its officers, employees, agents
               responsible or accountable for any of your listing, postings or information and nor shall we, our
               officers, employees or agents be liable for any misuse, illegal activity or third party content as most
               postings, listings or information are generated by various users directly and we do not have any role in
               the creation, publication or distribution of the posting, listing or information, nor are we in a
               position to have editorial control over the substance or content contained in the listings, postings, or
               information, save and except to the extent provided in sec 3 above.</p>
            <p>You understand and agree that we do not guarantee the accuracy or legitimacy of any listing, posting,
               information by other users. You further agree that we are not liable for any loss of money, goodwill, or
               reputation, or any special, indirect, or consequential damages arising out of your use of the site or as
               a result of any sale, purchase of goods and services with other users of the site. We also cannot
               guarantee continuous or secure access to our Services. Accordingly, to the extent legally permitted we
               exclude all implied warranties, of merchantability, fitness or quality of the Site and our services.</p>
            <span>7. </span><strong>Personal Information</strong>
            <p>By using MyClickOnline, you agree to the collection, transfer, storage and use of any personal
               information provided by you on the Site by MyClickOnline India Private Limited. The data is stored and
               controlled on servers located in Mumbai, India as further described in our Privacy Policy. By submitting
               your resume with your replies, you give permission to MyClickOnline to publicly display your resume which
               can be freely accessed by anyone. You also agree to receive marketing communications from us unless you
               specifically indicate otherwise in writing to us through support@MyClickOnline.com. You may send
               questions about this policy to support@MyClickOnline.com.</p>
            <p>You agree that MyClickOnline may use information shared by you to provide customer services and to
               understand if any other services or advertisement listed on MyClickOnline website may interest you.
               <p>You agree that, we may share personally identifiable data of any individual to our agents or
                  recruiters or Affiliates defined as any person directly or indirectly controlling, controlled by, or
                  under direct or indirect common control with, MyClickOnline, including its subsidiaries and associate
                  companies.</p>
            </p>
            <span>8. </span><strong>General</strong>
            <p>We may update this AUP or the listing policy at any time and may notify you of such updates via a post on
               the boards and /or through email communications. The modified AUP and /or Listing Policy shall come into
               effect either at the time you place your next posting, listing or information on the Site or after a
               period of 30 days from the date of the update, whichever is sooner. If we or our corporate affiliates are
               involved in a merger or acquisition, we may share personal information with another company, but this AUP
               shall continue to apply.</p>
            <span>9. </span><strong>Third Party Content and Services</strong>
            <p>MyClickOnline may provide, on its site, links to sites operated by other entities. If the user decides to
               view this site, they shall do so at their own risk, subject to that site’s terms and conditions of use
               and privacy policy that may be different from those of this site. It is the user’s responsibility to take
               all protective measures to guard against viruses or other destructive elements they may encounter on
               these sites. MyClickOnline makes no warranty or representation regarding, and do not endorse any linked
               website or the information appearing thereon or any of the products or services described thereon.</p>
            <p>Further, user’s interactions with organizations and/or individuals found on or through the service,
               including payment and delivery of goods and services, and any other terms, conditions, warranties or
               representations associated with such dealings, are solely between the user and such organization and/or
               individual. The user should make whatever investigation they feel necessary or appropriate before
               proceeding with any offline or online transaction with any of these third parties.</p>
            <span>10. </span><strong>Indemnity</strong>
            <p>The User should agree to indemnify and hold MyClickOnline, its officers, subsidiaries, affiliates,
               successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless
               from any claim or demand, including reasonable attorney fees and court costs, made by any third party due
               to or arising out of content submitted by the user, users use of the service, violation of the TOU,
               breach by the user of any of the representations and warranties herein, or user’s violation of any rights
               of another.</p>
            <span>11. </span><strong>Governing Law & Jurisdiction</strong>
            <p>This AUP and the Listing Policy shall be governed and construed in accordance with the laws of the
               Republic of India and the courts at Mumbai shall have exclusive jurisdiction on all matters and disputes
               arising out of and relating to the Site.</p>
            <span>12. </span><strong>Make an Offer/Buy Now Terms
            </strong>
            <h6 class="mt-3">MyClickOnline Assurance for Buyers</h6>
            <ol>
               <li>You should "Make an Offer" of the amount which you wish to pay for the product.</li>
               <li>Once your offer is accepted, you can make the payment online.</li>
               <li>The listed item will be delivered to you by our logistics partner, and only after the delivery,
                  payment will be released to seller.</li>
            </ol>
            <p>The BUY NOW purchase option enables a quick sale and implies that you agree to buy the product at the
               listed price. We will facilitate the payment directly and notify the seller. Once the payment has been
               made, we will schedule and complete the delivery. If you wish to negotiate, please choose 'Make an Offer'
               purchase option.</p>
         </div>
      </div>
   </div>

  <!-- FOOTER -->
<?php
include 'footer.php'
?>
   <script>
      var acc = document.getElementsByClassName("accordion");
      var i;

      for (i = 0; i < acc.length; i++) {
         acc[i].addEventListener("click", function () {
            this.classList.toggle("active1");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
               panel.style.maxHeight = null;
            } else {
               panel.style.maxHeight = panel.scrollHeight + "px";
            }
         });
      }
   </script>
</body>

</html>