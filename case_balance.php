<?php
include 'header.php'
?>
<title>Case Balance</title>
<section class="container">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-5 p-3">
            <div class="row set_padding">
                <div class="col-sm-12 text-center">
                    <img src="assets/images/case_img.PNG" alt="">
                    <p class="text-white">Your current balance</p>
                    <p class="h1 text-white">₹ 0 </p>
                </div>
            </div>
            <div class="row bg-white">
                <div class="col-sm-4">
                    <div class="row mt-3">
                        <div class="col-sm-6">
                            <div class="mt-3 float-right">
                                <img src="assets/images/refund.PNG" alt="FILE NOT FOUND">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-left">
                                <p class=" ">Refund</p>
                                <p class="">₹ 0 </p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="row mt-3">
                        <div class="col-sm-6">
                            <div class="float-right mt-3">
                                <img src="assets/images/remitance.PNG" alt="FILE NOT FOUND">
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <p class="mr-2">Remittance</p>
                            <p class="">₹ 0 </p>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="row mt-3">
                        <div class="col-sm-6">
                            <div class="float-right mt-3">
                                <img src="assets/images/promo.PNG" alt="FILE NOT FOUND">
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <p class="">Promo</p>
                            <p class="">₹ 0 </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row bg-white mt-3 pb-3">
                <div class="col-sm-12 mt-4">
                    <span>How to earn?</span>
                    <span class="float-right"><a href="#">Know more</a></span>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-3">
                        <img src="assets/images/case1.png" style="width:60%;margin-left:25px;" alt="FILE NOT FOUND">
                    </div>
                    <div class="col-sm-9">
                        <p>Convert your ad to premium and get cash back upto Rs.500 in your QCash account</p>
                        <span class="h3 text-dark">₹ 500</span>
                    </div>

                </div>
                <div class="row mt-3">
                    <div class="col-sm-3">
                        <img src="assets/images/case1.png" style="width:60%;margin-left:25px;" alt="FILE NOT FOUND">
                    </div>
                    <div class="col-sm-9">
                        <p>Post a Free Listing and earn Rs.50 in your QCash account</p>
                        <span class="h3 text-dark">₹ 50</span>
                    </div>

                </div>
            </div>
            <div class="row mt-3 bg-white py-3">
                <div class="col-sm-12">
                    <h4 class="mt-4 mb-4 text-dark">FAQ</h4>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        What is cash balance?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    MyClickOnline Cash balance stores records for the money you paid for cancelled or
                                    duplicate orders, money credited for the items you sold. It also consists of the
                                    MyClickOnline promotional money credited to your account based on your actions.

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What is remittance money?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordion">
                                <div class="card-body">
                                    Remittance money contains money credited for the items you sold on MyClickOnline.
                                    You can use this money on MyClickOnline or get it transferred to your bank account
                                    anytime.

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        What is refund money?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">

                                    Refund money contains money credited for the refund cases. You can use this money on
                                    MyClickOnline or get it transferred to your source account.

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                        What is promo money?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">


                                    Promo money contains the promotional money added to your account based on your
                                    actions on MyClickOnline.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                        How to use the cash balance?

                                    </button>
                                </h5>
                            </div>
                            <div id="collapsefive" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">

                                    The money will be auto-debited against your next transaction

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                        Can I withdraw money from my cash balance?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsesix" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">


                                    You can always withdraw your remittance money by providing your bank details. Refund
                                    money will always be transferred to the source through which you would have made the
                                    payment.

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseseven" aria-expanded="false"
                                        aria-controls="collapseseven">
                                        What happens to my unused remittance/refund <br> money?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseseven" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">
                                    If you don't use your refund money for 30 days, it will automatically get
                                    transferred to your source account. Remittance money need to be withdrawn by
                                    providing bank details.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-7 p-3">
            <div class="bg-white">
                <h4 class="h4 text-center p-3">TRANSACTION HISTORY</h4>
                <div class="m-4">
                    <div class="row">
                        <div class="col-sm-12 mx-3">
                            <!-- Nav pills -->
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link btn btn-default active" data-toggle="pill" href="#home">ALL</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-default" data-toggle="pill" href="#menu1">REFUND</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-default" data-toggle="pill" href="#menu2">REMITTANCE</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-default " data-toggle="pill" href="#menu3">PROMO</a>
                                </li>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="home" class="container tab-pane active">
                                        <br>
                                        <div class="row border-top pt-3">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Deduct on Promotion Expiry 200018756672</p>
                                            </div>

                                            <div class="col-sm-5">
                                                <h3 class="text-danger float-right">-₹100</h3>
                                                <p class=" float-right">Debited on: 9/4/2019, 1:45:33 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Amount credited for MyClickOnline Jobs promotion</p>
                                                <p>Expiring on 9/4/2019</p>
                                            </div>

                                            <div class="col-sm-5 mt-5">
                                                <h3 class="text-success float-right">-₹100</h3>
                                                <p class=" float-right">Credited on: 2/4/2019, 11:28:58 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top pt-3">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Deduct on Promotion Expiry 200018756672</p>
                                            </div>

                                            <div class="col-sm-5">
                                                <h3 class="text-danger float-right">-₹100</h3>
                                                <p class=" float-right">Debited on: 9/4/2019, 1:45:33 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Amount credited for MyClickOnline Jobs promotion</p>
                                                <p>Expiring on 9/4/2019</p>
                                            </div>

                                            <div class="col-sm-5 mt-5">
                                                <h3 class="text-success float-right">-₹100</h3>
                                                <p class=" float-right">Credited on: 2/4/2019, 11:28:58 am</p>
                                            </div>

                                        </div>
                                        <!--  -->
                                    </div>
                                    <div id="menu1" class="container tab-pane fade">
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="text-center ml-5">
                                                    <img src="assets/images/refund1.PNG" alt="img not found">
                                                    <p class="ml-5">There is no transaction to show!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu2" class="container tab-pane fade">
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="text-center ml-5">
                                                    <img src="assets/images/refund1.PNG" alt="img not found">
                                                    <p class="ml-5">There is no transaction to show!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu3" class="container tab-pane fade">
                                        <br>
                                        <div class="row border-top pt-3">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Deduct on Promotion Expiry 200018756672</p>
                                            </div>

                                            <div class="col-sm-5">
                                                <h3 class="text-danger float-right">-₹100</h3>
                                                <p class=" float-right">Debited on: 9/4/2019, 1:45:33 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Amount credited for MyClickOnline Jobs promotion</p>
                                                <p>Expiring on 9/4/2019</p>
                                            </div>

                                            <div class="col-sm-5 mt-5">
                                                <h3 class="text-success float-right">-₹100</h3>
                                                <p class=" float-right">Credited on: 2/4/2019, 11:28:58 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top pt-3">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Deduct on Promotion Expiry 200018756672</p>
                                            </div>

                                            <div class="col-sm-5">
                                                <h3 class="text-danger float-right">-₹100</h3>
                                                <p class=" float-right">Debited on: 9/4/2019, 1:45:33 am</p>
                                            </div>

                                        </div>
                                        <div class="row border-top">
                                            <div class="col-sm-6">
                                                <p><img src="assets/images/promo.PNG" alt="">
                                                    Amount credited for MyClickOnline Jobs promotion</p>
                                                <p>Expiring on 9/4/2019</p>
                                            </div>

                                            <div class="col-sm-5 mt-5">
                                                <h3 class="text-success float-right">-₹100</h3>
                                                <p class=" float-right">Credited on: 2/4/2019, 11:28:58 am</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

</section>
</body>

</html>