<?php
include 'header.php';
$id = $_GET['A'];
$ParentID = $_GET['P'];
?>
<title>Ad Details</title>
<?php 
    $db         = new DB();
    if($ParentID=='1' OR $ParentID=='2' OR $ParentID=='3' OR $ParentID=='4' OR $ParentID=='5'){
    $pSQL    = "SELECT * FROM `postadform` WHERE `parent_id` = $ParentID AND `id` = $id";
    $pResult = $db->executeQuery($pSQL);
    $adData = mysqli_fetch_assoc($pResult);
    ?>

<section class="container mt-4">
            <div class="row bg-clr-set border">
                <h3 class="p-4 text-dark font-weight-bold"><?=$adData['title'];?></h3>
                <div class="d-flex img-horizontal mx-1" id="iv">
                    <?php $iMgF = (explode(",",$adData['imgname']));
                    for ($i=1; $i <count($iMgF) ; $i++) { ?>
                        <span class="contain w-25">
                        <a href="<?=!empty($iMgF)?"upload/".$iMgF[$i]:"assets/images/demo/NIA.png"; ?>" title=""><img src="<?=!empty($iMgF)?"upload/".$iMgF[$i]:"assets/images/demo/NIA.png"; ?>"
                            class="img-thumbnail image ad-detail-img" alt="img not found">
                            <div class="middle">
                                <div class="text">
                                <button type="button" class="btn btn-secondary border-white"><i class="fas fa-search-plus px-3"></i></button>
                                </div>
                            </div>
                            </a>
                        </span>
                    <?php
                        }
                        ?>
                </div>
                <div class="m-3">
                        <h5 class="text-dark">Price:</h5>
                        <h5>₹ <?=$adData['price'];?></h5>
                </div>
            </div>
            <div class="row bg-white border mt-3 txt-clr">
                <!-- <h3 class="col-sm-12 text-dark text-center p-3"><u>Ad-Details</u></h3> -->
                <div class="col-12">
                    <h4 class="text-dark m-3">Description</h4>
                    <p class="text-dark text-justify mx-3"><?=$adData['description'];?></p>
                </div>
                <div class="col-12 border-top"></div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>Condition:</h6>
                    <span><?=$adData['condition'];?></span>
                </div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>Seller Type:</h6>
                    <span><?=$adData['saler_type'];?></span>
                </div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>GST Number:</h6>
                    <span><?=$adData['gst_number'];?></span>
                </div>
                <div class="col-sm-12 border-top mt-3"></div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>Mobile Number:</h6>
                    <span><?=$adData['mobile'];?></span>
                </div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>Email id:</h6>
                    <span><?=$adData['email'];?></span>
                </div>
                <div class="col-sm-4 text-center mt-3">
                    <h6>Pin Code:</h6>
                    <span><?=$adData['pincode'];?></span>
                </div>
                <hr class="w-80 ">
                <div class="col-sm-12 border-top mt-3"></div>
            </div>
            <div class="row bg-white border mt-3">
                <div class="col-sm-12">
                    <div class="d-flex justify-content-between p-4 txt-clr">
                        <h6>Ad ID: <span class="text-dark"><?=$adData['id'];?></span></h6>
                        <h6>Posted On: <span class="text-dark"><?= date('d F, Y (l)',strtotime($adData['posted_at'])) ;?></span></h6>
                    </div>
                </div>
            </div>
</section>

<?php
}elseif($ParentID=='6'){
    $jSQL    = "SELECT * FROM `jobpostform` WHERE `id` = $id";
    $jResult = $db->executeQuery($jSQL);
    $adData = mysqli_fetch_assoc($jResult);
?>
<section class="container mt-4">
    <div class="row">
        <div class="col-sm-12 bg-white border">
            <div class="">
                <h4 class="text-dark p-3"style="text-transform:uppercase;"><?=$adData['job_title'];?></h4>
                <div class="p-3" style="background-color:rgb(242,242,242);">
                    <h6 class="d-inline">Posted on | <span
                            class="text-dark"><?= date('d F, Y (l)',strtotime($adData['posted_at'])) ;?></span></h6>
                    <h6 class="d-inline float-right font-weight-normal">JOB: | <span class="text-dark">
                            <?=$adData['id'];?></span></h6>
                </div>
            </div>
            <div class="col-sm-2"></div>
            <h5 class="py-3 mx-1">JOB DETAILS</h5>
            <div class="row mx-1">
                <div class="col-sm-3 txt-job-sal">
                    <h6>Salary:</h6>
                    <h6>Job Type:</h6>
                    <h6>Company:</h6>
                    <h6>Experience:</h6>
                    <h6>Role:</h6>
                    <h6>Contact Number:</h6>
                    <h6>Email address:</h6>
                    <h6>Location:</h6>

                </div>
                <div class="col-sm-3 text-dark">
                    <h6>$ <?=$adData['salary_min']." - ".$adData['salary_max'];?></h6>
                    <h6><?=$adData['job_type'];?></h6>
                    <h6><?=$adData['company_name'];?></h6>
                    <h6><?=$adData['experience_min']." - ".$adData['experience_max'];?></h6>
                    <h6><?=$adData['role'];?></h6>
                    
                    <h6><?=$adData['number'];?></h6>
                    <h6><?=$adData['email'];?></h6>
                    <h6><?=$adData['locality'].", ".$adData['city'];?></h6>
                </div>
                <div class="col-sm-3"></div>
                <div class="col-sm-12 pb-4">
                    <div class="mt-3">
                            <h5 class="py-3">JOB DESCRIPTION</h5>
                            <p class="font-weight-normal text-justify p-3"><?=$adData['description'];?></p>
                    </div>
                </div>
                <div class="col-sm-12 pb-4 border-top">
                    <div class="mt-3">
                           <button type="button" class="btn btn-primary"><span class="px-4 py-2">Apply</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
}else{
    $oSQL    = "SELECT * FROM `postfreeadform` WHERE `parent_id` = $ParentID AND `id` = $id";
    $oResult = $db->executeQuery($oSQL);
    $adData = mysqli_fetch_assoc($oResult);
?>
<!--third section start-->
<section class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row bg-white mt-3 p-4 border">
                <div class="col-sm-12">
                    <h3 class="text-dark"><?=$adData['title'];?></h3>
                </div>
                <div class="col-sm-12 mt-4">
                <div class="d-flex img-horizontal mx-1" id="iv">
                    <?php $iMgF = (explode(",",$adData['imgname']));
                    for ($i=1; $i <count($iMgF) ; $i++) { ?>
                        <span class="contain w-25">
                            <a href="<?=!empty($iMgF)?"upload/".$iMgF[$i]:"assets/images/demo/NIA.png"; ?>" title=""><img src="<?=!empty($iMgF)?"upload/".$iMgF[$i]:"assets/images/demo/NIA.png"; ?>"
                            class="img-thumbnail image ad-detail-img" alt="img not found">
                            <div class="middle">
                                <div class="text">
                                <button type="button" class="btn btn-secondary border-white"><i class="fas fa-search-plus px-3"></i></button>
                                </div>
                            </div>
                            </a>
                        </span>
                    <?php
                        }
                        ?>
                </div>
                    
                </div>
            
            </div>
            <div class="row bg-white border mt-4">
                <h3 class="col-sm-12 text-dark p-3">Description</h3>
                <p class="px-3"><?=$adData['description'];?></p>
                <div class="col-sm-12 border-top mt-3"></div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Institute Name:</h6>
                    <span><?=$adData['institute_name'];?></span>
                </div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Type:</h6>
                    <span><?=$adData['education'];?></span>
                </div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Board:</h6>
                    <span><?=$adData['board'];?></span>
                </div>
                <div class="col-sm-12 border-top mt-3"></div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Subject:</h6>
                    <span><?=$adData['subject'];?></span>
                </div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Standard:</h6>
                    <span><?=$adData['standard'];?></span>
                </div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Delivery Mode:</h6>
                    <span><?=$adData['delivery_mode'];?></span>
                </div>
                <div class="col-sm-12 border-top mt-3"></div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Fees:</h6>
                    <span><?=$adData['fees'];?></span>
                </div>
                <div class="col-sm-4 mt-3">
                    <h6 class="text-dark d-inline">Discount Percentage:</h6>
                    <span><?=$adData['discount_percentage'];?></span>
                </div>
                <div class="col-sm-4 my-3">
                    <h6 class="text-dark d-inline">Locality:</h6>
                    <span><?=$adData['locality'];?></span>
                </div>

            </div>
            <div class="row bg-white border mt-3">
                <div class="col-sm-12 mt-2">
                    <h3 class="text-dark p-2">Contact Details</h3>
                    <div class="col-sm-12 border-top my-2"></div>
                        <div class="row p-3">
                        <div class="col-sm-4">
                            <h5 class="text-dark d-inline">Name:</h5>
                            <span><?=$adData['name'];?></span>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="text-dark d-inline">Contact Number:</h5>
                            <span><?=$adData['mobile'];?></span>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="text-dark d-inline">Email Address:</h5>
                            <p><?=$adData['email'];?></p>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12 border-top mt-3 mb-3"></div>
                        <div class="col-sm-4">
                            <h5 class="text-dark d-inline">Institute Website:</h5>
                            <p><a href="#"><?=$adData['institute_website'];?></a></p>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="text-dark d-inline">Institute Address:</h5>
                            <p><?=$adData['institute_adderess'];?></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>
                </div>

            </div>
            <div class="row bg-white border mt-3">
                <div class="col-sm-12">
                    <div class="d-flex justify-content-between p-4 txt-clr">
                        <h6>Ad ID: <span class="text-dark"><?=$adData['id'];?></span></h6>
                        <h6>Posted On: <span class="text-dark"><?= date('d F, Y (l)',strtotime($adData['posted_at'])) ;?></span></h6>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php
}
?>



<section class="footer-sec bg-white mt-5">
    <div class="container">
        <!---------------third main row start--------------->
        <div class="row">
            <div class="col-lg-7">
                <nav class="navbar navbar-expand-sm px-0">
                    <ul class="navbar-nav flex-wrap">
                        <li> <a href="aboutus.php" alt="any" class="nav-link"><i class="nav-item"></i> About us</a></li>
                        <li> <a href="contactus.php" alt="any" class="nav-link"><i class="nav-item"></i> Contact us</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="career.php">Careers</a></li>
                        <li class="nav-item"><a class="nav-link" href="adsales.php">Advertise with us</a></li>

                        <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                        <li> <a href="help.php" alt="any" class="nav-link"><i class="nav-item"></i> Help</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Premium Ads</a></li>
                    </ul>
                </nav>
                <p class="text-justify">Widely known as India’s no. 1 online classifieds platform, MyClickOnline is all
                    about you. Our aim is to empower every person in the country to independently connect with buyers
                    and
                    sellers online. We care about you — and the transactions that bring you closer to your dreams. Want
                    to
                    buy your first car? We’re here for you. Want to sell commercial property to buy your dream home?
                    We’re
                    here for you. Whatever job you’ve got, we promise to get it done.</p>
                <p class="text-dark bg-gray p-3">At MyClickOnline, you can buy, sell or rent anything you can think
                    of.<a class="btn-custom btn-red ml-4" href="postfreead.php">Post Free Ad</a></p>
                <nav class="navbar navbar-expand-sm px-0">
                    <ul class="navbar-nav flex-wrap">
                        <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Listing Policy</a></li>
                        <li class="nav-item"><a class="nav-link" href="t&c.php" alt="any" class="nav-link"><i
                                    class="nav-item"></i> Terms of uses</a></li>
                        <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Privacy Policy</a></li>
                        <li class="nav-item"><a class="nav-link" href="policies.php" alt="any" class="nav-link"><i
                                    class="nav-item"></i> mobile Policies</a></li>

                        <li class="nav-item"><a class="nav-link" href="sitemap.php" alt="any">Sitemap</a></li>
                        <li class="nav-item"><a class="nav-link" href="#" alt="any">News</a></li>
                    </ul>
                </nav>

            </div>
            <div class="col-lg-5 border-left">
                <div class="row">
                    <nav class="navbar col-sm-4">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="#">Ahmedabad</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Bangalore</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Chandigarh</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Chennai</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Coimbatore</a></li>
                        </ul>
                    </nav>
                    <!-- A vertical navbar -->
                    <nav class="navbar col-sm-4">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="#">Delhi</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Gurgaon</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Hyderabad</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Jaipur</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Kochi</a></li>
                        </ul>
                    </nav>
                    <!-- A vertical navbar -->
                    <nav class="navbar col-sm-4">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="#">Kolkata</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Lucknow</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Mumbai</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Pune</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Trivandrum</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="row flex-wrap social-icons my-3">
                    <h6>Follow Us :</h6>
                    <span>
                        <a href="#" class="fa fa-facebook"></a>
                        <a href="#" class="fa fa-linkedin"></a>
                        <a href="#" class="fa fa-google"></a>
                        <a href="#" class="fa fa-youtube"></a>
                    </span>
                </div>
                <div class="row flex-wrap social-icons my-3">
                    <h6>Download The App :</h6>
                    <span>
                        <a href="#" class="fa fa-apple font1"></a>
                        <a href="#" class="fa fa-android font2"></a>
                        <a href="#" class="fa fa-windows font3"></a>
                    </span>
                </div>
            </div>
        </div>
    </div><!-- container -->
</section><!-- footer-sec -->
<link rel="stylesheet" href="assets/css/jquery.imageview.css">
<script src="assets/js/jquery.imageview.js"></script>
    <script>
	$('#iv').imageview();
	</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<body>

    </html>