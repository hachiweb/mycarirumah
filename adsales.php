<?php
include 'header.php'
?>
<title>Advertise With Us</title>
   <div class="border-top mt-2"></div>
   <div>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img class="d-block w-100" src="assets/images/adsaleimg.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
               <img class="d-block w-100" src="assets/images/adsaleimg1.jpg" alt="Second slide">
            </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
   <section class="container">
      <div class="row mt-5">
         <div class="text-center text-muted text-justify">
            <h4>EXPLORE THE POWER OF MyClickOnline AUDIENCE PLATFORM</h4>
            <p class="text-justify text-center mt-4">Consumers don’t come to MyClickOnline for entertainment or news.
               They come
               to us with a very precise intent – to either buy, sell or find something – across a wide range of
               categories including cars, homes, mobiles, furniture, jobs and various services. Armed with this powerful
               information, brands can target audiences with high intent and purchasing power and ensure the maximum
               impact for their campaigns.
            </p>
         </div>
      </div>
      <div class="row">
         <div class=" mx-auto">
            <button class="btn btn-danger btn-lg">
               <a href="postfreead.php" alt="any" class="text-decoration-none text-white bdr_radius" data-target="#mymodalset"
                  data-toggle="modal">Enquery Now</a>
            </button>
         </div>
         <!-------------login/signup form----------->
         <div class="modal fade" id="mymodalset">
            <div class="modal-dialog modal-dialog-centered modal-lg">
               <div class="modal-content">
                  <div class="modal-header">
                     <h4 class="mt-2 text-dark">Enquire Now</h4>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="sub_adsales.php" method="post" enctype="multipart/form-data">
                     <div class="modal-body">

                        <div class="row">
                           <div class="col-sm-6">

                              <label for="name">Full Name*</label>
                              <input type="name" class="form-control bdr_radius " name="Name" placeholder="Enter Full Name">
                              <label for="number" class="mt-2">Mobile Number*</label>
                              <input type="number" class="form-control bdr_radius" name="Mobile" placeholder="Enter Number">
                              <label for="email" class="mt-2">Email ID*</label>
                              <input type="email" class="form-control bdr_radius" name="Email" placeholder="Enter Email">

                           </div>
                           <div class="col-sm-6 float-right">

                              <label for="name" class="mt-2">Company/Agency*</label>
                              <input type="name" class="form-control bdr_radius" name="Company" placeholder="Enter company/agency">
                              <label for="comment" class="mt-2">Query(optional)*</label>
                              <textarea class="form-control bdr_radius" rows="4" name="Query"></textarea>

                           </div>
                           <div class="row mx-auto">
                              <button type="submit" class="btn btn-lg btn-danger mt-4 bdr_radius">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="">
      <div class="row">
         <div class="set-background px-2">
            <h3 style="text-align:center;margin:10px;">Sharp Targeting. Powerful Results.</h3>
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                     <div class="set-space">
                        <div class="row ">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s14.png" alt="img not found">
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class="text-justify">Target audiences who have high purchase intent on our platform.</p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                     </div>
                     <div class="set-space">
                        <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s15.png" alt="img not found">
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class=" text-justify">
                                 Always stay top of mind with our Cookie Pool Retargeting on and outside MyClickOnline.
                              </p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                     </div>
                     <div class="set-space">
                        <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s16.png" alt="img not found" >
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class=" text-justify">Cut through clutter with innovations for your ad on our platform.
                              </p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="set-space">
                        <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s17.png" alt="img not found" >
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class=" text-justify">
                                 Reach affinity audiences across 180+ categories with our cross category targeting.
                              </p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                     </div>
                     <div class="set-space">
                        <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s18.png" alt="img not found" >
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class=" text-justify">
                                 Smarter ways to reach out to relevant audiences with our Price, Competitor
                                 Search based targeting.
                              </p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                     </div>
                     <div class="set-space">
                        <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                              <img class="" src="assets/images/s19.png" alt="img not found" style="">
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                              <p class=" text-justify">
                                 Your ad reaches a larger pool of by passing Ad Blockers only on MCO.
                              </p>
                              <a href="" class=" float-right">view case study</a>
                           </div>
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="row">
      <div class="mx-auto m-5">
         <h3>The MyClickOnline Advantage</h3>
      </div>
   </section>
   <section class="container">
      <div class="row p-3">
         <div class="col-lg-3 col-md-3 col-sm-3 col-6 text-center">
            <img src="assets/images/s10.PNG" style="width:35%;" alt="">
            <div class="border-right txt-font-set">
               <h3 class="text-primary">30 Million</h3>
               <p class="text-center">Unique Visitors/Month</p>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-sm-3 col-6 text-center">
            <img src="assets/images/s11.PNG" style="width:35%; alt="">
               <div class=" border-right txt-font-set">
            <h3 class="text-primary">90 Million</h3>
            <p class="text-center">Total Visits/Month</p>
         </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-6 text-center">
         <img src="assets/images/s12.PNG" style="width:35%; alt="">
               <div class=" border-right txt-font-set">
         <h3 class="text-primary">50+</h3>
         <p class="text-center">Categories</p>
      </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-6 text-center">
         <img src="assets/images/s13.PNG" style="width:35%;" alt="">
               <div class="txt-font-set ">
         <h3 class="text-primary">1000</h3>
         <p class="text-center">Cities</p>
      </div>
      </div>
      </div>
   </section>
   <section class="set_background1">

      <div class="container">
         <h2 class="text-center text-white p-4">The MyClickOnline Footprint</h2>
         <div class="row p-3">
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <img src="assets/images/s20.PNG" alt="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
               <div class="p-2 text-dark">
                  <h4><strong>180+</strong> Categories <br>
                     <strong>Infinite</strong> Opportunities.
                  </h4>
                  <p class="text-dark text-justify">Whether it is buying a new car or selling a pre-owned TV, searching for a
                     plumber or finding a job, getting information on Education or buying a new house – we are
                     helping our customers simplify their lives every single day.
                  </p>
               </div>
            </div>

         </div>

      </div>
   </section>
   <section class="set_background2">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
            <ul class="list-inline mt-5 d-block">
               <li class="list-inline-item text-white h5"> MyClickOnlineCars</li>
               <li class="list-inline-item text-white h5"> MyClickOnlineHomes</li>
               <li class="list-inline-item text-white h5"> MyClickOnlineServices</li>
               <li class="list-inline-item text-white h5"> MyClickOnlineCars</li>
               <li class="list-inline-item text-white h5"> MyClickOnlineCars</li>
               <li class="list-inline-item text-white h5"> MyClickOnlineCars</li>
            </ul>
         </div>
         </div>
      </div>
   </section>
   <section class="container mt-5">
      <div class="row mt-5">
         <div class="mx-auto">
            <h3>OUR CLIENTS</h3>
         </div>
      </div>
   </section>
   <div class="container mt-4">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <!-- Nav pills -->
            <ul class="nav nav-pills txt-font-less" role="tablist">
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default active" data-toggle="pill" href="#home">AUTOMOBILE</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default" data-toggle="pill" href="#menu1">BFSI</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default" data-toggle="pill" href="#menu2">ELECTRONICS</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default " data-toggle="pill" href="#menu2">INTERNET</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default " data-toggle="pill" href="#menu2">TELICOM</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default " data-toggle="pill" href="#menu2">CUSTOMER</a>
               </li>
               <li class="nav-item p-1">
                  <a class="nav-link btn btn-default " data-toggle="pill" href="#menu2">OTHERS</a>
               </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <div id="home" class="container tab-pane active">
                  <br>
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item">
                              <img src="assets/images/BMW.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/royal.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/hero.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/reva.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/Bridgestone.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/michelin.png" alt="" style="width:40%;">
                           </li>
                        </ul>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item">
                              <img src="assets/images/renault.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/toyota.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/general_motors.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/tata.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/tvs_tyres.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/Tvs_motors.png" alt="" style="width:40%;">
                           </li>
                        </ul>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6 margin-top">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item">
                              <img src="assets/images/Honda.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/bajaj.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/fiat.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/apollo.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/castrol.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/datsun.png" alt="" style="width:40%;">
                           </li>
                        </ul>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-6 margin-top">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item">
                              <img src="assets/images/mahindra.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/ford.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/jcb_india.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/firstchoice.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/gm.png" alt="" style="width:40%;">
                           </li>
                           <li class="list-group-item">
                              <img src="assets/images/mahindra_tractor.png" alt="" style="width:40%;">
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div id="menu1" class="container tab-pane fade">
                  <br>
                  <h3>Menu 1</h3>
                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                     consequat.
                  </p>
               </div>
               <div id="menu2" class="container tab-pane fade">
                  <br>
                  <h3>Menu 2</h3>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                     totam rem aperiam.
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row mt-5">
         <div class=" mx-auto">
            <button class="btn btn-danger btn-lg">
               <a href="postfreead.php" alt="any" class="text-decoration-none text-white corner_" data-target="#mymodalset"
                  data-toggle="modal">Advertise Now</a>
            </button>
         </div>
         <!-------------login/signup form----------->
         <div class="modal fade" id="mymodalset">
            <div class="modal-dialog modal-dialog-centered modal-lg">
               <div class="modal-content">
                  <div class="modal-header">
                     <h4 class="mt-2 text-dark">Enquire Now</h4>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="sub_adsales.php" method="post" enctype="multipart/form-data">
                     <div class="modal-body">

                        <div class="row">
                           <div class="col-sm-6">

                              <label for="name">Full Name*</label>
                              <input type="name" class="form-control bdr_radius" name="Name" placeholder="Enter Full Name">
                              <label for="number" class="mt-2">Mobile Number*</label>
                              <input type="number" class="form-control bdr_radius" name="Mobile" placeholder="Enter Number">
                              <label for="email" class="mt-2">Email ID*</label>
                              <input type="email" class="form-control bdr_radius" name="Email" placeholder="Enter Email">

                           </div>
                           <div class="col-sm-6 float-right">

                              <label for="name" class="mt-2">Company/Agency*</label>
                              <input type="name" class="form-control bdr_radius" name="Company" placeholder="Enter company/agency">
                              <label for="comment" class="mt-2">Query(optional)*</label>
                              <textarea class="form-control bdr_radius" rows="4" name="Query"></textarea>

                           </div>
                           <div class="row mx-auto">
                              <button type="submit" class="btn btn-lg btn-danger mt-4">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>

   <section class="row">
      <div class="back_color">
         <div class="container">
            <div class="row m-5">
               <div class="mx-auto">
                  <h3 class="text-dark">Testimonials</h3>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
               <!-- slick sider -->
               <div class="rio-promos">
                  <!--Profile Card -->
                  <div class="card profile-card-5">
                     <div class="card-img-block">
                        <img class="card-img-top car-img"
                           src="https://images.unsplash.com/photo-1517832207067-4db24a2ae47c" alt="Card image cap">
                     </div>
                     <div class="card-body mx-auto">
                        <h5 class="card-title text-center">Gautam Reghunath</h5>
                        <p class="text-center text-dark">Business Director – Digital, Madison World
                        </p>
                        <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were
                           very responsive and went out of their way to ensure maximum value for us. Campaigns were set
                           up under tight deadlines and they were were measured and tweaked to ensure best results.
                        </p>
                     </div>
                  </div>
                  <!--Profile Card -->

                  <div class="card profile-card-5">
                     <div class="card-img-block">
                        <img class="card-img-top" src="https://images.unsplash.com/photo-1517832207067-4db24a2ae47c"
                           alt="Card image cap">
                     </div>
                     <div class="card-body">
                        <h5 class="card-title text-center">Ranjit Wilson</h5>
                        <p class="text-center text-dark">Business Director – Digital, Madison World
                        </p>

                        <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were
                           very responsive and went out of their way to ensure maximum value for us. Campaigns were set
                           up under tight deadlines and they were were measured and tweaked to ensure best results."
                        </p>
                     </div>
                  </div>
                  <!--Profile Card -->

                  <div class="card profile-card-5">
                     <div class="card-img-block">
                        <img class="card-img-top" src="https://images.unsplash.com/photo-1517832207067-4db24a2ae47c"
                           alt="Card image cap">
                     </div>
                     <div class="card-body">
                        <h5 class="card-title text-center">Deepak Yohannan</h5>
                        <p class="text-center text-dark">Founder, MyInsuranceClub
                        </p>
                        <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were
                           very responsive and went out of their way to ensure maximum value for us. Campaigns were set
                           up under tight deadlines and they were were measured and tweaked to ensure best results.
                        </p>
                     </div>
                  </div>
                  <!--Profile Card -->

                  <div class="card profile-card-5">
                     <div class="card-img-block">
                        <img class="card-img-top" src="https://images.unsplash.com/photo-1517832207067-4db24a2ae47c"
                           alt="Card image cap">
                     </div>
                     <div class="card-body">
                        <h5 class="card-title text-center">Amit Duggal</h5>
                        <p class="text-center text-dark">Vice President, Omnicom Media Group

                           <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They
                              were very responsive and went out of their way to ensure maximum value for us. Campaigns
                              were set up under tight deadlines and they were were measured and tweaked to ensure best
                              results.
                           </p>
                     </div>
                  </div>

               </div>
               </div>
            </div>
            <!-- end slick slider -->
         </div>
      </div>
      </div>
   </section>
<!-- FOOTER -->
<?php
include 'footer.php'
?>

</html>