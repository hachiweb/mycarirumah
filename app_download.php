<?php
include 'header.php'
?>
<title>Download App</title>
<div class="border-top "></div>
<section class="bg-white">
<section class="container mt-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center p-5">
            <h3 class="text-dark">A Whole New Way To Buy & Sell</h3>
            <p class="text-center" style="font-size:18px;">Download our Mobile App. Convenience At Your Fingertips</p>
        </div>
    </div>
</section>
<section class="container p-3">
    <div class="row">
        <div class="col-sm-5">
            <h4 style="color:rgb(0,127,191)" class="mx-2">Download our Mobile App: </h4>
            <div class="form-group form-inline">

                <input type="number" class="form-control bdr_radius mx-2" id="usr" placeholder="Enter 10 digits number">
                <button class="btn btn-danger m-2 bdr_radius">Send download link</button>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a class="nav-link btn btn-default1" href="#"><i class=" fa fa-apple">iOS</i></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a class="nav-link btn btn-default1" href="#"><i class=" fa fa-android">Androad</i></a></div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a class="nav-link btn btn-default1" href="#"><i class=" fa fa-windows ">Window</i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="">
            <img src="assets/images/app_img3.PNG" alt="img not found">
        </div>

    </div>
</section>
<section class="container p-4">
    <div class="row my-5">
        <div class="col-sm-5">
            <span class="border-bottom text-primary h6">UPLOAD MULTIPLE PHOTOS IN ONE GO</span>
            <p>We have simplified your work. Now you can easily upload multiple photos in one step.</p>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-5">
            <span class="border-bottom text-primary h6">POST AN AD HASSLE FREE</span>
            <p>Posting an ad on Quikr has become easier than ever. A few simple steps and your ad is posted.</p>
        </div>

    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <img style="width:35%;" src="assets/images/app_img4.PNG" alt="img not foung">

        </div>
</section>
</section>
<section class="set_background3">

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                <img src="assets/images/app_img5.PNG" alt="img not foung">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-12 mt-5">
                <h3 style="color:rgb(0,127,191)">SELL, BUY OR RENT </h3>
                <h5 style="color:rgb(0,127,191)">ALL AT ONE PLACE!</h5>
                <p>Your shopping experience just got way more interesting. With MyClickOnline, you can find and buy your
                    favorite products. You can also sell the items that you no longer use. You can even share your
                    favorite listings with friends via SMS, Whatsapp, Gmail, Facebook, Hike and Skype.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                <h4 style="color:rgb(0,127,191)" class="mx-2">Download our Mobile App: </h4>
                <div class="form-group form-inline">
                    <input type="number" class="form-control bdr_radius mx-2" id="usr"
                        placeholder="Enter 10 digits number">
                    <button class="btn btn-danger m-2 bdr_radius">Send download link</button>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12 mt-4">
            <div class="row">

            <div class="col-sm-4">
                <a class="nav-link btn btn-default1 " href=""><i class="fa fa-apple">iOS</i></a>
            </div>
            <div class="col-sm-4">
                <a class="nav-link btn btn-default1" href="#"><i class="fa fa-android">Androad</i></a></div>
            <div class="col-sm-4">
                <a class="nav-link btn btn-default1" href="#"><i class="fa fa-windows ">Window</i></a>
            </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section class="set_background4">
    <div class="container ">
        <div class="row ">
            <div class="col-lg-4 col-mg-4 col-sm-4 col-12 mt-5">
                <h1 class="text-white">STAY UPDATED </h1>
                <h6 class="text-white">NEVER MISS ANY NOTIFICATION</h6>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-sm-4  col-12">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-sm-4  col-12 mt-5">
                <h6 class="text-white">MyClickOnline is the perfect platform for you to explore and shop. Not only will
                    you get access to the latest ads & exclusive offers, we’ll also update you on price drops, order
                    status and much more.</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-12">
                <img style="" src="assets/images/app_img6.PNG" alt="img not foung">
            </div>
        </div>
    </div>
</section>
<section style="background-image: url('assets/images/app_img7.PNG');height:600px;width:100%;background-position:center;background-repeat: no-repeat;">
    <div class="container ">
        <div class="row ">
            <div class="col-sm-4"></div>
            <div class="col-sm-6" style="margin-top:15%;">
                <span class="text-white h3 border-bottom">SHOP SMARTER</span>
                <p class="text-white">With MyClickOnlineNXT, chat & shop on the go. Share product photos & videos,
                    contacts and much more. Make an offer and shop MyClickOnline. Not just that, you can also avail of
                    cashback</p>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row" style="margin-top:14%;">
        <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                <h4 style="color:rgb(0,127,191)" class="mx-2">Download our Mobile App: </h4>
                <div class="form-group form-inline">
                    <input type="number" class="form-control bdr_radius mx-2" id="usr"
                        placeholder="Enter 10 digits number">
                    <button class="btn btn-danger m-2 bdr_radius">Send download link</button>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-6 mt-4">
                <div class="row">

                    <div class="col-sm-4">
                        <a class="nav-link btn btn-default1 " href=""><i class="fa fa-apple">iOS</i></a>
                    </div>
                    <div class="col-sm-4">
                        <a class="nav-link btn btn-default1" href="#"><i class="fa fa-android">Androad</i></a></div>
                    <div class="col-sm-4">
                        <a class="nav-link btn btn-default1" href="#"><i class="fa fa-windows ">Window</i></a>
                    </div>
                  </div>
            </div>

        </div>
    </div>

</section>


<!-- FOOTER -->
<?php
include 'footer.php'
?>
</body>

</html>