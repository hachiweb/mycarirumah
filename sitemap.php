<?php
include 'header.php'
?>
<title>Sitemap</title>
<div class="">
    <div class="row">
        <div class="col-lg-12">
            <img src="assets/images/16.jpg" alt="" style="width:100%; height:300px;">
        </div>
    </div>
</div>
<section class="bg-white">
    <div class="container p-2">
        <nav class="navbar navbar-expand-sm static-nav anav">
            <ul class="navbar-nav flex-wrap">
                <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
                <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
                <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
                <li class="nav-item active"><a class="active" href="t&c.php">Terms & Conditions</a></li>
                <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
            </ul>
        </nav>
    </div>
</section>

<div class="border-top"></div>
<section class="container bg-white p-5">
    <div class="row border-bottom p-2">
        <div class="col-sm-3">
            <div class="mt-4">
                <img src="assets/images/car.png" style="width:15%;" alt="">
                <span class="h5 text-muted">Cars & Bikes</span>
            </div>
        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Bikes & Scooters</a>
            <a href="#" class="mt-2">Spare Parts - Accessories</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Cars</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Commercial Vehicles</a>

        </div>
        <div class="col-sm-3">
            <a href="">Others Vehicles</a>
        </div>
    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-3">
            <div class="mt-4">
                <img src="assets/images/community.png" style="width:15%;" alt="">
                <span class="h5 text-muted">Community</span>
            </div>
        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Announcements</a><br>
            <a href="#" class="mt-2">Tender Notices</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Car Pool - Bike Ride</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Charity - Donate - NGO</a>

        </div>
        <div class="col-sm-3">
            <a href=""> Lost - Found</a>
        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-3">
            <div class="mt-4">
                <img src="assets/images/education.png" style="width:14%;" alt="">
                <span class="h5 text-muted">Education & Tranning</span>
            </div>
        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Announcements</a><br>
            <a href="#" class="mt-2">Tender Notices</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Car Pool - Bike Ride</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Charity - Donate - NGO</a>

        </div>
        <div class="col-sm-3">
            <a href=""> Lost - Found</a>
        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/electronics.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Electronics & Appliances</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Camera Accessories</a><br>
            <a href="#" class="mt-2">Inverters, UPS & Generators</a>
            <a href="#" class="mt-2">Music Systems - Home Theatre</a>
            <a href="#" class="mt-2">Tools - Machinery - Industrial</a>

        </div>
        <div class="col-sm-3">
            <a href="#">Cameras - Digicams</a><br>
            <a href="#" class="mt-2">IPods, MP3 Players</a><br>
            <a href="#" class="mt-2">Office Supplies</a><br>
            <a href="#" class="mt-2">TV - DVD - Multimedia</a>

        </div>
        <div class="col-sm-3">
            <a href="#">Everything Else</a><br>
            <a href="#" class="mt-2">Laptops</a><br>
            <a href="#" class="mt-2">Refrigerators</a><br>
            <a href="#" class="mt-2">Video Games - Consoles</a>
        </div>
        <div class="col-sm-3">
            <a href="">Fax, EPABX, Office Equipment</a>
            <a href="#" class="mt-2">Laptops - Computers</a>
            <a href="#" class="mt-2">Security Equipment - Products</a>
            <a href="#" class="mt-2">Washing Machines</a>
        </div>
    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/entertainment.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Entertainment</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Acting Schools</a><br>
            <a href="#" class="mt-2">Make Up - Hair - Films & TV</a><br>
            <a href="#" class="mt-2">Script Writers</a><br>
            <a href="#" class="mt-2">Other Entertainment</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Actor - Model Portfolios</a>
            <a href="#" class="mt-2">Modeling Agencies</a><br>
            <a href="#" class="mt-2">Set Designers</a>

        </div>
        <div class="col-sm-3">
            <a href="#">Art Directors - Editors</a><br>
            <a href="#" class="mt-2">Musicians</a><br>
            <a href="#" class="mt-2">Sound Engineers</a>

        </div>
        <div class="col-sm-3">
            <a href="">Fashion Designers - Stylists</a>
            <a href="#" class="mt-2">Photographers - Cameraman</a>
            <a href="#" class="mt-2">Studios - Locations for hire</a>
        </div>
    </div>

    </div>

    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/home2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Home & Lifestyle</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Furniture for Home & Office</a><br>
            <a href="#" class="mt-2">Sport - Fitness Equipment</a><br>
            <a href="#" class="mt-2">Clothing - Garments</a><br>
            <a href="#" class="mt-2">Home Decor - Furnishings</a><br>
            <a href="#" class="mt-2">Coins - Stamps</a><br>
            <a href="#" class="mt-2">Music - Movies</a><br>
            <a href="#" class="mt-2">Toys - Games</a>
        </div>
        <div class="col-sm-3">
            <a href="#">Bicycle & Accessories</a><br>
            <a href="#" class="mt-2">Watches</a><br>
            <a href="#" class="mt-2">Household</a><br>
            <a href="#" class="mt-2">Paintings</a><br>
            <a href="#" class="mt-2">Health - Beauty Products</a><br>
            <a href="#" class="mt-2">Cameras - Digicams</a><br>
            <a href="#" class="mt-2">Kids Learning</a>

        </div>
        <div class="col-sm-3">
            <a href="#">Books - Magazines</a><br>
            <a href="#" class="mt-2">Jewellery</a><br>
            <a href="#" class="mt-2">Kitchenware</a><br>
            <a href="#" class="mt-2">Collectibles</a><br>
            <a href="#" class="mt-2">Gifts - Stationary</a><br>
            <a href="#" class="mt-2">Baby - Infant Products</a>


        </div>
        <div class="col-sm-3">
            <a href="">Musical Instruments</a><br>
            <a href="#" class="mt-2">Bags - Luggage</a><br>
            <a href="#" class="mt-2">Antiques - Handicrafts</a><br>
            <a href="#" class="mt-2">Fashion Accessories</a><br>
            <a href="#" class="mt-2">Toys - Games</a><br>
            <a href="#" class="mt-2">Everything Else</a>

        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/job2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Jobs</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Full Time Jobs</a><br>
            <a href="#" class="mt-2">Work From Home</a><br>
        </div>
        <div class="col-sm-3">
            <a href="#">Internships</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Part Time Jobs</a><br>
        </div>
        <div class="col-sm-3">
            <a href="">Work Abroad</a><br>
        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/metromonial2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Matrimonial</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Brides</a><br>
        </div>
        <div class="col-sm-3">
            <a href="#">Grooms</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Wedding Planners</a><br>
        </div>
        <div class="col-sm-3">

        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/mobile2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Mobiles & Tablets</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Mobile Phones</a><br>
        </div>
        <div class="col-sm-3">
            <a href="#">Accessories</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Tablets</a><br>
        </div>
        <div class="col-sm-3">
            <a href="#">Wearables</a><br>

        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/pets2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted">Pets & Pet Care</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Pet Adoption</a><br>
            <a href="#">Pet Training & Grooming</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Pet Care - Accessories</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Pet Clinics</a><br>
        </div>
        <div class="col-sm-3">
            <a href="#">Pet Foods</a><br>

        </div>
    </div>

    </div>
    <div class="row border-bottom p-2">
        <div class="col-sm-4">
            <div class="mt-4">
                <img src="assets/images/realestate2.png" style="width:12%;" alt="">
                <span class="h5 text-muted text-muted mt-2">Real Estate</span>
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row p-2">
        <div class="col-sm-3">
            <a href="#">Commercial Property for Rent</a><br>
            <a href="#">Land - Plot For Sale</a><br>
            <a href="#">Service Apartments</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Commercial Property for Sale</a><br>
            <a href="#">Paying Guest - Hostel</a><br>
            <a href="#">Vacation Rentals - Timeshare</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Flatmates</a><br>
            <a href="#">Residential - Builder floors For Rent</a><br>
            <a href="#">Villas/Bungalows for Rent</a><br>

        </div>
        <div class="col-sm-3">
            <a href="#">Houses - Apartments for Rent</a><br>
            <a href="#">Pet Training & Grooming</a><br>
            <a href="#">Villas/Bungalows for Sale</a><br>
        </div>
    </div>

    </div>

</section>

<section class="bg-white">
<div class="container mt-2">
    <!---------------third main row start--------------->
    <div class="row">
        <div class="col-lg-7">
            <nav class="navbar navbar-expand-sm px-0">
                <ul class="navbar-nav flex-wrap">
                    <li> <a href="aboutus.php" alt="any" class="nav-link"><i class="nav-item"></i> About us</a></li>
                    <li> <a href="contactus.php" alt="any" class="nav-link"><i class="nav-item"></i> Contact us</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="career.php">Careers</a></li>
                    <li class="nav-item"><a class="nav-link" href="adsales.php">Advertise with us</a></li>

                    <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                    <li> <a href="help.php" alt="any" class="nav-link"><i class="nav-item"></i> Help</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Premium Ads</a></li>
                </ul>
            </nav>
            <p class="text-justify">Widely known as India’s no. 1 online classifieds platform, MyClickOnline is all
                about you. Our aim is to empower every person in the country to independently connect with buyers
                and
                sellers online. We care about you — and the transactions that bring you closer to your dreams. Want
                to
                buy your first car? We’re here for you. Want to sell commercial property to buy your dream home?
                We’re
                here for you. Whatever job you’ve got, we promise to get it done.</p>
            <p class="text-dark bg-gray p-3">At MyClickOnline, you can buy, sell or rent anything you can think
                of.<a class="btn-custom btn-red ml-4" href="postfreead.php">Post Free Ad</a></p>
            <nav class="navbar navbar-expand-sm px-0">
                <ul class="navbar-nav flex-wrap">
                    <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Listing Policy</a></li>
                    <li class="nav-item"><a class="nav-link" href="t&c.php" alt="any" class="nav-link"><i
                                class="nav-item"></i> Terms of uses</a></li>
                    <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Privacy Policy</a></li>
                    <li class="nav-item"><a class="nav-link" href="policies.php" alt="any" class="nav-link"><i
                                class="nav-item"></i> mobile Policies</a></li>

                    <li class="nav-item"><a class="nav-link" href="sitemap.php" alt="any">Sitemap</a></li>
                    <li class="nav-item"><a class="nav-link" href="#" alt="any">News</a></li>
                </ul>
            </nav>

        </div>
        <div class="col-lg-5 border-left">
            <div class="row">
                <nav class="navbar col-sm-3">
                    <!-- Links -->
                    <ul class="navbar-nav flex-wrap">
                        <li class="nav-item"><a class="nav-link" href="">Malaysia</a></li>
                    </ul>
                </nav>
                <!-- A vertical navbar -->
                <nav class="navbar col-sm-3">
                    <!-- Links -->
                    <ul class="navbar-nav flex-wrap">
                        <li class="nav-item"><a class="nav-link" href="">Australia</a></li>
                    </ul>
                </nav>
                <nav class="navbar col-sm-3">
                    <!-- Links -->
                    <ul class="navbar-nav flex-wrap">
                        <li class="nav-item"><a class="nav-link" href="">中国</a></li>
                    </ul>
                </nav>
                <!-- A vertical navbar -->
                <nav class="navbar col-sm-3">
                    <!-- Links -->
                    <ul class="navbar-nav flex-wrap">
                        <li class="nav-item"><a class="nav-link" href="">Singapore</a></li>
                    </ul>
                </nav>
            </div>
            <div class="row flex-wrap social-icons my-3">
                <h6>Download The App :</h6>
                <span>
                    <a href="#" class="fa fa-apple font1"></a>
                    <a href="#" class="fa fa-android font2"></a>
                    <a href="#" class="fa fa-windows font3"></a>
                </span>
            </div>
        </div>
    </div>
</div><!-- container -->
</section>
</body>

</html>