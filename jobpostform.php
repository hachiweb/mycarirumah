<?php
include 'header.php'
?>
<title>Job Post</title>
   <section class="container mt-3">
         <form action="sub_jobpostform.php" method="post" enctype="multipart/form-data">
            <div class="row">
               <div class="col-sm-12 border bg-white">
                  <div class="row">
                     <h4 class="ml-3 mt-3">Post a job</h4>
                     <div class="col-sm-12">
                     <div class="form-group">
                           <label>Job Title*</label>
                           <span class="float-right">Max 150 characters</span>
                           <input type="text" class="form-control" name="job_title"
                              placeholder="Eg. Sales exicutives needed argently for...">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                     <?php
                        $parent_id= $_GET["Category"];
                        $subcategory = $_GET["Subcategory"];
                        require_once 'dbconnect.php';
                        $db     = new DB();
                        $sql    = "SELECT * FROM `category` WHERE `parent_id` = $parent_id"; 
                        $result = $db->executeQuery($sql);
                        ?>
                        <label>Job Type*</label>

                        <select name="job_type" class="custom-select mb-3">
                           <?php
                           while ($fetch = mysqli_fetch_assoc($result)) {
                               if($subcategory == $fetch['id']){
                                 echo '<option value="'.$fetch['category_title'].'" selected>'.$fetch['category_title'].'</option>';
                              }else{
                                echo '<option value="'.$fetch['category_title'].'">'.$fetch['category_title'].'</option>';
                              }
                           }
                              ?>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <label>Role*</label>
                        <select name="role" class="js-example-basic-single form-control" style="min-width: 345px;">
                           <option value="PHP Developer">PHP Developer</option>
                           <option value="HTML Developer">HTML Developer</option>
                           <option value="JAVA Developer">JAVA Developer</option>
                           <option value=".NET Developer">.NET Developer</option>
                           <option value="Android Developer">Android Developer</option>
                           <option value="IOS Developer">IOS Developer</option>
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label>Monthly salary*</label>
                                 <input type="number" name="salary_min" class="form-control" placeholder="min">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label> </label>
                                 <input type="number" name="salary_max" class="form-control mt-2" placeholder="max">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="row">
                           <div class="col-sm-6">
                              <label>Select Experiance*</label>
                              <select name="experience_min" class="custom-select mb-3">
                                 <option selected>Select Min Exp...*</option>
                                 <option value="6">0</option>
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                                 <option value="3">3</option>
                                 <option value="4">4</option>
                                 <option value="5">6</option>
                                 
                              </select>
                           </div>
                           <div class="col-sm-6">
                              <label></label>
                              <select name="experience_max" class="custom-select mt-2">
                                 <option selected>Select Max Exp...*</option>
                                 <option value="6">0</option>
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                                 <option value="3">3</option>
                                 <option value="4">4</option>
                                 <option value="5">5</option>
                                 <option value="6">6</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <label>City*</label>
                        <select name="city" class="js-example-basic-single form-control" style="min-width: 345px;">
                           <option value="Dehradun">Dehradun</option>
                           <option value="Delhi">Delhi</option>
                           <option value="Mumbai">Mumbai</option>
                           <option value="Kolkata">Kolkata</option>
                           <option value="Chennai">Chennai</option>
                           <option value="Aasam">Aasam</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <label>Locality*</label>
                        <span class="float-right">(Up to 5)</span>
                        <select name="locality" class="js-example-basic-single form-control" style="min-width: 345px;">
                              <option value="Dehradun">Dehradun</option>
                              <option value="Delhi">Delhi</option>
                              <option value="Mumbai">Mumbai</option>
                              <option value="Kolkata">Kolkata</option>
                              <option value="Chennai">Chennai</option>
                              <option value="Aasam">Aasam</option>
                        </select>
                     </div>
                  </div>
                  <div class="row mt-4">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label for="comment">Job Description*</label>
                           <span class="float-right">Max 3000 characters</span>
                           <textarea class="form-control" rows="3" name="description"
                              placeholder="Type or paste the Job Description here. Only plain text is supported."></textarea>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-3">
                     <div class="col-sm-8">
                        <h4 class="d-inline">Recruiter details</h4>
                        <span> (Cannot be changed later)</span>
                        <div class="form-group mt-2">
                           <label for="comment">Hiring for (Company name)*</label>
                           <span class="float-right">Max 50 characterss</span>
                           <textarea class="form-control" rows="1" name="company_name"
                              placeholder="Type Here...."></textarea>
                        </div>
                     </div>
                     <div class="col-sm-4"></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Email*</label>
                           <input type="email" name="email" class="form-control" placeholder="Email ID">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label>Number*</label>
                           <input type="number" name="number" class="form-control" placeholder="10 digits number">
                           <input type="hidden" name="Parent" value="<?=$parent_id;?>">
                        </div>
                     </div>
                  </div>
                  <div class="row col-sm-12">
                     <h4 class="d-inline">Type of job </h4>
                     <p>(Please choose from below)</p>
                  </div>
                  <div class="row set-pad-mar">
                     <!--4 column -->
                     <div class="col-sm-3  ">
                        <div id="border-change1" style="border:1px solid rgb(204, 204, 204);height:480px;">
                           <h6 class="d-inline ml-2 mt-2">Free</h6>
                           <h6 class="float-right mr-2 mt-1">₹ 0</h6>
                           <div style="border:5px solid rgb(206,206,206);margin-top:98px;"></div>
                           <span> Normal x applications</span>
                           <p class="text-center"><i class="fas fa-check d-inline"></i>Limited applications</p>
                           <div class="border-top" style="margin-top:195px;">
                              <div class="form-check text-center mt-3">
                                 <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Free" name="job_plan">Option 1
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3  ">
                        <div id="border-change2" style="border:1px solid rgb(204, 204, 204); height:480px;">
                           <h6 class="d-inline ml-2 mt-2">Gold</h6>
                           <h6 class="float-right mr-2 mt-1">₹ 499</h6>
                           <p>Most popular<br>10 Days</p>
                           <div style="border:5px solid rgb(218,194,81);margin-top:37px;"></div>
                           <span>10 x applications</span>
                           <p class="text-center"><i class="fas fa-check d-inline"></i>View all applications</p>
                           <p class="text-center"><i class="fas fa-check d-inline"></i>Top of page
                              promotion<br>Website,App & emails</p>
                           <p class="text-center"><i class="fas fa-check d-inline"></i>Highlighted as 'Gold'</p>
                           <div class="border-top" style="margin-top:37px;">
                              <div class="form-check text-center mt-3">
                                 <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Gold" name="job_plan">Option 2
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3 ">
                        <div id="border-change3" style="border:1px solid rgb(204, 204, 204); height:480px;">
                           <h6 class="d-inline ml-2 mt-2">Gold+Argent</h6>
                           <h6 class="float-right mr-1 mt-1">₹ 599</h6>
                           <div style="border:5px solid rgb(87,195,89);margin-top:100px;"></div>
                           <p class="text-center"><i class="fas fa-check"></i>View all applications</p>
                           <p class="text-center"><i class="fas fa-check"></i>Top of page promotion </p>
                           <p class="text-center"><i class="fas fa-check"></i>Highlighted as 'Gold'</p>
                           <div class="border-top" style="margin-top:80px;">
                              <div class="form-check text-center mt-3">
                                 <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Gold+Argent" name="job_plan">Option 3
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3  ">
                        <div id="border-change4" style="border:1px solid rgb(204, 204, 204); height:480px;">
                           <h6 class="d-inline ml-2 mt-2">Rapid Hire</h6>
                           <h6 class="float-right mr-2 mt-1">₹ 899</h6>
                           <p>Instant 60 profiles</p>
                           <div style="border:5px solid rgb(0,139,207);margin-top:60px;"></div>
                           <p class="text-center"><i class="fas fa-check"></i>Post this job and get instant candidate
                              profiles on mail</p>
                           <p class="text-center"><i class="fas fa-check"></i>Candidates filtered based on your job</p>
                           <p class="text-center"><i class="fas fa-check"></i>Candidates get your job details on SMS</p>
                           <div class="border-top" style="margin-top:17px;">
                              <div class="form-check text-center mt-3">
                                 <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Rapid Hire" name="job_plan">Option 4
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-4">
                     <div class="col-sm-12">
                        <div class="form-check-inline">
                           <label class="form-check-label">
                              <input type="checkbox" name="include_mobile" class="form-check-input" value="Yes">Include a
                              Mobile Number in the Job Title in just Rs.249/-
                           </label>
                        </div>
                        <div class="form-check-inline mt-2">
                           <label class="form-check-label">
                              <input type="checkbox" name="include_link" class="form-check-input" value="Yes">Include a
                              Link to your website in just Rs.249/-
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="text-center m-4">
                     <button class="btn btn-danger w-50" style="font-size:20px;" type="submit">Post Job</button>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <p>By posting this job, you agree to <a alt="term and conditions" href="t&c.php">Terms of
                              Use</a> and <a alt="policies" href="policies.php">Privacy Policy</a>. If you have any
                           issues, please write to <a href="#">jobsupport@quikr.com</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </section>
  
   <section class="bg-white">
   <section class="container pt-4 bg-white">
      <div class="row">
         <div class="col-sm-12">
            <table class="table table-borderless ">
               <thead>
                  <tr>
                     <th>JOBS BY ROLE</th>
                     <th>JOBS BY TYPE</th>
                     <th>JOBS BY CITY</th>
                     <th>JOBS BY INDUSTRY</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td><a href="">Delivery Boy Jobs</a></td>
                     <td><a href="">work from home</a></td>
                     <td><a href="">job in banglore</a></td>
                     <td><a href="">Agriculture Industry</a></td>
                     <td><a href="">It industry</a></td>
                  </tr>
                  <tr>
                     <td><a href="">BPO/ Telicaller job</a></td>
                     <td><a href="">Part time</a></td>
                     <td><a href="">Job in Bombay</a></td>
                     <td><a href="">Automobile Industry</a></td>
                     <td><a href="">KPO Industry</a></td>
                  </tr>
                  <tr>
                     <td><a href="">Engineering job</a></td>
                     <td><a href="">Full time</a></td>
                     <td><a href="">job in dehli</a></td>
                     <td><a href="">Banking Industry</a></td>
                     <td><a href="">Madical Industry</a></td>
                  </tr>
                  <tr>
                     <td><a href="">Markiting job</a></td>
                     <td><a href="">Internships</a></td>
                     <td><a href="">Job in channai</a></td>
                     <td><a href="">BPO Industry</a></td>
                     <td><a href="">Mining Industry</a></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </section>
   <!-- FOOTER -->
         <?php
         include 'footer.php'
         ?>
   
   <script>
      $(document).ready(function () {
         $("#border-change1").mouseover(function () {
            $(this).css("border", "2px solid rgb(0,139,207)");
         });
         $("#border-change1").mouseleave(function () {
            $(this).css("border", "2px solid rgb(204, 204, 204)");
         });
         $("#border-change2").mouseover(function () {
            $(this).css("border", "2px solid rgb(0,139,207)");
         });
         $("#border-change2").mouseleave(function () {
            $(this).css("border", "2px solid rgb(204, 204, 204)");
         });
         $("#border-change3").mouseover(function () {
            $(this).css("border", "2px solid rgb(0,139,207)");
         });
         $("#border-change3").mouseleave(function () {
            $(this).css("border", "2px solid rgb(204, 204, 204)");
         });
         $("#border-change4").mouseover(function () {
            $(this).css("border", "2px solid rgb(0,139,207)");
         });
         $("#border-change4").mouseleave(function () {
            $(this).css("border", "2px solid rgb(204, 204, 204)");
         });

      });

      // In your Javascript (external .js resource or <script> tag)
      $(document).ready(function () {
         $('.js-example-basic-single').select2();
      });
   </script>
</body>

</html>