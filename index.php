<?php
include 'header.php'
?>
<title>MyClickOnline</title>
<div class="container mt-2">
    <!-------------New main first row created----------->
    <div class="row">
        <!------------group list--------------------->
        <div class="col-md-3 col-left-bx">
            <div class="list-group exshadow left-side-menu">
                <a href="ads.php?Category=1"
                    class="list-group-item-action list-group-item-light  align_set"><img
                        src="assets/images/car.png" alt="" style="width:12%;"> Car & Bikes</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=2"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/mobile2.png" alt="" style="width:12%;">
                    Mobiles & Tablets</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=3"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/electronics.png" alt="" style="width:12%;">Electronics & Appliances</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=4"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/realestate2.png" alt="" style="width:12%;">Real Estate</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=5"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/home2.png" alt="" style="width:12%;">
                    Home & Lifestyle</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=6" class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/job2.png" alt="" style="width:12%;">
                    Jobs</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=7"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/education2.png" alt="" style="width:12%;">
                    Education & Training</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=8"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/entertainment2.png" alt="" style="width:12%;">
                    Entertainment</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=9"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/pets2.png" alt="" style="width:12%;">
                    Pets & Pets Care</a>
                <div class="border-top"></div>

                <a href="ads.php?Category=10"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/community2.png" alt="" style="width:12%;">
                    Community</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=11"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/event.png" alt="" style="width:12%;">
                    Events</a>
                <div class="border-top"></div>
                <a href="ads.php?Category=12"
                    class="list-group-item-action list-group-item-light flex-wrap align_set"><img
                        src="assets/images/metrimonial.png" alt="" style="width:12%;">
                    Metrimonial</a>
                <div class="border-top"></div>
            </div>
        </div>
        <div class="col-md-9 col-right-container">
            <div class="banner_slider_dv">
                <div class="cat_banner">
                <?php
                    $db     = new DB();
                    $leadSQL    = "SELECT * FROM `postadform` WHERE `imgname` != NULL LIMIT 4";
                    $leadResult = $db->executeQuery($leadSQL);
                    $row = mysqli_num_rows($leadResult);
                    if (empty($row)) {
                        for ($i=1; $i <5 ; $i++) { ?>
                        <div>
                            <a href="javascript:void(0);">
                                <div class="img">
                                    <img src="assets/images/demo/<?=$i;?>.jpg" alt="Banner">
                                </div>
                            </a>
                        </div>
                    <?php
                        }
                    } else {
                        while ($leadFetch = mysqli_fetch_assoc($leadResult)){
                            ?>
                       <div>
                        <a href="javascript:void(0);">
                            <div class="img">
                                <a href="ad_details.php?A=<?=$leadFetch['id']?>&P=<?=$leadFetch['parent_id']?>"><img src="<?="upload/".$leadFetch['imgname']; ?>" alt="Banner"></a>
                            </div>
                        </a>
                    </div>
                    <?php
                    }
                }
                ?>
                    
                </div>
            </div>
            <!--------------------------first row box------------------------>
            <div class="row">
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=1">Car & Bikes</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell new and used cars or bikes a variety of auto
                            services.
                        </p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=13">Bikes & Scooters</a></li>
                            <li><a href="ads.php?Category=14">Cars</a></li>
                            <li><a href="ads.php?Category=15">Commercial Vehicles</a></li>
                        </ul>
                        <ul class="mt-2">
                            <li> <a href="ads.php?Category=1" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=2">Mobile & Tablets</a></span>
                        </div>
                        <p class="font-weight-normal">Buy and sell lakhs of used Mobiles & Tablets across India</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=18">Mobile Phones</a></li>
                            <li><a href="ads.php?Category=19">Accessories</a></li>
                            <li><a href="ads.php?Category=20">Tablets</a></li>
                        </ul>
                        <ul class="mt-2">
                            <li> <a href="ads.php?Category=2" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all Categories
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=3">Electronics & Appliances</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell new and used Electronics & Appliances a variety of Home
                            Services.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=28">Refrigerators / Fridge</a></li>
                            <li><a href="ads.php?Category=36">Laptops</a></li>
                            <li><a href="ads.php?Category=44">Water Purifier</a></li>
                        </ul>
                        <ul class="mt-2">
                            <li> <a href="ads.php?Category=3" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all Categories
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=4">RealEstate</a></span>
                        </div>
                        <p class="font-weight-normal">Buy,sell & Rent for real estate Properties in india.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=70">Commercial Property for Rent</a></li>
                            <li><a href="ads.php?Category=71">Commercial Property for Sale</a></li>
                            <li><a href="ads.php?Category=72">Flatmates</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=4" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all Categories
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=5">Home & Lifestyle</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell new and used Home a variety of Home Lifestyle.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=87">Furniture for Home & Office</a></li>
                            <li><a href="ads.php?Category=146">Sport - Fitness Equipment</a></li>
                            <li><a href="ads.php?Category=95">Clothing - Garments</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=5" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all Categories
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=6">Jobs</a></span>
                        </div>
                        <p class="font-weight-normal">Thousants of jobs to choosen from in your industries.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=103">Full Time Jobs</a></li>
                            <li><a href="ads.php?Category=104">Internships</a></li>
                            <li><a href="ads.php?Category=105">Part Time Jobs</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=6" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=7">Education & Training</a></span>
                        </div>
                        <p class="font-weight-normal">Thousants of Education & Training to choosen from in your
                            industries.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=108">Career Counseling</a></li>
                            <li><a href="ads.php?Category=109">Certifications & Training</a></li>
                            <li><a href="ads.php?Category=110">Competitive Exams Coaching</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=7" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all Categories
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=8">Entertainment</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell new and used Products or books a variety of
                            Entertainment services.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=118">Career Counseling</a></li>
                            <li><a href="ads.php?Category=119">Acting Schools</a></li>
                            <li><a href="ads.php?Category=120">Actor - Model Portfolios</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=8" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=9">Pets & PetsCare</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell Pet & Pet Care a variety of Home services.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=132">Pet Adoption</a></li>
                            <li><a href="ads.php?Category=133">Pet Care - Accessories</a></li>
                            <li><a href="ads.php?Category=134">Pet Clinics</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=9" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=10">Community</a></span>
                        </div>
                        <p class="font-weight-normal">A group of people with a common characteristic living together
                            within a larger society.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=137">Announcements</a></li>
                            <li><a href="ads.php?Category=159">Car Pool - Bike Ride</a></li>
                            <li><a href="ads.php?Category=138">Charity - Donate - NGO</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=10" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=11">Events</a></span>
                        </div>
                        <p class="font-weight-normal">An event is something that happens, especially when it is unusual
                            or important.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=141">Event Management</a></li>
                            <li><a href="ads.php?Category=142">Live Events</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=11" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 px-2 mb-3">
                    <div class="border exshadow px-3">
                        <div class="crad-dv-top d-flex flex-wrap">
                            <span class="name"><a href="">MyClickOnline</a></span>
                            <span class="cat"><a href="ads.php?Category=12">Matrimonial</a></span>
                        </div>
                        <p class="font-weight-normal">Buy & sell new and used metrimonial home a variety of Home
                            services.</p>
                        <ul class="sub_cat_list">
                            <li><a href="ads.php?Category=143">Brides</a></li>
                            <li><a href="ads.php?Category=144">Grooms</a></li>
                            <li><a href="ads.php?Category=145">Wedding Planners</a></li>
                        </ul>
                        <ul>
                            <li> <a href="ads.php?Category=12" alt="any"
                                    class="text-decoration-none list-group-item-light text-primary">View all
                                    Categories</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--------------------------End row box------------------------>
            <div class="row row-7">
            <?php
                $db     = new DB();
                $Carssql    = "SELECT * FROM `postadform` WHERE `parent_id` = 1 AND `imgname` != NULL LIMIT 12";
                $Carseresult = $db->executeQuery($Carssql);
                $Carseresult2 = $db->executeQuery($Carssql);
                ?>
                <div class="col-sm-12 mt-2 border exshadow">
                    <h5 class="font-weight-normal mb-3">Cars & Bikes Trending Ads</h5>
                    <div class="owl-carousel owl-theme">
                        <?php
                            if (empty(mysqli_fetch_assoc($Carseresult2))) { ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ads.php?Category=1">
                                    <img src="assets/images/demo/Cars.jpg" alt="any">Cars & Bikes
                                </a>
                                </div>
                            <?php
                            } else {
                                while ($Carsefetch = mysqli_fetch_assoc($Carseresult)){
                                    ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ad_details.php?A=<?=$Carsefetch['id']?>&P=<?=$Carsefetch['parent_id']?>">
                                    <img src="<?="upload/".$Carsefetch['imgname']; ?>" alt="any"><?=$Carsefetch['title']; ?>
                                </a>
                                </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row row-7">
            <?php
                $db     = new DB();
                $RealEstatesql    = "SELECT * FROM `postadform` WHERE `parent_id` = 4 AND `imgname` != NULL LIMIT 12";
                $RealEstateresult = $db->executeQuery($RealEstatesql);
                $RealEstateresult2 = $db->executeQuery($RealEstatesql);
                ?>
                <div class="col-sm-12 py-3 mt-3 border exshadow">
                    <h5 class="font-weight-normal mb-3">Real Estate Trending Ads</h5>
                    <div class="owl-carousel owl-theme">
                        <?php
                            if (empty(mysqli_fetch_assoc($RealEstateresult2))) { ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ads.php?Category=4">
                                    <img src="assets/images/demo/RealEstate.jpg" alt="any">Real Estate
                                </a>
                                </div>
                            <?php
                            } else {
                                while ($RealEstatefetch = mysqli_fetch_assoc($RealEstateresult)){
                                    ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ad_details.php?A=<?=$RealEstatefetch['id']?>&P=<?=$RealEstatefetch['parent_id']?>">
                                    <img src="<?="upload/".$RealEstatefetch['imgname']; ?>" alt="any"><?=$RealEstatefetch['title']; ?>
                                </a>
                                </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row row-7">
            <?php
                $db     = new DB();
                $OtherSql    = "SELECT * FROM `postfreeadform` WHERE `imgname` != NULL LIMIT 12";
                $Otherresult = $db->executeQuery($OtherSql);
                $Otherresult2 = $db->executeQuery($OtherSql);
                ?>
                <div class="col-sm-12 py-3 mt-3 border exshadow">
                    <h5 class="font-weight-normal mb-3">Other Trending Ads</h5>
                    <div class="owl-carousel owl-theme">
                        <?php
                            if (empty(mysqli_fetch_assoc($Otherresult2))) { ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ads.php?Category=7">
                                    <img src="assets/images/demo/Others.jpg" alt="any">Other
                                </a>
                                </div>
                            <?php
                            } else {
                                while ($Otherfetch = mysqli_fetch_assoc($Otherresult)){
                                    ?>
                                <div class="img-fluid d-block max-width: 100%">
                                <a href="ad_details.php?A=<?=$Otherfetch['id']?>&P=<?=$Otherfetch['parent_id']?>">
                                    <img src="<?="upload/".$Otherfetch['imgname']; ?>" alt="any"><?=$Otherfetch['title']; ?>
                                </a>
                                </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
            <!--------------form start------------------->
            <div class="row row-7">
                <div class="col-sm-12 py-3 mt-3 border exshadow">
                    <form action="sub_index.php" method="post" enctype="multipart/form-data">
                        <h5 class="font-weight-normal m-3">I Want To Receive Alerts</h5>
                        <div class="form-group d-flex justify-content-around">
                       
                            <select name="category" class="form-control mx-2 bdr_radius">
                                <option selected>Select Category *</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Services">Services</option>
                                <option value="Home & Lifestyle">Home & Lifestyle</option>
                                <option value="Cars & Bikes">Cars & Bikes</option>
                                <option value="Real Estate">Real Estate</option>
                            </select>
                            <select name="sub_category" class="form-control mx-2 bdr_radius">
                                <option selected>Select Sub Category *</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Services">Services</option>
                                <option value="Home & Lifestyle">Home & Lifestyle</option>
                                <option value="Cars & Bikes">Cars & Bikes</option>
                                <option value="Real Estate">Real Estate</option>
                            </select>
                            <select name="city" class="form-control mx-2 bdr_radius">
                                <option selected>Select City *</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Services">Services</option>
                                <option value="Home & Lifestyle">Home & Lifestyle</option>
                                <option value="Cars & Bikes">Cars & Bikes</option>
                                <option value="Real Estate">Real Estate</option>
                            </select>
                        </div>
                        <div class="form-group d-flex justify-content-start">
                        <select name="locality" class="form-control flex-fill mx-2 bdr_radius">
                                <option selected>Select Locality *</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Services">Services</option>
                                <option value="Home & Lifestyle">Home & Lifestyle</option>
                                <option value="Cars & Bikes">Cars & Bikes</option>
                                <option value="Real Estate">Real Estate</option>
                            </select>
                            <!-- <label style="color:grey;">Email address:</label> -->
                            <input type="email" class="form-control flex-fill mx-2 bdr_radius" style="" name="email" placeholder="Email address">
                            <!-- <label for="Mobile" style="color:grey;margin-left:4px;">Mobiles:</label> -->
                            <input type="number" class="form-control flex-fill mx-2 bdr_radius"style="" name="mobile" placeholder="Mobile">
                            <div class="">
                            <button type="submit" class="btn btn-danger text-white flex-fill bdr_radius" style="width:130px;">Create Alert</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!----------------form End------------------>
            <!------------------bg-image-------------->
            <div class="row row-7">
                <div class="col-sm-12 border mt-3 pr-0 pl-0 exshadow">
                    <div class="overlay p-3" style="background-image: url('assets/images/5.jpg');">
                        <div class=""></div>
                        <p class="text-right text-black font-weight-normal z-index3">Reach audiences across 180+
                            categories
                            <br>with high purchase intent</p>
                        <a href="adsales.php" class="btn-custom btn-red z-index3">Explore</a>
                    </div>
                </div>
            </div>
            <!---------------owl carosel--------->
            <div class="row row-7">
                <div class="col-sm-12 py-3 mt-3 border exshadow">
                    <h4 class="font-weight-normal mb-3">Other MyClickOnline Brands</h4>
                    <div class="owl-carousel owl-theme">
                        <div class="img-fluid d-block"><img src="assets/images/7.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/6.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/5.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/4.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/5.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/5.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/6.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/7.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/6.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/6.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/4.jpg" alt="any"></div>
                        <div class="img-fluid d-block"><img src="assets/images/7.jpg" alt="any"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----------New main second row created----------->
    <div class="image-container">
        <div class="mt-4">
            <div class="exshadow border download-via-dv bg-white overlay">
                <div style="background-image: url('assets/images/23.jpg'); background-size:cover;">
                    <div class="row m-0">
                        <div class="col-lg-6 mt-5">
                            <a class="text-decoration-none text-center h4 text-danger" href="app_download.php" alt="not uploaded">
                                Download MY CLICK ONLINE App
                            </a>
                            <p class="ml-0 pt-0 text-info">Buy or Sell anything using the app on your mobile. Find Jobs,
                                Homes, Services and more.</p>
                            <div class="ml-lg-5">
                                <nav class="navbar navbar-expand-sm social-icons">
                                    <ul class="navbar-nav flex-wrap">
                                        <li class="nav-item"><a href="#" class="fa fa-apple font1"></a></li>
                                        <li class="nav-item"><a href="#" class="fa fa-android font2"></a></li>
                                        <li class="nav-item"><a href="#" class="fa fa-windows font3"></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4 mt-5 float-right">
                            <h3 class="text-danger">Download via SMS</h3>
                            <form class="form-inline">
                                <input type="number" class="form-control bdr_radius" placeholder="EnterMobile Number">
                                <button class="btn bg-red text-white m-3 bdr_radius" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="border-top mt-4"></div>

<!-- FOOTER -->
<?php
include 'footer.php'
?>



<script>
$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        items: 1,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 4,
                slideBy: 4,
                loop:( $('.owl-carousel').length > 4 )
            }
        }
    });
});
</script>
</body>

</html>