<?php
if(!isset($_SESSION)) 
{ 
    session_start();
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>MyClickOnline</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-signin-client_id"
    content="947272285040-mppdrbottk5nmj9pkocjs8erthnrgt65.apps.googleusercontent.com">
    <link rel="stylesheet" href="assets/css/style.css">
    <!------------------owlcarosoul---------->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/owlcarosoul/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/owlcarosoul/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
    integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- js library -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/owlcarosoul/owl.carousel.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/script.js"></script>
    
    <!------------------custom css------------->
    <link rel="stylesheet" href="assets/css/custom.css">
    <!-- login button style start -->
    <style>
        .data {
            display: none;
        }

        .data4 {
            display: none;
        }
    </style>
    <!-- login button style end -->
</head>

<body>
    <!---------sidebar---------->
    <div class="wrapper">
        <div id="mySidenav" class="sidenav exshadow">
            <a href="index.php">
                <img style="width:50%; text-align:center; top:3px; position:absolute;" src="assets/images/logo/image-logo.svg"
                alt="">
            </a>
            <a href="javascript:void(0)" class="closebtn float-right" onclick="closeNav()">&times;</a>
            <a href="#" data-target="#mymodal" data-toggle="modal" style="margin:10px;" class="data2">
                <img src="assets/images/user.png" alt="" style="width:10%;">
                <span class="h6"> Login/Register</span>
            </a>
            <div class="border-top mt-2"></div>
            <a href="profile.php">
                <img src="
                <?php if (empty($_SESSION["picture"])) { echo "assets/images/user1.png";
                } else {
                    echo $_SESSION["picture"];
                }
                ?>" id="sidebar_profile_pic" alt="" style="border-radius:100%;width:40px;">
                <span class="h6">My Account</span>
            </a>
            <a href="#"><img src="assets/images/comercial.png" alt="" style="width:15%;"> Order & Payment</a>
            <a href="profile.php?BySideBar=1"> My Ads</a>
            <div class="border-top"></div>
            <h6 class="ml-3 mt-3 text-muted">CATEGORIES</h6>
            <a href="ads.php?Category=1"><img src="assets/images/car.png" alt="" style="width:15%;"> Car & Bikes</a>
            <a href="ads.php?Category=2"><img src="assets/images/mobile2.png" alt="" style="width:17%;"> Mobiles &
            Tablets</a>
            <a href="ads.php?Category=3"><img src="assets/images/electronics.png" alt="" style="width:15%;">Electronics
            & Appliances</a>
            <a href="ads.php?Category=4"><img src="assets/images/realestate2.png" alt="" style="width:15%;"> Real
            Estate</a>
            <a href="ads.php?Category=5"><img src="assets/images/home2.png" alt="" style="width:17%;"> Homes &
            Lifestyles</a>
            <a href="ads.php?Category=6"><img src="assets/images/job2.png" alt="" style="width:15%;"> Jobs</a>

            <a href="ads.php?Category=7"><img src="assets/images/education2.png" alt="" style="width:17%;"> Education &
            Training</a>
            <a href="ads.php?Category=8"><img src="assets/images/entertainment2.png" alt="" style="width:17%;">
            Entertainment</a>

            <div id="accordion">
                <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne">More Categories</a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                        <div class="card-body">
                            <a href="ads.php?Category=9">
                                <img src="assets/images/pets2.png" alt="" style="width:17%;">
                                Pets & Pets care
                            </a>

                            <a href="ads.php?Category=10">
                                <img src="assets/images/community2.png" alt="" style="width:17%;"> Community
                            </a>
                            <a href="ads.php?Category=11">
                                <img src="assets/images/event.png" alt="" style="width:17%;">
                                Events
                            </a>
                            <a href="ads.php?Category=12">
                                <img src="assets/images/metrimonial.png" alt="" style="width:17%;"> Metrimonial
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SIDEBAR -->
            <div class="border-top"></div>
            <h6 class="ml-3 mt-3 text-muted">OTHERS</h6>
            <a href="forbusiness.php">
                <img src="assets/images/forbuseness.png" alt="" style="width:15%;">
                For Businesses
            </a>
            <a href="#">
                <img src="assets/images/deal.png" alt="" style="width:15%;">
                Deals & Offers
            </a>
            <!-- <a href="#"><i class="fas fa-sign-in-alt fa-1x"></i> Tricky</a> -->
            <a href="app_download.php">
                <img src="assets/images/download.png" alt="" style="width:15%;">
                Download Apps
            </a>
        </div>
    </div>

    <!-- top header -->
    <section class="header-container-dv">
        <section class="main-header-bar">
            <div class="container">
                <section class="top-header-bar">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="d-inline-flex">
                                <div class="mx-2 top-nav-expand">
                                    <a href="forbusiness.php" alt="any"
                                    class="text-decoration-none list-group-item-light"><i
                                    class="fas fa-briefcase fa-1x text center"></i> For Businesses</a>
                                </div>
                                <div class="mx-2 top-nav-expand">
                                    <a href="app_download.php" alt="any"
                                    class="text-decoration-none list-group-item-light">
                                    <i class="fas fa-download fa-1x text center"></i> Download The App
                                </a>
                            </div>
                            <div class="mx-2 top-nav-expand">
                                <a href="#" alt="any" class="text-decoration-none list-group-item-light">
                                    <i class="fas fa-rupee-sign fa-1x text center"></i> Discounts on Premium Ads
                                </a>
                            </div>
                            <div class="mx-2 top-nav-expand">
                                <a href="cart.php" alt="any" class="text-decoration-none list-group-item-light">
                                    <i class="fas fa-shopping-cart fa-1x text center"></i> Cart
                                </a>
                            </div>
                        </div>
                            <!-- <nav class="navbar navbar-expand-md">
                                <ul class="navbar-nav flex-wrap top-nav-expand">
                                    <li>
                                        <a href="forbusiness.php" alt="any"
                                            class="text-decoration-none list-group-item-light"><i
                                                class="fas fa-briefcase fa-1x text center"></i> For Businesses</a>
                                    </li>
                                    <li>
                                        <a href="app_download.php" alt="any"
                                            class="text-decoration-none list-group-item-light">
                                            <i class="fas fa-download fa-1x text center"></i> Download The App
                                        </a>
                                    </li>
                                    <li>
                                        <a href="case_balance.php" alt="any"
                                            class="text-decoration-none list-group-item-light">
                                            <i class="fas fa-wallet fa-1x text center"></i> Cash Balance ₹ 0
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" alt="any" class="text-decoration-none list-group-item-light">
                                            <i class="fas fa-rupee-sign fa-1x text center"></i> Discounts on Premium Ads
                                        </a>
                                    </li>
                                    <li>
                                        <a href="cart.php" alt="any" class="text-decoration-none list-group-item-light">
                                            <i class="fas fa-shopping-cart fa-1x text center"></i> Cart
                                        </a>
                                    </li>
                                </ul>
                            </nav> -->
                        </div><!-- col -->

                        <!-- Show the user profile details -->
                        <div class="userContent" style="display: none;"></div>

                        <!-- facebook login start -->
                        <a class="data4 col-xl-8 col-lg-2 float-right" data-target="#myModal3" data-toggle="modal"><i
                            class="fas fa-user-circle"></i> View Profile <br></a>
                            <div id="status" class="col-xl-8 col-lg-2 float-right"></div>
                            <a class="f-signin2 col-xl-8 col-lg-2 float-right" href="javascript:void(0);"
                            onclick="fbLogout()" id="fbLink"><i></i></a>

                            <!-- popup module3 start -->
                            <div class="modal" id="myModal3">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Welcome to MyClickOnline</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <!-- Display user profile data -->
                                            <div class="col-xl-8" id="userData"></div>
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger show_again"
                                            data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- popup module3 end -->
                            <!-- facebook login end -->
                        </div>
                    </section><!-- top-header-bar -->

                    <section class="logo-sec-bx">
                        <div class="row align-items-center">
                            <!----------collapse sidebar---------------->
                            <div class="col-xl-1">
                                <div class="h-main-left">
                                    <span style="font-size:40px;cursor:pointer" onclick="openNav()">&#9776;</span>
                                </div>
                            </div>
                            <!----------End collapse sidebar---------------->
                        <!-- <div class="col-xl-3 ">
                            <a href="index.php" alt="any" class="text-decoration-none logo-link">
                                <img alt="logo alternate" class="img-fixed rounded" src="assets/images/logo/myclogo.png">
                            </a>
                        </div> -->
                        <div class="col-xl-3">
                            <a href="index.php" alt="any" class="text-decoration-none">
                                <img alt="logo alternate" class="" src="assets/images/logo/FULL-logo.png">
                            </a>
                        </div>

                        <div class="col-xl-7">
                            <div class="text-right right-head-dv">
                                <p id="email"></p>
                                <div class="row">
                                    <div class="text-right ml-auto mr-3">
                                        <a href="postfreead.php" alt="any" class="btn-custom btn-red">Post Free Ad</a>
                                    </div>

                                    <div class="data2 mr-3">
                                        <a href="#" alt="any" class="btn-custom btn-blue" data-target="#mymodal"
                                        data-toggle="modal">
                                        <i class="fas fa-sign-in-alt fa-1x"></i>
                                        Login/Register
                                    </a>
                                </div>

                                <!-- Google login details -->
                                <div class="data">
                                    <img id="pic" class="mx-2 float-right" style="border-radius:100px;width:25%"
                                    data-target="#myModal2" class="float-right" data-toggle="modal" />
                                    <button class="btn-custom btn-red border-0 signout ">SignOut</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Google login details end-->
                <!-------------login/signup form----------->
                <div class="modal fade" id="mymodal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">

                            <div style="display:none;" id="show_forgot">
                                <div class="modal-header">
                                    <h5 class="mt-2 text-center" style="color:rgb(195,38,47)">Forgot Password</h5>
                                    <button type="button" class="close show_again" data-dismiss="modal"
                                    id="close">&times;</button>
                                </div>
                                <div class="modal-body" style="height:500px;">
                                    <div class="row">
                                        <div class="col-sm-12 p-3">
                                            <P class="text-justify font-weight-normal">We will send a OTP on your
                                            registered email id to reset your password.</P>
                                            <div id="message"></div>
                                            <div class="form-group">
                                                <label for="username" style="font-size:16px;">Email Address:</label>
                                                <input type="email" class="form-control" style="border-radius:0;"
                                                id="username" name="username">
                                            </div>

                                            <div class="">
                                                <button type="submit" id="resetPass" name="resetPass"
                                                class="btn btn-custom mx-auto w-100"
                                                style="background-color:rgb(195,38,47);color:white;">SUBMIT</button>
                                                <h6 class="text-center mt-3 text-danger show_again"
                                                style="cursor:pointer;" data-dismiss="modal" id="show_again">
                                            Cancel</h6>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <div style="display:none;" id="show_resetPass">
                            <div class="modal-header">
                                <h5 class="mt-2 text-center" style="color:rgb(195,38,47)">Reset Password</h5>
                                <button type="button" class="close show_again" data-dismiss="modal"
                                id="close">&times;</button>
                            </div>
                            <div class="modal-body" style="height:500px;">
                                <div class="row">
                                    <div class="col-sm-12 p-3">
                                        <P class="text-justify font-weight-normal"></P>
                                        <div class="form-group">
                                            <label for="email" style="font-size:16px;">OTP:</label>
                                            <input type="number" class="form-control" style="border-radius:0;"
                                            id="OTP" name="OTP">
                                        </div>
                                        <div class="form-group">
                                            <label for="email" style="font-size:16px;">New Password:</label>
                                            <input type="password" class="form-control"
                                            pattern="(?=.\d)(?=.[a-z])(?=.*[A-Z]).{8,}"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                            style="border-radius:0;" id="password" name="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="email" style="font-size:16px;">Confirm Password:</label>
                                            <input type="password" class="form-control" style="border-radius:0;"
                                            id="confirm_password" name="confirm_password">
                                        </div>

                                        <div class="">
                                            <button type="submit" id="changePass" name="changePass"
                                            class="btn btn-custom mx-auto w-100"
                                            style="background-color:rgb(195,38,47);color:white;">SUBMIT</button>
                                            <h6 class="text-center mt-3 text-danger show_again"
                                            style="cursor:pointer;" data-dismiss="modal" id="show_again">
                                        Cancel</h6>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>


                    <div id="hide_forgot">
                        <div class="modal-header">
                            <h5 class="mt-2" style="color:rgb(195,38,47)">Login/Sign Up On MyClickOnline
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                            id="close">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <div class="card">

                                        <div class="card-header">
                                            <ul class="nav nav-tabs card-header-tabs" id="bologna-list"
                                            role="tablist">
                                            <li class="nav-item text-center">
                                                <a class="nav-link active" style="width:200px;"
                                                href="#description" role="tab"
                                                aria-controls="description"
                                                aria-selected="true">Login</a>
                                            </li>
                                            <li class="nav-item text-center">
                                                <a class="nav-link" style="width:200px;" href="#history"
                                                role="tab" aria-controls="history"
                                                aria-selected="false">Register</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content mt-3">
                                            <div class="tab-pane active" id="description"
                                            role="tabpanel">
                                            <form class="ml-5">
                                                <div class="input-group md-form form-sm">
                                                    <div class="input-group-prepend">
                                                        <span
                                                        class="input-group-text bg-info lighten-3"
                                                        id="basic-text1">
                                                        <i class="fa  fa-user text-white"
                                                        aria-hidden="true"></i></span>
                                                    </div>
                                                    <input class="form-control my-0 py-1"
                                                    type="text" name="find" id="customUser"
                                                    placeholder="Username" aria-label="Search">
                                                </div>
                                                <div class="input-group md-form form-sm mt-3">
                                                    <div class="input-group-prepend">
                                                        <span
                                                        class="input-group-text bg-info lighten-3"
                                                        id="basic-text1">
                                                        <i class="fa  fa-unlock text-white"
                                                        aria-hidden="true"></i></span>
                                                    </div>
                                                    <input class="form-control my-0 py-1"
                                                    type="password" name="find" id="customPswd"
                                                    placeholder="Password" aria-label="Search">
                                                </div>
                                                <div class="text-right mt-3">
                                                    <p class=" text-info" id="click_forgot"
                                                    style="cursor:pointer;">Forgot Password?</p>
                                                </div>
                                                <div class="text-center mt-5">
                                                    <button onclick="return customLogin()"
                                                    class="btn btn-custom btn-blue"
                                                    style="width:100px;background-color:rgb(195,38,47);color:white;"
                                                    data-dismiss="modal"><i
                                                    class="fas fa-sign-in-alt"></i>
                                                Login</button>
                                            </div>
                                            <p class="mt-3">By Submitting in you agree to<a
                                                href="t&c.php" alt="">T&C</a> and <a
                                                href="policies.php" alt="">Privacy
                                            Policy</a></p>
                                        </form>
                                    </div>

                                    <div class="tab-pane" id="history" role="tabpanel"
                                    aria-labelledby="history-tab">
                                    <form action="" onsubmit="return customReg()"
                                    method="POST">
                                    <div class="form-group">
                                        <label class="" for="">Name:</label>
                                        <input type="text" style="border-radius:0;"
                                        id="reg-name" class="form-control"
                                        placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <label class="" for="">Phone Number:</label>
                                        <input type="number" style="border-radius:0;"
                                        id="reg-phone" class="form-control"
                                        placeholder="Number">
                                    </div>
                                    <div class="form-group">
                                        <label class="" for="email">Email:</label>
                                        <input type="email" style="border-radius:0;"
                                        id="reg-email" class="form-control"
                                        placeholder="Email-id">
                                    </div>
                                    <div class="form-group">
                                        <label class="" for="">Password:</label>
                                        <input type="password" style="border-radius:0;"
                                        id="reg-password" class="form-control"
                                        placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label class="" for="">Confirm Password:</label>
                                        <input type="password" style="border-radius:0;"
                                        id="reg-confirm-password"
                                        class="form-control"
                                        placeholder="Confirm Password">
                                    </div>

                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox"
                                            class="form-check-input" value="age">I
                                            confirm that I am 18 years or
                                            older
                                        </label>
                                    </div>
                                    <div class="text-center mt-4">
                                        <button type="submit"
                                        class="btn btn-custom btn-blue"
                                        style="background-color:rgb(195,38,47);color:white;">Create
                                    Account</button>
                                </div>
                            </form>


                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</div>
<div class="modal-footer justify-content-center">


    <div class="clearfix mt-2 text-center">
        <h5 class="font-weight-normal mb-3">LOGIN USING</h5>
        <a href="javascript:void(0);" onclick="fbLogin()" id="fbLink">
            <button type="button" class="exshadow btn border text-muted setted"><i
                class="fa fa-facebook-square float-left"
                style="font-size:30px;"></i>Facebook</button>
            </a>
            <div class="g-signin2 mt-3 signindiv" style="width:350px;"
            data-onsuccess="onSignIn"></div>
            <p class="text-muted mt-3">We will never post anything without your
            permission</p>
        </div>


        <!-- facbook login script start -->
        <script>
            window.fbAsyncInit = function() {
                                            // FB JavaScript SDK configuration and setup
                                            FB.init({
                                                appId: '1073011809575118', // FB App ID
                                                cookie: true, // enable cookies to allow the server to access the session
                                                xfbml: true, // parse social plugins on this page
                                                version: 'v2.8' // use graph api version 2.8
                                            });

                                            // Check whether the user already logged in
                                            FB.getLoginStatus(function(response) {
                                                if (response.status === 'connected') {
                                                    //display user data
                                                    getFbUserData();
                                                }
                                            });
                                        };

                                        // Load the JavaScript SDK asynchronously
                                        (function(d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/en_US/sdk.js";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));

                                        // Facebook login with JavaScript SDK
                                        function fbLogin() {
                                            FB.login(function(response) {
                                                if (response.authResponse) {
                                                    // Get and display the user profile data
                                                    getFbUserData();
                                                    $(".data4").css("display", "block");
                                                    $(".data2").css("display", "none");
                                                    $(".f-signin2").css("display", "block");
                                                } else {
                                                    document.getElementById('status').innerHTML =
                                                    'User cancelled login or did not fully authorize.';
                                                }
                                            }, {
                                                scope: 'email'
                                            });
                                        }

                                        // Fetch the user profile data from facebook
                                        function getFbUserData() {
                                            FB.api('/me', {
                                                locale: 'en_US',
                                                fields: 'id,first_name,last_name,email,link,gender,locale,picture'
                                            },
                                            function(response) {
                                                document.getElementById('fbLink').setAttribute("onclick",
                                                    "fbLogout()");
                                                document.getElementById('fbLink').innerHTML = 'Logout';
                                                document.getElementById('status').innerHTML =
                                                'Thanks for logging in, ' + response.first_name + '!';
                                                document.getElementById('userData').innerHTML =
                                                '<p><b>FB ID:</b> ' +
                                                response.id + '</p><p><b>Name:</b> ' + response.first_name +
                                                ' ' +
                                                response.last_name + '</p><p><b>Email:</b> ' + response
                                                .email +
                                                '</p><p><b>Gender:</b> ' + response.gender +
                                                '</p><p><b>Locale:</b> ' + response.locale +
                                                '</p><p><b>Picture:</b> <img src="' + response.picture.data
                                                .url +
                                                '"/></p><p><b>FB Profile:</b> <a target="_blank" href="' +
                                                response
                                                .link + '">click to view profile</a></p>';

                                                    // Save user data
                                                    saveUserData(response);
                                                });
                                        }
                                        // Save user data to the database
                                        function saveUserData(userData) {
                                            $.post('userdata.php', {
                                                oauth_provider: 'facebook',
                                                userData: JSON.stringify(userData)
                                            }, function(data) {
                                                return true;
                                            });
                                        }
                                        // Logout from facebook
                                        function fbLogout() {
                                            FB.logout(function() {
                                                document.getElementById('fbLink').setAttribute("onclick",
                                                    "fbLogin()");
                                                document.getElementById('fbLink').innerHTML =
                                                '<img src="fblogin.png"/>';
                                                document.getElementById('userData').innerHTML = '';
                                                document.getElementById('status').innerHTML =
                                                'You have successfully logout';
                                                $(".data4").css("display", "none");
                                                $(".data2").css("display", "block");
                                                $(".f-signin2").css("display", "none");
                                            });
                                        }
                                    </script>

                                    <!-- facbook login script end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- container -->
    </section><!-- main-header-bar -->
</section>


    <section class="form-header-dv">
        <div class="container">
            <form action="search.php" method="post" enctype="multipart/form-data">
                <div class="bg-red pt-2 px-3">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <p class="text-mute h6 text-white" for="">What are you looking for?</p>
                            <div class="input-group md-form form-sm form-1 pl-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-info lighten-3" id="basic-text1"><i
                                        class="fas fa-search text-white" aria-hidden="true"></i></span>
                                    </div>
                                    <input class="form-control my-0 py-1" type="text" name="find"
                                    placeholder="Find me..." aria-label="Search">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4">
                                <p class="text-mute h6 text-white" for="">Location</p>
                                <div class="input-group md-form form-sm form-1 pl-0">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-info lighten-3" id="basic-text1"><i
                                            class="fas fa-map-marker-alt text-white"
                                            aria-hidden="true"></i></span>
                                        </div>
                                        <input class="form-control my-0 py-1" type="text" name="location"
                                        placeholder="Country, Region or City" aria-label="Search">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-6">
                                    <p class="text-mute h6 text-white" for="">Category</p>
                                    <!--Dropdown primary-->
                                    <div class="dropdown">
                                        <!--Trigger-->
                                        <?php
                                        require_once 'dbconnect.php';
                                        $subcategory =160;
                                        $db     = new DB();
                                        $sql    = "SELECT * FROM `category` WHERE `parent_id` = 0"; 
                                        $result = $db->executeQuery($sql);
                                        ?>
                                        <select name="id" class="custom-select mb-3">
                                            <?php
                                            while ($fetch = mysqli_fetch_assoc($result)) {
                                              if($subcategory == $fetch['id']){
                                               echo '<option value="'.$fetch['id'].'" selected>'.$fetch['category_title'].'</option>';
                                           }else{
                                               echo '<option value="'.$fetch['id'].'">'.$fetch['category_title'].'</option>';
                                           }
                                       }
                                       ?>
                                   </select>
                               </div>
                               <!--/Dropdown primary-->
                           </div>
                           <div class="col-lg-3 col-md-4 col-6">
                            <p class="text-mute h6 text-white" for="">Country</p>
                            <div class="">
                                <select name="" id="SelectLanguage" class="form-control">
                                    <option value="en" selected>Select Country</option>
                                    <option value="my">Malaysia</option>
                                    <option value="en">Australia</option>
                                    <option value="en">中国</option>
                                    <option value="en">Singapore</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 mt-3">
                            <button type="submit" style="width:200px;"
                            class="btn-custom btn-yellow border-0 mb-3">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- container -->
    </section>



<script>
    $(document).ready(function() {
        $('#bologna-list a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        });
    });
</script>
<?php
if (isset($_SESSION["username"])) {
 ?>
 <input type="hidden" id="res_email" value="<?php echo $_SESSION["username"] ?>">
 <input type="hidden" id="res_name" value="<?php echo $_SESSION["name"] ?>">
 <script>
    window.custome_login = true;
    $(".g-signin2").css("display", "none");
    $(".data").css("display", "block");
    $(".data2").css("display", "none");
    //  $("#pic").attr('src',profile.getImageUrl());
    $("#email").text($("#res_email").val());
    //  $("#pic1").attr('src',profile.getImageUrl());
    $("#email1").text($("#res_email").val());
    $("#name").text($("#res_name").val());
</script>

<?php
}

?>

<script>
    $(document).ready(function() {
        $("#click_forgot").click(function() {
            $("#hide_forgot").hide();
            $("#show_forgot").show();
        });

        $(".show_again").click(function() {
            $("#hide_forgot").show();
            $("#show_forgot").hide();
            $("#show_resetPass").hide();
        });

        $("#resetPass").click(function() {
            var username = $("#username").val().trim();
            if (username != "") {
                $.ajax({
                    url: 'process_forgot.php',
                    type: 'POST',
                    data: {
                        username: username,
                        action: "generate_OTP"
                    },
                    success: function(response) {
                        var msg = "";
                        if (response == 1) {
                            $("#show_forgot").hide();
                            $("#show_resetPass").show();
                        } else {
                            msg = "Sorry, This email was incorrect.";
                        }
                        $("#message").html(msg);
                    }
                });
            }
        });

        $("#changePass").click(function() {
            var OTP = $("#OTP").val().trim();
            var pswd = $("#password").val().trim();
            var cf_pswd = $("#confirm_password").val().trim();
            if (pswd != cf_pswd) {
                $("#message").html("Confirm Password Not Match!");
            }
            if (OTP != "" && pswd != "" && cf_pswd != "" && pswd == cf_pswd) {
                $.ajax({
                    url: 'process_forgot.php',
                    type: 'POST',
                    data: {
                        OTP: OTP,
                        pswd: pswd,
                        cf_pswd: cf_pswd,
                        action: "verify_OTP"
                    },
                    success: function(response) {
                        var msg = "";
                        if (response == 1) {
                            window.location = "index.php"
                        } else {
                            msg = "OTP Not Match!";
                        }
                        $("#message").html(msg);
                    }
                });
            }
        });

    });
</script>
    <!--forgot password-->