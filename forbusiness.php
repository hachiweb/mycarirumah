<?php
include 'header.php'
?>
<title>For Business</title>
   <section class="container-fluid">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-12"
            style="background-image: url('assets/images/20.jpg'); background-size:cover;width:100%;padding:40px 10px;">
            <div class="row">
               <div class="col-sm-5 align-on-img">
                  <h1>Get More Customers - <br>Quickly & Cost Effectively</h1>
                  <p class="set">Advertise on MyClickOnline Now!</p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="container">
      <div class="row">
         <div class="col-sm-8 py-2">
            <div class="row exshadow bg-white mx-1 txt-text-less">
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center py-3 bg-set-clr" id="set1">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Product Seller</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout1" role="button">List your Product</a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set2">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Advertisers</h5>
                  <a href="adsales.php" class="btn btn-default1 set-btn" id="mouseout2" role="button">Advertisers now</a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set1">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Owners/ Brokers/ Builders</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout1" role="button"> Buy Services </a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set2">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Service Providers</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout2" role="button">List a Services</a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set1">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Consultants/Recruiters</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout1" role="button">Hire Condidates</a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set2">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Car/Bike Dealer</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout2" role="button">Sell a Car/Bike</a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set1">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Institute</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout1" role="button"> Explore </a>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center p-3 bg-set-clr" id="set2">
                  <i class="fa fa-car fa-2x"></i>
                  <h5 class="text-dark">Others</h5>
                  <a href="#" class="btn btn-default1 set-btn" id="mouseout2" role="button"> Explore </a>
               </div>
            </div>
         </div>

         <div class="col-lg-4 col-md-4 col-sm-6 col-12 p-2">
            <div class="row exshadow bg-white mx-1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <h4 class="text-center mt-4 text-dark">Have Questions?</h4>
                  <p class="text-center">Let us help you</p>
                  <form action="sub_forbusiness.php" method="post" enctype="multipart/form-data" calss="padd-right">
                        <div class="form-group m-2">
                           <label type="Name" class="mt-2 mb-0">Name:</label>
                           <input type="name" class="form-control bdr_radius px-2" name="Name"aceholder="Name">
                           <label for="Email" class="mt-2 mb-0">Email id:</label>
                           <input type="email" class="form-control bdr_radius px-2" name="Email" placeholder="Email address">
                           <label for="Mobile" class="mt-2 mb-0">Mobiles:</label>
                           <input type="mobile" class="form-control bdr_radius px-2" name="Mobile" placeholder="Mobile">
                           <label type="Name" class="mt-2 mb-0">Locality:</label>
                           <select name="Locality" class="custom-select bdr_radius px-2">
                              <option selected>Select</option>
                              <option value="Dehradun">Dehradun</option>
                              <option value="Delhi">Delhi</option>
                              <option value="Mumbai">Mumbai</option>
                              <option value="Chennai">Chennai</option>
                              <option value="Kolkata">Kolkata</option>
                           </select>
                           <label for="" class="mt-2 mb-0" >Subject:</label>
                           <input type="text" class="form-control bdr_radius px-2" name="Subject" placeholder="Subject">
                           <label for="comment" class="mt-2 mb-0" >Your Query:</label>
                           <textarea class="form-control bdr_radius px-2" rows="5" name="Query"></textarea>
                           <div class="text-center p-3">
                           <button type="submit" class="btn btn-danger bdr_radius">Submit Query</button>
                           </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
  <!-- FOOTER -->
<?php
include 'footer.php'
?>n>
   <script>
      $(document).ready(function () {
            $("#set1").mouseover(function () {
            $("#mouseout1").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout1").css("color", "white");
            });
            $("#set1").mouseout(function () {

            $("#mouseout1").css("background-color", "white");
            $("#mouseout1").css("color", "rgb(0, 131, 202)");
            });

            $("#set2").mouseover(function () {
            $("#mouseout2").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout2").css("color", "white");
            });
            $("#set2").mouseout(function () {
            $("#mouseout2").css("background-color", "white");
            $("#mouseout2").css("color", "rgb(0, 131, 202)");
            });

            $("#set3").mouseover(function () {
            $("#mouseout3").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout3").css("color", "white");
            });
            $("#set3").mouseout(function () {
            $("#mouseout3").css("background-color", "white");
            $("#mouseout3").css("color", "rgb(0, 131, 202)");
            });

            $("#set4").mouseover(function () {
            $("#mouseout4").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout4").css("color", "white");
            });
            $("#set4").mouseout(function () {
            $("#mouseout4").css("background-color", "white");
            $("#mouseout4").css("color", "rgb(0, 131, 202");
            });

            $("#set5").mouseover(function () {
            $("#mouseout5").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout5").css("color", "white");
            });
            $("#set5").mouseout(function () {
            $("#mouseout5").css("background-color", "white");
            $("#mouseout5").css("color", "rgb(0, 131, 202)");
            });

            $("#set6").mouseover(function () {
            $("#mouseout6").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout6").css("color", "white");
            });
            $("#set6").mouseout(function () {
            $("#mouseout6").css("background-color", "white");
            $("#mouseout6").css("color", "rgb(0, 131, 202)");
            });

            $("#set7").mouseover(function () {
            $("#mouseout7").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout7").css("color", "white");
            });
            $("#set7").mouseout(function () {
            $("#mouseout7").css("background-color", "white");
            $("#mouseout7").css("color", "rgb(0, 131, 202)");
            });

            $("#set8").mouseover(function () {
            $("#mouseout8").css("background-color", "rgb(0, 131, 202)");
            $("#mouseout8").css("color", "white");
            });
             $("#set8").mouseout(function () {
            $("#mouseout8").css("background-color", "white");
            $("#mouseout8").css("color", "rgb(0, 131, 202)");
            });

      });
</script>
</body>

</html>