<?php
include 'header.php'
?>
<title>ADS</title>
    <?php
        require_once 'dbconnect.php';
        $id = $_GET["Category"];
        $db     = new DB();
        $sql    = "SELECT * FROM `category` WHERE `id` = $id"; 
        $result = $db->executeQuery($sql);
        while ($fetch = mysqli_fetch_assoc($result)) {
            if($id == $fetch['id']){ ?>
            <section class="container mt-5">
                <h3 class="text-dark">Showing results for <strong><?php echo $fetch['category_title']; ?></strong></h3>
        <?php
            }
        }
        ?>
        <?php
            if($id=='6'){
            $sql1    = "SELECT * FROM `jobpostform` WHERE `is_active` =1 ORDER BY `id` DESC"; 
            $result1 = $db->executeQuery($sql1);
            echo '<div class="row">';
            while ($fetch1 = mysqli_fetch_assoc($result1)) {?>
                <div class="col-xl-4 col-lg-4 col-sm-6 col-md-4 mb-4">
                <div class="border exshadow p-3 bg-white txt-job">
                <h5 class='mt-2 text-primary text-ellipsis' style='text-transform:uppercase;'><strong><?=$fetch1['job_title']." - " .$fetch1['company_name']?></strong></h5>
               <h6>Salary:&nbsp;<span>₹ <?=$fetch1['salary_min']." - " .$fetch1['salary_max']?> /month<br></span></h6>
               <h6>Job Type:&nbsp;<span><?=$fetch1['job_type']?></span></h6>
               <h6>Role:&nbsp;<span><?=$fetch1['role']?></span></h6>
               <h6>Company:&nbsp;<span><?=$fetch1['company_name']?></span></h6>
               <h6>Location:&nbsp;<span><?=$fetch1['city']." - " .$fetch1['locality']?></span></h6>
               <div class="text-right my-auto">
                <a href="ad_details.php?A=<?=$fetch1['id']?>&P=6" class="btn btn-primary btn-block my-3">
               See Details </a>
               </div>
                </div>
               </div>
                <?php } ?>
               </div>
               <?php }
            elseif ($id=='1' or $id=='2' or $id=='3' or $id=='4' or $id=='5' or $id>12){
            $sql2    = "SELECT * FROM `postadform` WHERE parent_id = $id AND `is_active` =1 ORDER BY `id` DESC";
            $result2 = $db->executeQuery($sql2);?>
            <div class="row ads-boxes">
                <?php 
            while ($fetch2 = mysqli_fetch_assoc($result2)) { ?>


                    <div class="col-sm-4 mt-3">
                    <div class="card exshadow">
                    <?php $iMgF1 = (explode(",",$fetch2['imgname']));
                    for ($i=1; $i <2 ; $i++) { 
                        if (empty($iMgF1)) {
                            # sorry nothing to do.
                        } else {
                             ?>
                            <span class="contain">
                                <a href="ad_details.php?A=<?=$fetch2['id']?>&P=<?=$fetch2['parent_id']?>"><img src="<?php echo "upload/".$iMgF1[$i]; ?>" class="card-img-top p-2 img-ads-dimension image" alt="...">
                                <div class="middle">
                                        <div class="text">
                                        <button type="button" class="btn btn-secondary border-white"><span class="mx-3 my-2">View</span></button>                                        </div>
                                </div>
                                </a>
                            </span>
                        <?php } } ?>
                        <div class="card-body ads-body">
                            <a href="" class="font-weight-bold d-block"><?=$fetch2['title'];?></a>
                            <a href=""><span><?=$fetch2['saler_type'];?> </span>/<span> <?=$fetch2['condition'];?> </span></a>
                            <div class="d-flex justify-content-between price-set-add mt-3">
                                <span class="text-primary">₹ <?=$fetch2['price'];?></span>
                            </div>
                        </div>
                            <div class="border-top"></div>
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                            <div>
                            <p>Posted Date: <br><?= date('d F, Y',strtotime($fetch2['posted_at'])) ;?></p>
                            </div>
                            <div class="my-auto">
                                <a href="ad_details.php?A=<?=$fetch2['id']?>&P=<?=$fetch2['parent_id']?>" class="btn btn-outline-primary"><span>View</span></a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php
            } ?> </div> <?php 
            }
            else{
                $sql3    = "SELECT * FROM `postfreeadform` WHERE parent_id = $id AND `is_active` =1 ORDER BY `id` DESC";
            $result3 = $db->executeQuery($sql3);?>
            <div class="row ads-boxes">
          <?php   while ($fetch3 = mysqli_fetch_assoc($result3)) { ?>
                <div class="col-sm-4 mt-3">
                    <div class="card exshadow">
                    <?php $iMgF3 = (explode(",",$fetch3['imgname']));
                    for ($i=1; $i <2 ; $i++) { 
                        if (empty($iMgF3)) {
                            # sorry nothing to do.
                        } else {
                             ?>
                             <span class="contain">
                                <a href="ad_details.php?A=<?=$fetch3['id']?>&P=<?=$fetch3['parent_id']?>"><img src="<?=!empty($iMgF3)?"upload/".$iMgF3[$i]:"assets/images/demo/NIA.png"; ?>" class="card-img-top p-2 img-ads-dimension image" alt="...">
                                    <div class="middle">
                                        <div class="text">
                                            <button type="button" class="btn btn-secondary border-white"><span class="mx-3 my-2">View</span></button>
                                        </div>
                                    </div>
                                </a>
                            </span>
                
                        <?php } } ?>
                        <div class="card-body ads-body txt-job">
                            <a href=""><h5 class="font-weight-bold text-primary" style='text-transform:uppercase;'><?=$fetch3['title'];?></h5> </a>
                            <a href=""> <h6 class="font-weight-bold text-dark mt-2"><span><?=$fetch3['education'];?> </span>/<span> <?=$fetch3['board'];?> </span></h6> </a>
                            <h6>Institute Name:&nbsp;<span><?=$fetch3['institute_name']?></span></h6>                            
                            <h6>Subject:&nbsp;<span><?=$fetch3['subject']?></span></h6>
                            <h6>Standard:&nbsp;<span><?=$fetch3['standard']?></span></h6>
                            <h6>Institute Website:&nbsp;<span><?=$fetch3['institute_website']?></span></h6>
                        </div>
                            <div class="border-top"></div>
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                            <div>
                            <p>Posted Date: <br><?= date('d F, Y',strtotime($fetch3['posted_at'])) ;?></p>
                            </div>
                            <div class="my-auto">
                                <a href="ad_details.php?A=<?=$fetch3['id']?>&P=<?=$fetch3['parent_id']?>" class="btn btn-outline-primary"><span>View</span></a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php } ?>
                  </div>
                        <?php } ?>
    </section>

    <br>
    <section class="footer-sec bg-white">
    <div class="container">
        <!---------------third main row start--------------->
        <div class="row">
            <div class="col-lg-7">
                <nav class="navbar navbar-expand-sm px-0">
                    <ul class="navbar-nav flex-wrap">
                        <li> <a href="aboutus.php" alt="any" class="nav-link"><i class="nav-item"></i> About us</a></li>
                        <li> <a href="contactus.php" alt="any" class="nav-link"><i class="nav-item"></i> Contact us</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="career.php">Careers</a></li>
                        <li class="nav-item"><a class="nav-link" href="adsales.php">Advertise with us</a></li>

                        <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                        <li> <a href="help.php" alt="any" class="nav-link"><i class="nav-item"></i> Help</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Premium Ads</a></li>
                    </ul>
                </nav>
                <p class="text-justify">Widely known as Malaysia's no. 1 online classifieds platform, MyClickOnline is all
                    about you. Our aim is to empower every person in the country to independently connect with buyers
                    and
                    sellers online. We care about you — and the transactions that bring you closer to your dreams. Want
                    to
                    buy your first car? We’re here for you. Want to sell commercial property to buy your dream home?
                    We’re
                here for you. Whatever job you’ve got, we promise to get it done.</p>
                <p class="text-dark bg-gray p-3">At MyClickOnline, you can buy, sell or rent anything you can think
                    of.<a class="btn btn-danger ml-4" href="postfreead.php">Post Free Ad</a></p>
                    <nav class="navbar navbar-expand-sm px-0">
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Listing Policy</a></li>
                            <li class="nav-item"><a class="nav-link" href="t&c.php" alt="any" class="nav-link"><i
                                class="nav-item"></i> Terms of uses</a></li>
                                <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Privacy Policy</a></li>
                                <li class="nav-item"><a class="nav-link" href="policies.php" alt="any" class="nav-link"><i
                                    class="nav-item"></i> mobile Policies</a></li>

                                    <li class="nav-item"><a class="nav-link" href="#" alt="any">Sitemap</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#" alt="any">News</a></li>
                                </ul>
                            </nav>

                        </div>
                        <div class="col-lg-5 border-left">
                        <div class="row">
                            <nav class="navbar col-sm-3">
                                <!-- Links -->
                                <ul class="navbar-nav flex-wrap">
                                    <li class="nav-item"><a class="nav-link" href="">Malaysia</a></li>
                                </ul>
                            </nav>
                            <!-- A vertical navbar -->
                            <nav class="navbar col-sm-3">
                                <!-- Links -->
                                <ul class="navbar-nav flex-wrap">
                                    <li class="nav-item"><a class="nav-link" href="">Australia</a></li>
                                </ul>
                            </nav>
                            <nav class="navbar col-sm-3">
                                <!-- Links -->
                                <ul class="navbar-nav flex-wrap">
                                    <li class="nav-item"><a class="nav-link" href="">中国</a></li>
                                </ul>
                            </nav>
                            <!-- A vertical navbar -->
                            <nav class="navbar col-sm-3">
                                <!-- Links -->
                                <ul class="navbar-nav flex-wrap">
                                    <li class="nav-item"><a class="nav-link" href="">Singapore</a></li>
                                </ul>
                            </nav>
                        </div>
                            <div class="row flex-wrap social-icons mt-3">
                                <h6>Follow Us :</h6>
                                <span>
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-linkedin"></a>
                                    <a href="#" class="fa fa-google"></a>
                                    <a href="#" class="fa fa-youtube"></a>
                                </span>
                            </div>
                            <div class="row flex-wrap social-icons mt-3">
                                <h6>Download The App :</h6>
                                <span>
                                    <a href="#" class="fa fa-apple font1"></a>
                                    <a href="#" class="fa fa-android font2"></a>
                                    <a href="#" class="fa fa-windows font3"></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div><!-- container -->
            </section><!-- footer-sec -->



            <script>
                $(document).ready(function() {
                    $('.owl-carousel').owlCarousel({
                        loop: true,
                        margin: 10,
                        nav: true,
                        dots: false,
                        items: 1,
                        responsiveClass: true,
                        responsive: {
                            0: {
                                items: 1
                            },
                            768: {
                                items: 4
                            }
                        }
                    });
                });
            </script>
        </body>

        </html>