<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
require_once 'config.php';
include_once '../dbconnect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Thank You</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <link rel="stylesheet" href="css/jquery.auto-complete.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body style="background-color:#eeeeee">
<div class="jumbotron text-center">
<?php
$ad_plan_name = $_SESSION['ad_plan_name'];
$ad_plan_month = $_SESSION['ad_plan_month'];
$ad_plan_price = $_SESSION['ad_plan_price'];
$currency = $_SESSION['currency'];
$email = $_SESSION["username"];
$payment_id = $statusMsg = '';
$db       = new DB(); 
$ordStatus = 'error'; 
 
// Check whether stripe token is not empty 
if(!empty($_POST['stripeToken'])){ 
     
    // Retrieve stripe token, card and user info from the submitted form data 
    $token  = $_POST['stripeToken']; 
    $name = $_POST['name']; 
    $card_number = $_POST['card_number']; 
    $card_exp_month = $_POST['card_exp_month']; 
    $card_exp_year = $_POST['card_exp_year']; 
    $card_cvc = $_POST['card_cvc']; 
     
    // Include Stripe PHP library 
    require_once 'stripe-php/init.php'; 
     
    // Set API key 
    \Stripe\Stripe::setApiKey(STRIPE_API_KEY); 
     
    // Add customer to stripe 
    $customer = \Stripe\Customer::create(array( 
        'email' => $email, 
        'source'  => $token 
    )); 
     
    // Unique order ID 
    $orderID = strtoupper(str_replace('.','',uniqid('', true))); 
     
    // Convert price to cents 
    $ad_plan_price = ($ad_plan_price*100); 
     
    // Charge a credit or a debit card 
    $charge = \Stripe\Charge::create(array( 
        'customer' => $customer->id, 
        'amount'   => $ad_plan_price, 
        'currency' => $currency, 
        'description' => $ad_plan_name, 
        'metadata' => array( 
            'order_id' => $orderID 
        ) 
    )); 
     
    // Retrieve charge details 
    $chargeJson = $charge->jsonSerialize(); 
 
    // Check whether the charge is successful 
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){ 
        // Order details  
        $transactionID = $chargeJson['balance_transaction']; 
        $paidAmount = $chargeJson['amount']; 
        $paidCurrency = $chargeJson['currency']; 
        $payment_status = $chargeJson['status'];
        // Insert tansaction data into the database 
        $sql = "INSERT INTO orders(name,username,card_number,card_exp_month,card_exp_year,ad_plan_name,ad_plan_month,ad_plan_price,item_price_currency,paid_amount,paid_amount_currency,txn_id,payment_status) VALUES('".$name."','".$email."','".$card_number."','".$card_exp_month."','".$card_exp_year."','".$ad_plan_name."','".$ad_plan_month."','".$ad_plan_price."','".$currency."','".$paidAmount."','".$paidCurrency."','".$transactionID."','".$payment_status."')"; 
        $insert = $db->executeQuery($sql); 
        $payment_id = $db->insert_id(); 
         
        // If the order is successful 
        if($payment_status == 'succeeded'){ 
            $ordStatus = 'success'; 
            $statusMsg = 'Your Payment has been Successful!'; 
        }else{ 
            $statusMsg = "Your Payment has Failed!"; 
        } 
    }else{ 
        //print '<pre>';print_r($chargeJson); 
        $statusMsg = "Transaction has been failed!"; 
    } 
}else{ 
    $statusMsg = "Error on form submission."; 
} 
?>

<div class="container">
    <div class="status">
        <?php if(!empty($payment_id)){ ?>
        
          <h1 class="<?php echo $ordStatus; ?>"><?php echo $statusMsg; ?></h1>
		<div>
            <h4>Payment Information</h4>
            <p><b>Reference Number:</b> <?php echo $payment_id; ?></p>
            <p><b>Transaction ID:</b> <?php echo $transactionID; ?></p>
            <p><b>Paid Amount:</b> <?php echo $paidAmount.' '.$paidCurrency; ?></p>
            <p><b>Payment Status:</b> <?php echo $payment_status; ?></p>
        </div>
        <div>
            <h4>Product Information</h4>
            <p><b>Plan Name:</b> <?php echo $ad_plan_name; ?></p>
            <p><b>Plan Month:</b> <?php echo $ad_plan_month; ?></p>
            <p><b>Price:</b> <?php echo '$'.$ad_plan_price.' '.$currency; ?></p>
            <?php }else{ ?>
                <h1 class="error">Your Payment has Failed</h1>
            <?php } ?>
        </div>
    </div>
</div>
  <h1 class="display-3">Thank You!</h1>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="index.php" role="button">Continue to homepage</a>
  </p>
</div>   
</body>
</html>