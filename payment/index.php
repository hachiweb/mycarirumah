<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:../index.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
    }
include('config.php');
$tempData = $_POST['adPlanData'];
$adplan = explode(",",$tempData);
$ad_plan_price = $adplan[0];
$ad_plan_name  = $adplan[1];
$ad_plan_month = $adplan[2];
$currency = "USD";
if (!empty($_POST)) {
    $_SESSION['ad_plan_name'] = $ad_plan_name;
    $_SESSION['ad_plan_month'] = $ad_plan_month;
    $_SESSION['ad_plan_price'] = $ad_plan_price;
    $_SESSION['currency'] = $currency;
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    <!-- jQuery is used only for this example; it isn't required to use Stripe -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Buy Ad Plan</title>
</head>
<body>


    <?php //include('../header.php'); ?>
    <section class="payment-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="panel panel-bx-sec">
                        <div class="panel-heading">
                            <h3 class="panel-title">Charge <?php echo '$'.$ad_plan_price; ?> for your Ad Plan</h3>

                            <!-- Product Info -->
                            <p><b>Plan Name:</b> <?php echo $ad_plan_name; ?></p>
                            <p><b>Plan Month:</b> <?php echo $ad_plan_month; ?></p>
                            <p><b>Price:</b> <?php echo '$'.$ad_plan_price.' '.$currency; ?></p>
                        </div>
                        <div class="panel-body">
                            <!-- Display errors returned by createToken -->
                            <div class="payment-status"></div>

                        </div>
                        <!-- Payment form -->
                        <form action="payment.php" method="POST" id="paymentFrm">
                            <div class="form-group">
                                <label>NAME</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" required="" autofocus="">
                            </div>
                            <div class="form-group">
                                <label>CARD NUMBER</label>
                                <input type="text" class="form-control"  name="card_number" id="card_number" placeholder="1234 1234 1234 1234" autocomplete="off" required="">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="d-block">EXPIRY DATE</label>
                                        <div class="form-inline">
                                            <input type="text"  class="form-control" name="card_exp_month" id="card_exp_month" placeholder="MM" required="">

                                            <input type="text"  class="form-control text-left mt-3" name="card_exp_year" id="card_exp_year" placeholder="YYYY" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>CVC CODE</label>
                                        <input type="text"  class="form-control" name="card_cvc" id="card_cvc" placeholder="CVC" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn-custom btn-blue border-0 mt-4" id="payBtn">
                                    Submit Payment
                                </button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </section>

</body>
<script>
// Set your publishable key
Stripe.setPublishableKey('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

// Callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        // Enable the submit button
        $('#payBtn').removeAttr("disabled");
        // Display the errors on the form
        $(".payment-status").html('<p>'+response.error.message+'</p>');
    } else {
        var form$ = $("#paymentFrm");
        // Get token id
        var token = response.id;
        // Insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        // Submit form to the server
        form$.get(0).submit();
    }
}

$(document).ready(function() {
    // On form submit
    $("#paymentFrm").submit(function() {
        // Disable the submit button to prevent repeated clicks
        $('#payBtn').attr("disabled", "disabled");

        // Create single-use token to charge the user
        Stripe.createToken({
            number: $('#card_number').val(),
            exp_month: $('#card_exp_month').val(),
            exp_year: $('#card_exp_year').val(),
            cvc: $('#card_cvc').val()
        }, stripeResponseHandler);

        // Submit from callback
        return false;
    });
});
</script>

<?php //include('../footer.php'); ?>

</html>