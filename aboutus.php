<?php
include 'header.php'
?>
   <title>About Us</title>
 
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12"
               style="background-image: url('assets/images/16.jpg'); background-size:cover;width:100%;">
               <h1 style="text-align:center;padding:100px;color:white">About Us</h1>
            </div>
         </div>
      </div>
      <section class="bg-white">
      <div class="container py-2">
         <nav class="navbar navbar-expand-sm static-nav anav">
            <ul class="navbar-nav">
               <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
               <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
               <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
               <li class="nav-item"><a class="" href="t&c.php">Terms & Conditions</a></li>
               <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
            </ul>
         </nav>
      </div>
      </section>
      <div class="border-top"></div>
      <div class="container p-3 text-muted text-justify">
         <h4>Our story begins with you</h4>
         <p>MyClickOnline is all about you - Our aim is to empower every person in the country to independently connect
            with buyers and sellers online. Brainchild of Pranay Chulet, MyClickOnline is widely known as India’s no. 1
            online classifieds platform - and there’s a reason behind that. We care about you - and the transactions that
            bring you closer to your dreams. Want to buy your first car? We’re here for you. Want to sell commercial
            property to buy a home for your family? We’re here for you. Whatever job you’ve got, we promise to get it done.
         </p>
         <p>Founded in 2008 and headquartered in Bangalore, MyClickOnline has left a memorable footprint in over 1000
            cities. And we continue to work towards building the future of trading and e-commerce.</p>
         <div class="static-content-section">
            <ul>
               <li>In 2014 we raised about $150 million split into different sources - In a financing round led by Tiger
                  Global, we raised $90 million, and in yet another round led by Kinnevik, we raised $60 million. Till now,
                  MyClickOnline has raised $350 million in 7 rounds, to fulfill the dream of disrupting the Indian
                  e-commerce market to enable and empower people.</li>
               <li>We’ve had the good fortune to be backed by some of the world’s most reputed investment giants. The road
                  to our milestones was paved with the support of Kinnevik, Matrix Partners India, Omidyar Network, Norwest
                  Venture Partners, Nokia Growth Partners, Warburg Pincus and eBay Inc.</li>
            </ul>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12"
               style="background-image: url('assets/images/17.png'); background-size:cover;width:100%;">
               <div class="align-making">
                  <h2 class="txt_media">What makes us<br>
                     MY CLICK ONLINE?
                  </h2>
                  <p class="textline text-justify">Wondering what it would be like to join the revolution<br>
                     that is MyClickOnline? Hear it straight from the people who’ve<br>
                     worked on bringing it to you.
                  </p>
                  <a href="#" aly="any" data-toggle="modal" data-target="#"
                     class="btn btn-danger border border-white exshadow">
                     <i class="fa fa-youtube fa-fw"></i>
                     <span >Play Video</span>
                  </a>
               </div>
               <!-- <div class="marginset">
                  <p class=" text-white">MyClickOnline today is at the cusp of something really beautiful… We are
                     much bigger than we were before but we will be much bigger than we are today.
                     We can offer people a chance to be an entrepreneur in an environment that can actually give you a much
                     bigger platform.
                  </p>
               </div> -->
            </div>
         </div>
      </div>
      <div class="container text-muted py-4 text-justify">
         <h4>Ushering in the new age of MyClickOnline</h4>
         <p class="text-justify">In July 2015, we changed our brand identity to reflect who we are - dynamic, flexible and ready to adapt to the
            spirit of the contemporary Indian audience. In addition to that, we recently appointed actors Ranveer Singh and
            Suriya as our brand ambassadors. We’ve also launched MyClickOnline Cars - venturing into the diverse range of
            automobiles i.e. cars, motorbikes and commercial vehicles.</p>
         <p class="text-justify">Our idea is to consistently keep adding something game-changing and worthwhile to your MyClickOnline experience
            - whether it is MyClickOnlineNXT, the freedom to buy or sell without risking your privacy, or the missed call
            service, to give MyClickOnline access to the masses who aren’t online today but would have internet access in
            the near future.
         </p>
      </div>
      <section class="bg-white">
      <div class="container">
         <div class="row py-5 text-justify">
            <h4 class="text-muted mx-3">Recognition & Awards</h4>
            <p class="text-justify mx-3">We are proud to be India’s leading platform to sell, buy, rent or find something. Our goal is to help our
               community of buyers and sellers address their needs in the simplest and fastest way. We do this by listening
               to our community, fostering innovation and keeping the platform simple. The awards and recognition we
               receive is thanks to the effort and success achieved by millions of individuals and small businesses across
               India.</p>
            <div class="mx-auto">
               <nav class="navbar navbar-expand-sm ">
                  <ul class="navbar-nav flex-wrap">
                     <li class="ml-0 mt-3">
                        <a class="text-decoration-none" href="#">
                           <div class="imgset"><img class="rounded " src="assets/images/18.jpg" alt="">
                              <span class="d-block text-center text-dark mt-2 ">"always on global"<br>250 winner
                                 <br>2009-2010</span>
                           </div>
                        </a>
                     </li>
                     <li class="mset mt-3">
                        <a class="text-decoration-none" href="#">
                           <div class="imgset"><img class="rounded " src="assets/images/18.jpg" alt="">
                              <span class="d-block text-center text-dark mt-2">"always on global"<br>250 winner
                                 <br>2009-2010</span>
                           </div>
                        </a>
                     </li>
                     <li class="mset mt-3">
                        <a class="text-decoration-none" href="#">
                           <div class="imgset"><img class="rounded " src="assets/images/18.jpg" alt="">
                              <span class="d-block text-center text-dark mt-2">"always on global"<br>250 winner
                                 <br>2009-2010</span>
                           </div>
                        </a>
                     </li>
                     <li class="mset mt-3">
                        <a class="text-decoration-none" href="#">
                           <div class="imgset"><img class="rounded " src="assets/images/18.jpg" alt="">
                              <span class="d-block text-center text-dark text-dark mt-2">"always on global"<br>250 winner
                                 <br>2009-2010</span>
                           </div>
                        </a>
                     </li>
                     <li class="mset mr-0 mt-3">
                        <a class="text-decoration-none" href="#">
                           <div class="imgset"><img class="rounded " src="assets/images/18.jpg" alt="">
                              <span class="d-block text-center text-dark mt-2">"always on global"<br>250 winner
                                 <br>2009-2010</span>
                           </div>
                        </a>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
         <div class="border-top border-dark p-3"></div>
      </div>
</div>
      
      <section class="bg-gray">
      <div class="container">
         <div class="py-4">
            <h3>Ask and ye shall receive</h3>
            <p>If you’ve got any ad-posting related questions, don’t hesitate to drop us a note at <a
                  href="">support@MyClickOnline.com</a> </p>
            <p>Rest assured, you can count on us. Now and always. Our support team will be happy to address your queries.
               You can also give us a call at <br>
               <strong> 080-67364545 (Monday - Saturday: 9AM to 9PM)</strong>>
            </p>
         </div>
      </div>
      </section>
         <!-- FOOTER -->
      <?php
      include 'footer.php'
      ?>
   </body>
</html>