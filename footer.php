<section class="footer-sec bg-white">
    <div class="container py-4">
        <!---------------third main row start--------------->
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
                <nav class="navbar navbar-expand-sm px-2">
                    <ul class="d-flex justify-contect-start flex-wrap txt-about-us">
                        <li class="nav-item mx-2"><a class="nav-item" href="aboutus.php"> About us</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href="contactus.php"> Contact us</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href="career.php"> Careers</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href="adsales.php"> Advertise with us</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href=""> Blog</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href="help.php"> Help</a></li>
                        <li class="nav-item mx-2"><a class="nav-item" href=""> Premium Ads</a></li>
                    </ul>
                </nav>
                <p class="text-justify mx-3 pt-0">Widely known as India’s no. 1 online classifieds platform, MyClickOnline is all
                    about you. Our aim is to empower every person in the country to independently connect with buyers
                    and sellers online. We care about you — and the transactions that bring you closer to your dreams. Want
                    to buy your first car? We’re here for you. Want to sell commercial property to buy your dream home?
                    We’re here for you. Whatever job you’ve got, we promise to get it done.</p>

                <p class="text-dark bg-gray ml-3 px-2">At MyClickOnline, you can buy, sell or rent anything you can think
                    of.<a class="btn-custom btn-red m-2" href="postfreead.php">Post Free Ad</a></p>
               

            </div>
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 border-left">
            <nav class="navbar navbar-expand-sm px-0">
                    <ul class="d-flex justify-contect-start flex-wrap txt-about-us">
                    <li class="nav-item mx-2"><a class="nav-item" href="policies.php"> Listing Policy</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href="t&c.php"> Terms of uses</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href="policies.php"> Privacy Policy</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href="policies.php"> Mobile Policies</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href="sitemap.php"> Sitemap</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href=""> News</a></li>
                    </ul>
                </nav>
               <div class="border-top">
               <nav class="navbar navbar-expand-sm px-0">
                    <ul class="d-flex justify-contect-start flex-wrap txt-about-us">
                    <li class="nav-item mx-2"><a class="nav-item" href=""> Malaysia</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href=""> Australia</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href=""> 中国</a></li>
                    <li class="nav-item mx-2"><a class="nav-item" href=""> Singapore</a></li>
                    </ul>
                </nav>
                 </div>
                <div class="row flex-wrap social-icons mx-3">
                    <h6 class="text-dark">Follow Us :</h6>
                    <span>
                        <a href="#" class="fa fa-facebook"></a>
                        <a href="#" class="fa fa-linkedin"></a>
                        <a href="#" class="fa fa-google"></a>
                        <a href="#" class="fa fa-youtube"></a>
                    </span>
                </div>
                <div class="row flex-wrap social-icons mx-3">
                    <h6 class="text-dark">Download The App :</h6>
                    <span>
                        <a href="#" class="fa fa-apple font1"></a>
                        <a href="#" class="fa fa-android font2"></a>
                        <a href="#" class="fa fa-windows font3"></a>
                    </span>
                </div>
            </div>
        </div>
    </div><!-- container -->
</section><!-- footer-sec -->