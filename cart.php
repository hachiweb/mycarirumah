<?php
include 'header.php'
?>
<title>Cart</title>

<div class="border-top"></div>
<section class="container my-2">
    <div class="row border" style="padding:155px;background-color:white;">
      <div class="col-md-12 col-sm-12 text-center media_color">
        <h3 class="text-muted media_color">There are no items in your cart</h3>
        <a href=""><button type="button" class="btn btn-primary bdr_radius media_color">Continue Shoping</button></a>
      </div>
    </div>
</section>
<!-- FOOTER -->
<?php
include 'footer.php'
?>
</body>

</html>