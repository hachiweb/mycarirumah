<?php
include 'header.php'
?>
<title>Help Center</title>
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12"
            style="background-image: url('assets/images/16.jpg'); background-size:cover;height:250px;width:100%;">
            <h1 style="text-align:center;padding-top:100px;color:white">Help Center</h1>
         </div>
      </div>
   </div>
   <section class="bg-white">
   <div class="container py-2">
      <nav class="navbar navbar-expand-sm static-nav anav">
         <ul class="navbar-nav flex-wrap">
            <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
            <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
            <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
            <li class="nav-item active"><a class="active" href="t&c.php">Terms & Conditions</a></li>
            <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
         </ul>
      </nav>
   </div>
   </section>
   <div class="border-top"></div>
   <section class="container bg-white mt-3">
      <div class="row p-3">
         <div class="col-md-4">
            <h4 class="text-dark p-3"> Help Center</h4>
            <!-- Accordian -->
            <h4 class="accordion">Registration and Sign-in</h4>
            <div class="panel">
               <ul>
                  <li><a href=""> Using MyClickOnline</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Abuse of MyClickOnline</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
               </ul>
            </div>
            <h4 class="accordion">Posting Ads</h4>
            <div class="panel">
               <ul>
                  <li><a href="">Definitions of words</a></li>
                  <li><a href="">General conditions</a></li>
                  <li><a href="">Scope of Contract</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href="">Special Conditions</a></li>
                  <li><a href="">Eligibility</a></li>
                  <li><a href="">Eligibility</a></li>
               </ul>
            </div>
            <h4 class="accordion">Search & Browse</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <h4 class="accordion"> My Account</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <h4 class="accordion"> MyClickOnline Doorstep</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <h4 class="accordion">Suggestions</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <h4 class="accordion">Contact Us</h4>
            <div class="panel">
               <ul>
                  <li><a href="">The Scope of Cover</a></li>
                  <li><a href="">Exclusions</a></li>
                  <li><a href=""> Special Exclusions</a></li>
                  <li><a href="">Violations by User</a></li>
                  <li><a href=""> Compensation</a></li>
                  <li><a href="">Protection Plan Condition</a></li>
                  <li><a href=""> Claim Procedure</a></li>
               </ul>
            </div>
            <!-- End Accordian -->
         </div>
         <div class="col-md-8 mt-5 text-muted text-justify">
            <h3 class="p-3 text-dark">MyClickOnline Doorstep</h3>
            <span>1.</span><strong> What is MyClickOnline Doorstep?</strong>
            <p>MyClickOnline Doorstep is the easiest way to buy and sell pre-owned goods online. With Doorstep you can
               negotiate a price, pay for the product and have it picked up and delivered.</p>
            <span>2.</span><strong> How do I know if my area is serviceable by Doorstep?</strong>
            <p>MyClickOnline Doorstep covers over 250+ cities across India and allows you to choose from over 30
               categories of products to buy and sell. You can also visit this page to enter your location and confirm
               if you can avail our service.</p>
            <span>3.</span><strong> How can I post an ad?</strong>
            <p>Look for the Post an Ad button on our app or site and select the category to indicate what type of
               product you wish to sell. Fill in the details, upload pictures of the product and click POST. Didn’t we
               tell you it would be easy?</p>
            <span>4.</span><strong> I’ve seen something I like, how can I buy it?</strong>
            <p>MyClickOnline Doorstep allows you to Make an Offer against a product you like. Once this is done, you
               officially begin a negotiation with the seller who can choose to accept your offer or counter it. If both
               parties are satisfied, you can proceed to make a payment which is held safely in our escrow account and
               released only after the product is delivered to you. We encourage you to always check the condition of
               the product once received.</p>
            <span>5.</span><strong> As a seller, how will I know if the buyer has made the payment?</strong>
            <p>All payments are routed through our escrow account. Once the buyer pays the agreed amount, we will notify
               you via your registered email id and mobile number. This amount will get credited to your bank account
               once the product has been delivered to the buyer.</p>
            <span>6.</span><strong> How much am I being charged for Doorstep pickup and delivery?</strong>
            <p>A nominal convenience fee is charged based on the location as well as the product’s weight and
               dimensions.</p>
            <span>7.</span><strong> While trying to sell an item, one of the offers was Auto-accepted. Why did this
               happen?</strong>
            <p>Any offer that is above the asking price is auto-accepted by the system. In case you would like to change
               the asking price you may go back and edit the original ad.</p>
            <span>8.</span><strong> How much am I being charged for Doorstep pickup and delivery?How long does it take
               for a product to get delivered after a transaction has been made?</strong>
            <p>Lighter items can be shipped throughout India while medium and heavy products can be shipped only within
               the seller’s city. Intercity deliveries are fulfilled within 5-7 working days while deliveries within the
               same city happen between 2-3 working days.</p>
            <span>9.</span><strong>Is it possible to track the status of the shipment while in transit?</strong>
            <p>Shipments are made as per scheduled slots, however, you can also track your order status via the app and
               site.</p>
            <span>10.</span><strong> Can I pick-up the product myself?</strong>
            <p>Absolutely! You can pay for your purchase online but opt to pick-up the product yourself.</p>
            <span>11.</span><strong> How can I delete my Ad once the transaction has taken place?</strong>
            <p> Once your product has been sold you can delete the ad to ensure that you do not get any further queries
               from buyers. To do this, log in to MyClickOnline account, select Manage My Ads and under active Ads,
               select the Ad that you would like to delete and finally click on the Trash Can icon to delete the ad.</p>
         </div>
      </div>
   </section>
   <section class="bg-white">
   <!-- FOOTER -->
<?php
include 'footer.php'
?>
   <script>
      var acc = document.getElementsByClassName("accordion");
      var i;

      for (i = 0; i < acc.length; i++) {
         acc[i].addEventListener("click", function () {
            this.classList.toggle("active1");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
               panel.style.maxHeight = null;
            } else {
               panel.style.maxHeight = panel.scrollHeight + "px";
            }
         });
      }
   </script>
</body>

</html>