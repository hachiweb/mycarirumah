<?php
include 'header.php'
?>
<title>Post Free Ad</title>
<section class="container mt-2 postfree-main-sec">
   <div class="row">
      <div class="col-sm-4 pr-sm-0">
         <div class="nav h-100 border flex-column nav-pills post-tab-nav bg-white" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <h5 class="text-center p-3">Select a category</h5>
            <a class="nav-link border-bottom border-top active" id="v-pills-Cars-tab" data-toggle="pill" href="#v-pills-Cars" role="tab" aria-controls="v-pills-Cars" aria-selected="true">
               <img src="assets/images/car2.png" alt="">
               Cars & Bikes
            </a>
            <a class="nav-link border-bottom" id="v-pills-Mobiles-tab" data-toggle="pill" href="#v-pills-Mobiles" role="tab" aria-controls="v-pills-Mobiles" aria-selected="false">
               <img src="assets/images/mobile2.png" alt=""> 
               Mobiles & Tablets
            </a>
            <a class="nav-link border-bottom" id="v-pills-Electronics-tab" data-toggle="pill" href="#v-pills-Electronics" role="tab" aria-controls="v-pills-Electronics" aria-selected="false">
               <img src="assets/images/electronics2.png" alt=""> 
               Electronics Appliances
            </a>
            <a class="nav-link border-bottom" id="v-pills-Real-tab" data-toggle="pill" href="#v-pills-Real" role="tab" aria-controls="v-pills-Real" aria-selected="false">
               <img src="assets/images/realestate2.png" alt="">
               Real Estate
            </a>
            <a class="nav-link border-bottom" id="v-pills-Home-tab" data-toggle="pill" href="#v-pills-Home" role="tab" aria-controls="v-pills-Home" aria-selected="false">
               <img src="assets/images/home2.png" alt="">
               Home & Lifestyle
            </a>
            <a class="nav-link border-bottom" id="v-pills-Jobs-tab" data-toggle="pill" href="#v-pills-Jobs" role="tab" aria-controls="v-pills-Jobs" aria-selected="false">
               <img src="assets/images/job2.png" alt="">
               Jobs
            </a>

            <a class="nav-link border-bottom" id="v-pills-Education-tab" data-toggle="pill" href="#v-pills-Education" role="tab" aria-controls="v-pills-Education" aria-selected="false">
               <img src="assets/images/education2.png" alt=""> 
               Education & Training
            </a>
            <a class="nav-link border-bottom" id="v-pills-Entertainment-tab" data-toggle="pill" href="#v-pills-Entertainment" role="tab" aria-controls="v-pills-Entertainment" aria-selected="false">
               <img src="assets/images/entertainment2.png" alt="">
               Entertainment
            </a>
            <a class="nav-link border-bottom" id="v-pills-Pets-tab" data-toggle="pill" href="#v-pills-Pets" role="tab" aria-controls="v-pills-Pets" aria-selected="false">
               <img src="assets/images/pets2.png" alt="">
               Pets & Pet Care
            </a>
            <a class="nav-link border-bottom" id="v-pills-Community-tab" data-toggle="pill" href="#v-pills-Community" role="tab" aria-controls="v-pills-Community" aria-selected="false">
               <img src="assets/images/community2.png" alt="">
               Community
            </a>
            <a class="nav-link border-bottom" id="v-pills-Events-tab" data-toggle="pill" href="#v-pills-Events" role="tab" aria-controls="v-pills-Events" aria-selected="false">
               <img src="assets/images/event2.png" alt="">
               Events
            </a>
            <a class="nav-link border-bottom" id="v-pills-Matrimonial-tab" data-toggle="pill" href="#v-pills-Matrimonial" role="tab" aria-controls="v-pills-Matrimonial" aria-selected="false">
               <img src="assets/images/metromonial2.png" alt="">
               Matrimonial
            </a>
         </div>
      </div>


      <div class="col-sm-8 pl-sm-0">
         <div class="bg-white h-100">
            <h2 class="p-4">Post Free Ad</h2>
            <div class="tab-content" id="v-pills-tabContent">
               <div class="tab-pane fade show active" id="v-pills-Cars" role="tabpanel" aria-labelledby="v-pills-Cars-tab">
                  <p class="ml-4 h5">Select a subcategory</p>
                  <ol class="setmargin ml-4 ">
                     <li><a href="postadform.php?Category=1&Subcategory=13" alt="any">Bikes & Scooters</a></li>
                     <li><a href="postadform.php?Category=1&Subcategory=14" alt="any">Cars</a></li>
                     <li><a href="postadform.php?Category=1&Subcategory=15" alt="any">Commercial Vehicles</a></li>
                     <li><a href="postadform.php?Category=1&Subcategory=16" alt="any">Spare Parts - Accessories</a></li>
                     <li><a href="postadform.php?Category=1&Subcategory=17" alt="any">Other Vehicles</a></li>
                  </ol>
               </div>

               <div class="tab-pane fade ml-4" id="v-pills-Mobiles" role="tabpanel" aria-labelledby="v-pills-Mobiles-tab">
                  <div class="mt-3">
                     <p class="mt-3 h5">Select a subcategory</p>
                     <ul class="setmargin">
                        <li><a href="postadform.php?Category=2&Subcategory=18">Mobile Phones</a></li>
                        <li><a href="postadform.php?Category=2&Subcategory=19">Accessories</a></li>
                        <li><a href="postadform.php?Category=2&Subcategory=20">Tablets</a></li>
                        <li><a href="postadform.php?Category=2&Subcategory=21">Wearables</a></li>
                     </ul>
                  </div>
               </div>

               <!---------------electronics tab--------->
               <div class="tab-pane fade" id="v-pills-Electronics" role="tabpanel" aria-labelledby="v-pills-Electronics-tab">
                  <div class="ml-4">
                     <br>
                     <!-- Nav tabs -->
                     <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" data-toggle="tab" href="#home">
                              <h5>Post Free Ad</h5>
                              <br>For Individual-Free
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#menu1">
                              <h5>Post Bulks Ad</h5>
                              <br>For Business-Free
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#menu2">
                              <h5>Sell For Me</h5>
                              <br>MyClickOnline Post Ads & Sell Your Products
                           </a>
                        </li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div id="home" class="container tab-pane active">
                           <br>
                           <table class="table table-borderless">
                              <thead>
                                 <tr>
                                    <th><p class="h6">Home Appliances</p></th>
                                    <th><p class="h6">Laptops & Accessories</p></th>
                                    <th><p class="h6">Kitchen Appliances</p></th>
                                 </tr>
                              </thead>
                              <tbody calss="">
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=28" alt="any">Refrigerators / Fridge</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=36" alt="any">Laptops</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=44" alt="any">Water Purifier</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=29" alt="any">Washing Machine</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=37" alt="any">Desktop/Computers</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=45" alt="any">Microwave Ovens</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=30" alt="any">Air Coolers</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=38" alt="any">Monitor</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=46" alt="any">Mixer/Grinder/Juicer</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=31" alt="any">Air Conditioners / AC</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=39" alt="any">Scanners</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=47" alt="any">Induction Cook Tops</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=32" alt="any">Water Heaters / Geysers</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=40" alt="any">Printers</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=48" alt="any">Gas Stove</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=33" alt="any">Sewing Machines</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=41" alt="any">Printer Ink and Toners</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=49" alt="any">Oven Toaster Griller</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=34" alt="any">Vacuum Cleaners</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=42" alt="any">Routers</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=50" alt="any">Electric Cookers</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=22&Subcategory=35" alt="any">Ceiling Fans</a></td>
                                    <td><a href="postadform.php?Category=23&Subcategory=43" alt="any">CPU</a></td>
                                    <td><a href="postadform.php?Category=24&Subcategory=51" alt="any">Food Processors</a></td>
                                 </tr>
                              </tbody>
                              <thead>
                                 <tr>
                                    <th><p class="h6">Audio, Video & Gaming</p></th>
                                    <th><p class="h6">Camera & Accessories</p></th>
                                    <th><p class="h6">Other Devices</p></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><a href="postadform.php?Category=25&Subcategory=52" alt="any">TV</a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=57" alt="any">Cameras - Digicams</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=64" alt="any">Tools - Machinery - Industrial</a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=25&Subcategory=53" alt="any">Video Games - Consoles</a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=58" alt="any">Digital SLR</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=65" alt="any">Inverters, UPS & Generators</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=25&Subcategory=54" alt="any">Music Systems - Home Theatre</a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=59" alt="any">Digital Point & Shoot Cameras</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=66" alt="any">Fax, EPABX, Office Equipment</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=25&Subcategory=55" alt="any">iPods, MP3 Players</a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=60" alt="any">Camera Accessories</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=67" alt="any">Security Equipment - Products</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=25&Subcategory=56" alt="any">DVD players</a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=61" alt="any">Camcorders</a></td>
                                    <td><a href="postadform.php?Category=#&Subcategory=#" alt="any"></a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=2&Subcategory=" alt="any"></a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=52" alt="any">Film Cameras</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=68" alt="any">Office Supplies</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=2&Subcategory=" alt="any"></a></td>
                                    <td><a href="postadform.php?Category=26&Subcategory=63" alt="any">Binoculars, Telescopes</a></td>
                                    <td><a href="postadform.php?Category=27&Subcategory=69" alt="any">Everything Else</a></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <div id="menu1" class="container tab-pane fade border mt-4">
                           <br>
                           <table>
                              <tr>
                                 <td>
                                    <h4>Have a lot of items to sell?</h4>
                                    <p>Use MyClickOnline's Bulk Ad Upload tool to add multiple items faster</p>
                                    <a href="#" alt="any"><button class="btn btn-primary ml-4 mt-5">Take Me
                                    Now</button></a>
                                 </td>
                                 <td>
                                    <img style="background-size: 100%" src="assets/images/22.png" alt="any">
                                 </td>
                              </tr>
                           </table>
                           <div class="row border mt-4"></div>
                           <h5 class="text-center mt-4">Introducing MyClickOnline Seller Tools - To manage & sell multiple items superfast</h5>
                           <p class="text-center">What we offer?</p>
                           <table calss="ml-5">
                              <tr>
                                 <td>
                                    <img src="assets/images/s1.PNG" alt="">
                                    <p>Free Bulk Ad<br>Posting Facility</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s2.PNG" alt="">
                                    <p>Inventory<br> Management Tool</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s3.PNG" alt="">
                                    <p>All-in-one<br>Dashboard</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s4.PNG" alt="">
                                    <p>Dedicated<br>Customer Support</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s1.PNG" alt="">
                                    <p>Free Bulk Ad<br>Posting Facility</p>
                                 </td>
                              </tr>
                           </table>
                        </div>
                        <div id="menu2" class="container tab-pane fade">
                           <br>
                           <h6 class="d-inline">Sit back and relax while MyClickOnline Relationship Manager sells
                           your product</h6>
                           <span class="float-right"><a class="text-primary">1-800-3000-500</a></span>
                           <div class="border-top mt-3"></div>
                           <table>
                              <tr>
                                 <td>
                                    <form action="">
                                       <div class="form-group">
                                          <label for="email">Email Address:</label>
                                          <input type="email" class="form-control" id="email"
                                          placeholder="Enter Email">
                                       </div>
                                       <div class="form-group">
                                          <label for="phone">Phone</label>
                                          <input type="number" class="form-control" id="mobile"
                                          placeholder="Enter Number">
                                       </div>
                                       <button type="submit" class="btn btn-primary">SUBMIT</button>
                                    </form>
                                 </td>
                                 <td><img class="w-300" src="assets/images/s5.PNG" alt="any"></td>
                              </tr>
                           </table>
                           <div class="border-top mt-4"></div>
                           <h6 class="text-center mt-4">Benefits of this service</h6>
                           <table calss="ml-5">
                              <tr>
                                 <td>
                                    <img src="assets/images/s6.PNG" alt="">
                                    <p>We post an <br>awesome</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s7.PNG" alt="">
                                    <p>We negotiate <br>with buyers & sell faster</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s8.PNG" alt="">
                                    <p>We arrange<br> transportation</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s9.PNG" alt="">
                                    <p>...and We can <br>store your products</p>
                                 </td>
                              </tr>
                           </table>
                        </div>
                     </div>
                  </div>                              
               </div>

               <!---------------Real tabs------->
               <div class="tab-pane fade" id="v-pills-Real" role="tabpanel" aria-labelledby="v-pills-Real-tab">
                  <div class="mt-3">
                     <p class="mt-3 ml-4 h5">Select a subcategory</p>
                     <ul class="setmargin ml-4">
                        <li><a href="postadform.php?Category=4&Subcategory=70">Commercial Property for Rent</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=71">Commercial Property for Sale</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=72">Flatmates</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=73">Houses - Apartments for Rent</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=74">Land - Plot For Sale</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=75">Paying Guest - Hostel</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=76">Residential - Builder floors For Rent</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=77">Residential - Builder floors For Sale</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=78">Service Apartments</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=79">Vacation Rentals - Timeshare</a></li>
                        <li><a href="postadform.php?Category=4&Subcategory=80">Villas/Bungalows for Rent</a></li>
                        <li><a href="postadform.php?Category=&Subcategory=">Villas/Bungalows for Sale</a></li>
                     </ul>
                  </div>
               </div>

               <!---------------Home & lifestyle tab------->
               <div class="tab-pane fade" id="v-pills-Home" role="tabpanel" aria-labelledby="v-pills-Home-tab">
                  <div class="ml-4">
                     <br>
                     <!-- Nav tabs -->
                     <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" data-toggle="tab" href="#home1">
                              <h5>Post Free Ad</h5>
                              <br>For Individual-Free
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#menu11">
                              <h5>Post Bulks Ad</h5>
                              <br>For Business-Free
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#menu12">
                              <h5>Sell For Me</h5>
                              <br>MyClickOnline Post Ads & Sell Your Products
                           </a>
                        </li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div id="home1" class="container tab-pane active">
                           <br>
                           <table class="table table-borderless">
                              <thead>
                                 <tr>
                                    <th><p class="h6">Furniture & Decor</p></th>
                                    <th><p class="h6">Sports, Books & Hobbies</p></th>
                                    <th><p class="h6">Fashion</p></th>
                                 </tr>
                              </thead>
                              <tbody calss="">
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=87" alt="any">Furniture for Home & Office</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=146" alt="any">Sport - Fitness Equipment</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=95" alt="any">Clothing - Garments</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=88" alt="any">Home Decor - Furnishings</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=147" alt="any">Bicycle & Accessories</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=96" alt="any">Watches</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=89" alt="any">Household</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=148" alt="any">Books - Magazines</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=97" alt="any">Jewellery</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=90" alt="any">Kitchenware</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=149" alt="any">Musical Instruments</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=98" alt="any">Bags - Luggage</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=91" alt="any">Antiques - Handicrafts</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=150" alt="any">Coins - Stamps</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=99" alt="any">Footwear</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=82&Subcategory=92" alt="any">Paintings</a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=151" alt="any">Collectibles</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=100" alt="any">Fashion Accessories</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=&Subcategory=" alt="any"></a></td>
                                    <td><a href="postadform.php?Category=83&Subcategory=152" alt="any">Music - Movies</a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=101" alt="any">Health - Beauty Products</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=&Subcategory=" alt="any"></a></td>
                                    <td><a href="postadform.php?Category=&Subcategory=" alt="any"></a></td>
                                    <td><a href="postadform.php?Category=84&Subcategory=102" alt="any">Gifts - Stationary</a></td>
                                 </tr>
                              </tbody>
                              <thead>
                                 <tr>
                                    <th><p class="h6">Kids & Toys</p></th>
                                    <th><p class="h6">Misc</p></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><a href="postadform.php?Category=85&Subcategory=153" alt="any">Toys - Games</a></td>
                                    <td><a href="postadform.php?Category=86&Subcategory=156" alt="any"> Cameras - Digicams</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=85&Subcategory=154" alt="any">Baby - Infant Products</a></td>
                                    <td><a href="postadform.php?Category=86&Subcategory=157" alt="any"> Everything Else</a></td>
                                 </tr>
                                 <tr>
                                    <td><a href="postadform.php?Category=85&Subcategory=155" alt="any">Kids Learning</a></td>
                                    <td><a href="postadform.php?Category=&Subcategory=" alt="any"></a></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <div id="menu11" class="container tab-pane fade border mt-4">
                           <br>
                           <table>
                              <tr>
                                 <td>
                                    <h4>Have a lot of items to sell?</h4>
                                    <p>Use MyClickOnline's Bulk Ad Upload tool to add multiple items faster</p>
                                    <a href="#" alt="any"><button class="btn btn-primary ml-4 mt-5">Take Me
                                    Now</button></a>
                                 </td>
                                 <td>
                                    <img style="background-size: 100%" src="assets/images/22.png" alt="any">
                                 </td>
                              </tr>
                           </table>
                           <div class="row border mt-4"></div>
                           <h5 class="text-center mt-4">Introducing MyClickOnline Seller Tools - To manage & sell
                           multiple items superfast</h5>
                           <p class="text-center">What we offer?</p>
                           <table calss="ml-5">
                              <tr>
                                 <td>
                                    <img src="assets/images/s1.PNG" alt="">
                                    <p>Free Bulk Ad<br>Posting Facility</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s2.PNG" alt="">
                                    <p>Inventory<br> Management Tool</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s3.PNG" alt="">
                                    <p>All-in-one<br>Dashboard</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s4.PNG" alt="">
                                    <p>Dedicated<br>Customer Support</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s1.PNG" alt="">
                                    <p>Free Bulk Ad<br>Posting Facility</p>
                                 </td>
                              </tr>
                           </table>
                        </div>
                        <div id="menu12" class="container tab-pane fade">
                           <br>
                           <h6 class="d-inline">Sit back and relax while MyClickOnline Relationship Manager sells
                           your product</h6>
                           <span class="float-right"><a class="text-primary">1-800-3000-500</a></span>
                           <div class="border-top mt-3"></div>
                           <table>
                              <tr>
                                 <td>
                                    <form action="">
                                       <div class="form-group">
                                          <label for="email">Email Address:</label>
                                          <input type="email" class="form-control" id="email"
                                          placeholder="Enter Email ID">
                                       </div>
                                       <div class="form-group">
                                          <label for="phone">Phone Number:</label>
                                          <input type="number" class="form-control" id="mobile"
                                          placeholder="Enter Number">
                                       </div>
                                       <button type="submit" class="btn btn-primary">SUBMIT</button>
                                    </form>
                                 </td>
                                 <td><img class="w-300" src="assets/images/s5.PNG" alt="any"></td>
                              </tr>
                           </table>
                           <div class="border-top mt-4"></div>
                           <h6 class="text-center mt-4">Benefits of this service</h6>
                           <table calss="ml-5">
                              <tr>
                                 <td>
                                    <img src="assets/images/s6.PNG" alt="">
                                    <p>We post an <br>awesome</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s7.PNG" alt="">
                                    <p>We negotiate <br>with buyers & sell faster</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s8.PNG" alt="">
                                    <p>We arrange<br> transportation</p>
                                 </td>
                                 <td>
                                    <img src="assets/images/s9.PNG" alt="">
                                    <p>...and We can <br>store your products</p>
                                 </td>
                              </tr>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>

               <!---------------Jobs tab------->
               <div class="tab-pane fade" id="v-pills-Jobs" role="tabpanel" aria-labelledby="v-pills-Jobs-tab">
                  <div class="mt-3">
                     <p class="ml-4 h5">Select a subcategory</p>
                     <ul class="ml-4">
                        <li><a href="jobpostform.php?Category=6&Subcategory=103">Full Time Jobs</a></li>
                        <li><a href="jobpostform.php?Category=6&Subcategory=104">Internships</a></li>
                        <li><a href="jobpostform.php?Category=6&Subcategory=105">Part Time Jobs</a></li>
                        <li><a href="jobpostform.php?Category=6&Subcategory=106">Work Abroad</a></li>
                        <li><a href="jobpostform.php?Category=6&Subcategory=107"> Work From Home</a></li>
                     </ul>
                  </div>
               </div>

               <!---------------Education tab------->
               <div class="tab-pane fade" id="v-pills-Education" role="tabpanel"
               aria-labelledby="v-pills-Education-tab">
               <div class="mt-3">
                  <p class="ml-4 h5">Select a subcategory</p>
                  <ul class="ml-4">
                     <li><a href="postfreeadform.php?Category=7&Subcategory=108">Career Counseling</a><span
                        class="badge badge-warning">NEW</span></li>
                        <li><a href="postfreeadform.php?Category=7&Subcategory=109"> Certifications & Training</a><span
                           class="badge badge-warning">NEW</span></li>
                           <li><a href="postfreeadform.php?Category=7&Subcategory=110">Competitive Exams Coaching</a><span
                              class="badge badge-warning">NEW</span></li>
                              <li><a href="postfreeadform.php?Category=7&Subcategory=158">Entrance Exam Coaching</a></li>
                              <li><a href="postfreeadform.php?Category=7&Subcategory=111">Hobby Classes</a></li>
                              <li><a href="postfreeadform.php?Category=7&Subcategory=112"> Play Schools - Creche</a><span
                                 class="badge badge-warning">NEW</span></li>
                                 <li><a href="postfreeadform.php?Category=7&Subcategory=113">Schools & School Tuitions</a><span
                                    class="badge badge-warning">NEW</span></li>
                                    <li><a href="postfreeadform.php?Category=7&Subcategory=114">Study Abroad Consultants</a></li>
                                    <li><a href="postfreeadform.php?Category=7&Subcategory=115">Text books & Study Material</a><span
                                       class="badge badge-warning">NEW</span></li>
                                       <li><a href="postfreeadform.php?Category=7&Subcategory=116">Vocational Skill Training</a><span
                                          class="badge badge-warning">NEW</span></li>
                                          <li><a href="postfreeadform.php?Category=7&Subcategory=117">Workshops</a></li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!---------------Entertainment tab------->
                                 <div class="tab-pane fade" id="v-pills-Entertainment" role="tabpanel"
                                 aria-labelledby="v-pills-Entertainment-tab">
                                 <div class="mt-3">
                                    <p class="ml-4 h5">Select a subcategory</p>
                                    <ul class=" ml-4">
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=118"> Career Counseling</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=119"> Acting Schools</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=120"> Actor - Model Portfolios</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=121"> Art Directors - Editors</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=122"> Fashion Designers - Stylists</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=123"> Make Up - Hair - Films & TV</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=124"> Modeling Agencies</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=125"> Musicians</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=126"> Photographers - Cameraman</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=127"> Script Writers</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=128"> Set Designers</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=129"> Sound Engineers</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=130"> Studios - Locations for hire</a></li>
                                       <li><a href="postfreeadform.php?Category=8&Subcategory=131"> Other Entertainment </a></li>
                                    </ul>
                                 </div>
                              </div>
                              <!---------------Pets and pet care tab------->
                              <div class="tab-pane fade" id="v-pills-Pets" role="tabpanel" aria-labelledby="v-pills-pets-tab">
                                 <div>
                                    <p class="ml-4 h5"> Select a subcategory</p>
                                    <ul class="ml-4">
                                       <li><a href="postfreeadform.php?Category=9&Subcategory=132">Pet Adoption</a></li>
                                       <li><a href="postfreeadform.php?Category=9&Subcategory=133">Pet Care - Accessories</a></li>
                                       <li><a href="postfreeadform.php?Category=9&Subcategory=134">Pet Clinics</a></li>
                                       <li><a href="postfreeadform.php?Category=9&Subcategory=135">Pet Foods</a></li>
                                       <li><a href="postfreeadform.php?Category=9&Subcategory=136">Pet Training & Grooming</a></li>
                                    </ul>
                                 </div>
                              </div>
                              <!---------------Cumunity tab------->
                              <div class="tab-pane fade" id="v-pills-Community" role="tabpanel"
                              aria-labelledby="v-pills-cummunity-tab">
                              <p class="ml-4 h5"> Select a subcategory</p>
                              <ul class="ml-4">
                                 <li><a href="postfreeadform.php?Category=10&Subcategory=137">Announcements</a></li>
                                 <li><a href="postfreeadform.php?Category=10&Subcategory=159">Car Pool - Bike Ride</a></li>
                                 <li><a href="postfreeadform.php?Category=10&Subcategory=138">Charity - Donate - NGO</a></li>
                                 <li><a href="postfreeadform.php?Category=10&Subcategory=139">Lost - Found</a></li>
                                 <li><a href="postfreeadform.php?Category=10&Subcategory=140">Tender Notices</a></li>
                              </ul>
                           </div>
                           <!-----------Events tab------->
                           <div class="tab-pane fade" id="v-pills-Events" role="tabpanel" aria-labelledby="v-pills-Events-tab">
                              <p class="ml-4 h5"> Select a subcategory</p>
                              <ul class="ml-4">
                                 <li><a href="postfreeadform.php?Category=11&Subcategory=141"> Event Management</a></li>
                                 <li><a href="postfreeadform.php?Category=11&Subcategory=142"> Live Events</a></li>
                              </ul>
                           </div>

                           <!---------------Matrimonial tab------->
                           <div class="tab-pane fade" id="v-pills-Matrimonial" role="tabpanel"
                           aria-labelledby="v-pills-Matrimonial-tab">
                           <p class="ml-4 h5"> Select a subcategory</p>
                           <ul class="ml-4">
                              <li><a href="postfreeadform.php?Category=12&Subcategory=143">Brides</a></li>
                              <li><a href="postfreeadform.php?Category=12&Subcategory=144">Grooms</a></li>
                              <li><a href="postfreeadform.php?Category=12&Subcategory=145">Wedding Planners</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <script>
               // $('.nav-pills > a.nav-link').hover(function () {
               //    $(this).stop().tab('show');
               // });

               $('.post-tab-nav>a').hover(function(e){
                e.preventDefault();
                $('#v-pills-tabContent>.tab-pane').removeClass('active');
                tabContentSelector = $(this).attr('href');
                $(this).tab('show');
                $(tabContentSelector).addClass('active');
             });

               // $(document).ready(function () {
               //    $("#mouseover1").mouseenter(function () {
               //       $("this").css('background-color', 'rgb(0, 131, 202)');
               //       $("this").css('color', 'white');
               //    });
               // });
            </script>
         </body>

         </html>