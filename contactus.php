<?php
include 'header.php'
?>
<title>Contact Us</title>
<section class="bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"
                style="background-image: url('assets/images/contact_us.jpg'); background-size:cover;height:250px;width:100%;">
                <h1 style="text-align:center;padding-top:100px;color:white">Contact us</h1>
            </div>
        </div>
    </div>
    <div class="container py-2">
        <nav class="navbar navbar-expand-sm static-nav anav">
            <ul class="navbar-nav flex-wrap">
                <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
                <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
                <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
                <li class="nav-item active"><a class="active" href="t&c.php">Terms & Conditions</a></li>
                <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
            </ul>
        </nav>
    </div>
    <div class="border-top mt-1"></div>
    <h3 class="text-center text-dark mt-5">HOW CAN WE HELP YOU?</h3>
    <div class="container my-5">
        <!----- Nav tabs ------>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">
                    <h5 class="text-dark">We're all ears!</h5>
                    <p>Whether queries, issues or feedback,<br> send
                        us a note, we’re waiting to hear from you
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">
                    <h5 class="text-dark">Write to management</h5>
                    <p>If you still haven’t heard from us on your<br>
                        most pressing concerns, let us know
                    </p>
                </a>
            </li>
        </ul>
        <!-------- Tab panes ------>
        <div class="tab-content">
            <div id="home" class="container tab-pane active">
                <br>
                <div class="row">
                    <div class="col-sm-3 exshadow">
                        <i class="fa fa-car fa-2x"></i>
                        <a class=" text-decoration-none setmargin" href="" data-target="#mymodal1"
                            data-toggle="modal">Cars & Bikes</a>
                        <p class="setmargin1">things MyClickOnlineCars </p>
                        <!------------ form----------->
                        <div class="modal fade" id="mymodal1">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="sub_contactus.php" method="post" enctype="multipart/form-data">
                                            <h4>Cars & Bikes</h4>
                                            <input type="hidden" name="category" value="Cars & Bikes">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control bdr_radius" name="name"
                                                        placeholder="Enter Name">
                                                    <label class="mt-2">Mobile Number:</label>
                                                    <input type="mobile" class="form-control bdr_radius" name="mobile"
                                                        placeholder="Enter Number">
                                                    <label class="mt-2">Email address:</label>
                                                    <input type="email" class="form-control bdr_radius" name="email"
                                                        placeholder="Enter Email ">
                                                </div>
                                                <div class="col-sm-6 float-right">
                                                    <label for="subject">Subject*</label>
                                                    <input type="sunject" class="form-control bdr_radius" name="subject"
                                                        placeholder="Enter Subject">
                                                    <label for="comment" class="mt-3">Comment:</label>
                                                    <textarea class="form-control bdr_radius" rows="4" name="comment"></textarea>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button style="width:200px;margin:0px 280px;" type="submit"
                                                        class="btn btn-primary mt-4 bdr_radius">ENTER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------------- End form----------->
                    <div class="col-sm-3 exshadow">
                        <i class="fa fa-home fa-2x"></i>
                        <a class=" text-decoration-none setmargin" href="" data-target="#mymodal2"
                            data-toggle="modal">Doorstep Delivery</a>
                        <p class="setmargin1">All things related to pick-up and delivery, payments, refunds</p>
                        <!------------ form----------->
                        <div class="modal fade" id="mymodal2">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="sub_contactus.php" method="post" enctype="multipart/form-data">
                                            <h4>Doorstep Delivery</h4>
                                            <input type="hidden" name="category" value="Doorstep Delivery">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control bdr_radius " name="name"
                                                        placeholder="Enter Name">
                                                    <label class="mt-2">Mobile Number:</label>
                                                    <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                        placeholder="Enter Number">
                                                    <label class="mt-2">Email address:</label>
                                                    <input type="email" class="form-control bdr_radius " name="email"
                                                        placeholder="Enter Email ">
                                                </div>
                                                <div class="col-sm-6 float-right">
                                                    <label for="subject">Subject*</label>
                                                    <input type="sunject" class="form-control bdr_radius " name="subject"
                                                        placeholder="Enter Subject">
                                                    <label for="comment" class="mt-3">Comment:</label>
                                                    <textarea class="form-control bdr_radius " rows="4" name="comment"></textarea>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button style="width:200px;margin:0px 280px;" type="submit"
                                                        class="btn btn-primary mt-4">ENTER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-------------End form----------->
                    </div>
                    <div class="col-sm-3 exshadow">
                        <i class="fa fa-home fa-2x"></i>
                        <a class=" text-decoration-none setmargin" href="" data-target="#mymodal3"
                            data-toggle="modal">Home</a>
                        <p class="setmargin1">All things MyClickOnline Home </p>
                        <!------------ form----------->
                        <div class="modal fade" id="mymodal3">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="sub_contactus.php" method="post" enctype="multipart/form-data">
                                            <h4>Home</h4>
                                            <input type="hidden" name="category" value="Home">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control bdr_radius " name="name"
                                                        placeholder="Enter Name">
                                                    <label class="mt-2">Mobile Number:</label>
                                                    <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                        placeholder="Enter Number">
                                                    <label class="mt-2">Email address:</label>
                                                    <input type="email" class="form-control bdr_radius " name="email"
                                                        placeholder="Enter Email ">
                                                </div>
                                                <div class="col-sm-6 float-right">
                                                    <label for="subject">Subject*</label>
                                                    <input type="sunject" class="form-control bdr_radius " name="subject"
                                                        placeholder="Enter Subject">
                                                    <label for="comment" class="mt-3">Comment:</label>
                                                    <textarea class="form-control bdr_radius " rows="4" name="comment"></textarea>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button style="width:200px;margin:0px 280px;" type="submit"
                                                        class="btn btn-primary mt-4">ENTER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-------------End form----------->
                    <div class="col-sm-3 exshadow">
                        <i class="fa fa-graduation-cap fa-2x"></i>
                        <a class=" text-decoration-none setmargin" href="" data-target="#mymodal4"
                            data-toggle="modal">Education</a>
                        <p class="setmargin1">All things MyClickOnlineLearner</p>
                        <!------------ form----------->
                        <div class="modal fade" id="mymodal4">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="sub_contactus.php" method="post" enctype="multipart/form-data">
                                            <h4>Education</h4>
                                            <input type="hidden" name="category" value="Education">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control bdr_radius " name="name"
                                                        placeholder="Enter Name">
                                                    <label class="mt-2">Mobile Number:</label>
                                                    <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                        placeholder="Enter Number">
                                                    <label class="mt-2">Email address:</label>
                                                    <input type="email" class="form-control bdr_radius " name="email"
                                                        placeholder="Enter Email ">
                                                </div>
                                                <div class="col-sm-6 float-right">
                                                    <label for="subject">Subject*</label>
                                                    <input type="sunject" class="form-control bdr_radius " name="subject"
                                                        placeholder="Enter Subject">
                                                    <label for="comment" class="mt-3">Comment:</label>
                                                    <textarea class="form-control bdr_radius " rows="4" name="comment"></textarea>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button style="width:200px;margin:0px 280px;" type="submit"
                                                        class="btn btn-primary mt-4">ENTER</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-------------End form----------->
                </div>
                <div class="row">
                    <div class="col-sm-3 exshadow">
                        <div class="border-top">
                            <i class="fa fa-car fa-2x"></i>
                            <a class=" text-decoration-none setmargin" href="" data-target="#mymodal5"
                                data-toggle="modal">Jobs</a>
                            <p class="setmargin1">All things MyClickOnlineJobs</p>
                            <!------------ form----------->
                            <div class="modal fade" id="mymodal5">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="sub_contactus.php" method="post"
                                                enctype="multipart/form-data">
                                                <h4>Jobs</h4>
                                                <input type="hidden" name="category" value="Jobs">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Name:</label>
                                                        <input type="text" class="form-control bdr_radius " name="name"
                                                            placeholder="Enter Name">
                                                        <label class="mt-2">Mobile Number:</label>
                                                        <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                            placeholder="Enter Number">
                                                        <label class="mt-2">Email address:</label>
                                                        <input type="email" class="form-control bdr_radius " name="email"
                                                            placeholder="Enter Email ">
                                                    </div>
                                                    <div class="col-sm-6 float-right">
                                                        <label for="subject">Subject*</label>
                                                        <input type="sunject" class="form-control bdr_radius " name="subject"
                                                            placeholder="Enter Subject">
                                                        <label for="comment" class="mt-3">Comment:</label>
                                                        <textarea class="form-control bdr_radius " rows="4"
                                                            name="comment"></textarea>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button style="width:200px;margin:0px 280px;" type="submit"
                                                            class="btn btn-primary mt-4">ENTER</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 exshadow">
                        <div class="border-top">
                            <i class="fa fa-car fa-2x"></i>
                            <a class=" text-decoration-none setmargin" href="" data-target="#mymodal6"
                                data-toggle="modal">Services
                            </a>
                            <p class="setmargin1">things MyClickOnlineCars </p>
                            <!------------ form---------->
                            <div class="modal fade" id="mymodal6">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="sub_contactus.php" method="post"
                                                enctype="multipart/form-data">
                                                <h4>Services</h4>
                                                <input type="hidden" name="category" value="Services">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Name:</label>
                                                        <input type="text" class="form-control bdr_radius " name="name"
                                                            placeholder="Enter Name">
                                                        <label class="mt-2">Mobile Number:</label>
                                                        <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                            placeholder="Enter Number">
                                                        <label class="mt-2">Email address:</label>
                                                        <input type="email" class="form-control bdr_radius " name="email"
                                                            placeholder="Enter Email ">
                                                    </div>
                                                    <div class="col-sm-6 float-right">
                                                        <label for="subject">Subject*</label>
                                                        <input type="sunject" class="form-control bdr_radius " name="subject"
                                                            placeholder="Enter Subject">
                                                        <label for="comment" class="mt-3">Comment:</label>
                                                        <textarea class="form-control bdr_radius " rows="4"
                                                            name="comment"></textarea>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button style="width:200px;margin:0px 280px;" type="submit"
                                                            class="btn btn-primary mt-4">ENTER</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 exshadow">
                        <div class="border-top">
                            <i class="fa fa-car fa-2x"></i>
                            <a class=" text-decoration-none setmargin" href="" data-target="#mymodal7"
                                data-toggle="modal">Cars & Bikes</a>
                            <p class="setmargin1">things MyClickOnlineCars </p>
                            <!------------ form----------->
                            <div class="modal fade" id="mymodal7">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="sub_contactus.php" method="post"
                                                enctype="multipart/form-data">
                                                <h4>Cars & Bikes</h4>
                                                <input type="hidden" name="category" value="Cars & Bikes">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Name:</label>
                                                        <input type="text" class="form-control bdr_radius " name="name"
                                                            placeholder="Enter Name">
                                                        <label class="mt-2">Mobile Number:</label>
                                                        <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                            placeholder="Enter Number">
                                                        <label class="mt-2">Email address:</label>
                                                        <input type="email" class="form-control bdr_radius " name="email"
                                                            placeholder="Enter Email ">
                                                    </div>
                                                    <div class="col-sm-6 float-right">
                                                        <label for="subject">Subject*</label>
                                                        <input type="sunject" class="form-control bdr_radius " name="subject"
                                                            placeholder="Enter Subject">
                                                        <label for="comment" class="mt-3">Comment:</label>
                                                        <textarea class="form-control bdr_radius " rows="4"
                                                            name="comment"></textarea>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button style="width:200px;margin:0px 280px;" type="submit"
                                                            class="btn btn-primary mt-4">ENTER</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------- End form----------->
                        </div>
                    </div>
                    <div class="col-sm-3 exshadow">
                        <div class="border-top">
                            <i class="fa fa-car fa-2x"></i>
                            <a class=" text-decoration-none setmargin" href="" data-target="#mymodal8"
                                data-toggle="modal">Cars & Bikes</a>
                            <p class="setmargin1">things MyClickOnlineCars </p>
                            <!------------ form----------->
                            <div class="modal fade" id="mymodal8">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="mt-2 text-dark">How we can assist you?</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="sub_contactus.php" method="post"
                                                enctype="multipart/form-data">
                                                <h4>Cars & Bikes</h4>
                                                <input type="hidden" name="category" value="Cars & Bikes">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Name:</label>
                                                        <input type="text" class="form-control bdr_radius " name="name"
                                                            placeholder="Enter Name">
                                                        <label class="mt-2">Mobile Number:</label>
                                                        <input type="mobile" class="form-control bdr_radius " name="mobile"
                                                            placeholder="Enter Number">
                                                        <label class="mt-2">Email address:</label>
                                                        <input type="email" class="form-control bdr_radius " name="email"
                                                            placeholder="Enter Email ">
                                                    </div>
                                                    <div class="col-sm-6 float-right">
                                                        <label for="subject">Subject*</label>
                                                        <input type="sunject" class="form-control bdr_radius " name="subject"
                                                            placeholder="Enter Subject">
                                                        <label for="comment" class="mt-3">Comment:</label>
                                                        <textarea class="form-control bdr_radius " rows="4"
                                                            name="comment"></textarea>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button style="width:200px;margin:0px 280px;" type="submit"
                                                            class="btn btn-primary mt-4">ENTER</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------- End form----------->
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu1" class="container tab-pane fade">
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <form action="sub_contactus.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="category" value="Management">
                            <div class="row align-center">
                                <div class="col-sm-6 ">
                                    <label>Name:</label>
                                    <input type="text" class="form-control bdr_radius " name="name" placeholder="Enter Name">
                                    <label class="mt-2">Mobile Number:</label>
                                    <input type="mobile" class="form-control bdr_radius " name="mobile" placeholder="Enter Number">
                                    <label class="mt-2">Email address:</label>
                                    <input type="email" class="form-control bdr_radius " name="email" placeholder="Enter Email ">
                                </div>
                                <div class="col-sm-6 float-right">
                                    <label for="subject">Subject*</label>
                                    <input type="sunject" class="form-control bdr_radius " name="subject"
                                        placeholder="Enter Subject">
                                    <label for="comment" class="mt-3">Comment:</label>
                                    <textarea class="form-control bdr_radius " rows="4" name="comment"></textarea>
                                    <button type="submit" class="btn btn-danger mt-4 float-right">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Tab panes -->
    </div>
    <section class="bg-gray">
        <section class="container mt-5">
            <div class="row py-4">
                <div class="col-lg-5 col-md-5 col-sm-5 col-6 text-muted">
                    <h4>Did Not Find What You Were Looking For?</h4>
                    <p>You may want to get in touch with the team directly by selecting from the options below.</p>
                    <form class="" action="/action_page.php" id="formsetup" class="form-inline">
                        <div class="form-group form-inline">
                            <label for="" class="text-dark ml-2">Query for:</label>
                            <select name="cars" class="form-control bdr_radius ml-2">
                                <option selected>Select Category</option>
                                <option value="Entertainment">Advertising</option>
                                <option value=" Marketing"> Marketing</option>
                                <option value="Media">Media</option>
                                <option value=" Partnership"> Partnership</option>
                                <option value="Report Vulnerability">Report Vulnerability</option>
                            </select>
                            <button type="submit" class="btn btn-danger bdr_radius m-2">GO</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-6"></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 float-right text-muted">
                    <h4>Customer Care:</h4>
                    <h3>080-67364545</h3>
                    <h6>Monday - Saturday: 9AM to 9PM</h6>
                </div>
            </div>
        </section>
    </section>
    <div class="border-top mt-5"></div>
    <div class="mt-3"></div>
    <!-- FOOTER -->
<?php
include 'footer.php'
?>
</body>

</html>