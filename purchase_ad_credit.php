<?php
include 'header.php'
?>
<title>Purchase Ad Credit</title>

   <div class="border-top mt-2"></div>
<section class="container mt-5">
<div class="row border">
<div class="col-sm-8">
<h6 class="mt-3">Get MyClickOnline Premium Ad Credit packs</h6>
<p>Premium Ad Credits are an option to buy multiple Premium Ads at once with upto 80% discount</p>
</div>
<div class="col-sm-4 mt-3">
<button type="button" class="btn btn-info">Premium Ads Credits</button>
<button type="button" class="btn btn-success ml-4">Read FAQs</button>
</div>
</section>

<section class="container border text-center">
<h6 class="mt-3">Please provide your details to buy Ad Credits</h6>
<form action="payment/index.php" method="POST">
<div class="row mt-4">
<!-- <div class="col-sm-4">
            

                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">All India <span class="caret"></span></button>
                <ul class="dropdown-menu scrollable-menu" role="menu">
                    <li><a href="#">Ahmedabad</a></li>
                    <li><a href="#">Bangalore</a></li>
                    <li><a href="#">Chandigarh</a></li>
                    <li><a href="#">Chennai</a></li>
                    <li><a href="#">Coimbatore</a></li>
                    <li><a href="#">Delhi</a></li>
                    <li><a href="#">Gurgaon</a></li>
                    <li><a href="#">Hyderabad</a></li>
                    <li><a href="#">Jaipur</a></li>
                    <li><a href="#">Kochi</a></li>
                    <li><a href="#">Kolkata</a></li>
                    <li><a href="#">Lucknow</a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Navi Mumbai</a></li>
                    <li><a href="#">Noida</a></li>
                    <li><a href="#">Pune</a></li>
                    <li><a href="#">Trivandrum</a></li>

                </ul>
         
           

</div>
<div class="col-sm-4">
<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">Select Category <span class="caret"></span></button>
                <ul class="dropdown-menu scrollable-menu" role="menu">
                    <li><a href="#">Entertainment</a></li>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Home & Lifestyle</a></li>
                    <li><a href="#">Cars & Bikes</a></li>
                    <li><a href="#">Real Estate</a></li>
                    <li><a href="#">Community</a></li>
                    <li><a href="#">Matrimonial</a></li>
                    <li><a href="#">Education & Training</a></li>
                    <li><a href="#">Pets & Pet Care</a></li>
                    <li><a href="#">Electronics & Appliances</a></li>
                    <li><a href="#">Mobiles & Tablets</a></li>
                    <li><a href="#">Events</a></li>

                </ul>
                </div>
<div class="col-sm-4">
<label for="email">Email address:</label>
<input type="email"  id="email">
</div> -->
</div>
<section class="row">
<div class="col-sm-2"></div>
<div class="col-sm-8">
<p><h5 class="d-inline">Top of the page</h5> (1 Ad Pack = $590 Per Ad)</p>
<div class="border p-4">

<table class="table table-bordered">

      <tr>
        <td>
        
         <label class="form-check-label ml-3">
         <input type="radio" value="3,3 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">3 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$3</span>
         <span style="text-decoration:line-through;" class="float-right">$6</span>
         <p class="text-success">50% off</p>
        
        
        </td>
        <td>
        <label class="form-check-label ml-3">
         <input type="radio" value="5,5 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">5 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$5</span>
         <span style="text-decoration:line-through;" class="float-right">$10</span>
         <p class="text-success">50% off</p>
        
         </td>
        <td>
        <label class="form-check-label ml-3">
         <input type="radio" value="10,10 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">10 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$10</span>
         <span style="text-decoration:line-through;" class="float-right">$20</span>
         <p class="text-success">50% off</p>
        
         </td>
      </tr>
      <tr>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="15,15 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">15 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$15</span>
         <span style="text-decoration:line-through;" class="float-right">$30</span>
         <p class="text-success">50% off</p>
        </td>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="18,18 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">18 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$18</span>
         <span style="text-decoration:line-through;" class="float-right">$36</span>
         <p class="text-success">50% off</p>
        </td>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="20,20 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">20 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$20</span>
         <span style="text-decoration:line-through;" class="float-right">$40</span>
         <p class="text-success">50% off</p>
        </td>
      </tr>
      <tr>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="22,22 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">22 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$22</span>
         <span style="text-decoration:line-through;" class="float-right">$44</span>
         <p class="text-success">50% off</p>
        </td>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="30,30 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">30 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$30</span>
         <span style="text-decoration:line-through;" class="float-right">$60</span>
         <p class="text-success">50% off</p>
        </td>
        <td> <label class="form-check-label ml-3">
         <input type="radio" value="40,40 Ads Pack, 1 Month" class="form-check-input" name="adPlanData">40 Ads Pack
         </label>
         <p>1 Month</p>
         <span class="d-inline h4">$40</span>
         <span style="text-decoration:line-through;" class="float-right">$80</span>
         <p class="text-success">50% off</p>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<!-- <p class="h6 mt-3">OR Customize your pack</p>
<div class="row border p-4">
<div class="col-sm-6">
<input type="text" class="form-control" id="text" placeholder="Enter: Number of Ads(Max 25)">
</div>
<div class="col-sm-6 ">
<div class="dropdown float-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">Select Validity</button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">3 Months</a>
      <a class="dropdown-item" href="#">10 Months</a>
      <a class="dropdown-item" href="#">22 Months</a>

      
    </div>
  </div>

</div>

</div>
<div class="col-sm-2"></div> -->
</section>









<div class="row mt-5 mb-5">
<div class="col-sm-12">
<div class="text-center">
<button type="submit" style="width:40%;" class="btn btn-info">Purchase Pack</button>
</div>
</div>
</div>
</form>

</section>









   <div class="row mt-5">
            <div class="col-lg-7">
               <nav class="navbar navbar-expand-sm ">
                  <ul class="navbar-nav flex-wrap">
                     <li> <a href="aboutus.php" alt="any" class="nav-link"><i class="nav-item"></i> About us</a></li>
                     <li> <a href="contactus.php" alt="any" class="nav-link"><i class="nav-item"></i> Contact us</a>
                     </li>
                     <li class="nav-item"><a class="nav-link" href="career.php">Careers</a></li>
                    
                     <li class="nav-item"><a class="nav-link" href="adsales.php">Advertise with us</a></li>

                     <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                     <li> <a href="help.php" alt="any" class="nav-link"><i class="nav-item"></i> Help</a></li>
                     <li class="nav-item"><a class="nav-link" href="#">Premium Ads</a></li>
                  </ul>
               </nav>
               <p class="text-justify">Widely known as India’s no. 1 online classifieds platform, MyClickOnline is all
                  about you. Our aim is to empower every person in the country to independently connect with buyers and
                  sellers online. We care about you — and the transactions that bring you closer to your dreams. Want to
                  buy your first car? We’re here for you. Want to sell commercial property to buy your dream home? We’re
                  here for you. Whatever job you’ve got, we promise to get it done.</p>
               <p class="text-dark pt-0">At MyClickOnline, you can buy, sell or rent anything you can think of.<a
                     class="btn btn-danger ml-4" href="postfreead.php">Post Free Ad</a></p>
               <nav class="navbar navbar-expand-sm">
                  <ul class="navbar-nav flex-wrap">
                     <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Listing Policy</a></li>
                     <li class="nav-item"><a class="nav-link" href="t&c.php" alt="any" class="nav-link"><i
                              class="nav-item"></i> Terms of uses</a></li>
                     <li class="nav-item"><a class="nav-link" href="policies.php" alt="any">Privacy Policy</a></li>
                     <li class="nav-item"><a class="nav-link" href="policies.php" alt="any" class="nav-link"><i
                              class="nav-item"></i> mobile Policies</a></li>
                   
                     <li class="nav-item"><a class="nav-link" href="sitemap.php" alt="any">Sitemap</a></li>
                     <li class="nav-item"><a class="nav-link" href="#" alt="any">News</a></li>
                  </ul>
               </nav>
              
            </div>
            <div class="col-lg-5">
            <div class="row">
                    <nav class="navbar col-sm-3">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="">Malaysia</a></li>
                        </ul>
                    </nav>
                    <!-- A vertical navbar -->
                    <nav class="navbar col-sm-3">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="">Australia</a></li>
                        </ul>
                    </nav>
                    <nav class="navbar col-sm-3">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="">中国</a></li>
                        </ul>
                    </nav>
                    <!-- A vertical navbar -->
                    <nav class="navbar col-sm-3">
                        <!-- Links -->
                        <ul class="navbar-nav flex-wrap">
                            <li class="nav-item"><a class="nav-link" href="">Singapore</a></li>
                        </ul>
                    </nav>
                </div>
               <div class="row">
                  <h6>Download The App :
                     <a href="#" class="fa fa-apple font1"></a>
                     <a href="#" class="fa fa-android font2"></a>
                     <a href="#" class="fa fa-windows font3"></a>
                  </h6>
               </div>
            </div>
         </div>
      </div>


   <style>
   .scrollable-menu {
    height: auto;
    max-height: 2px;
    overflow-x: hidden;
}
</style>
   </body>
   </html>