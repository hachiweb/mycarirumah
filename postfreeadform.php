<?php
include 'header.php'
?>
<title>Post Free ad Form</title>
   <div class="border-top mt-2"></div>
      <section class="container border bg-white">
         <form action="sub_postfreeadform.php" method="post" enctype="multipart/form-data">
            <h2 class="ml-4 mt-4">POST FREE AD</h2>
            <p class="ml-2">If you have purchased an Ad pack, please login to “Post Premium Ad”<button
               class="btn btn-primary ml-5">Login</button></p>
            <p class="text-dark ml-2 pb-0">Post an Ad in just 30 seconds</p>
            <p class="ml-2 pt-0">Please DO NOT post multiple ads for the same items or service. All duplicate, spam and
               wrongly categorized ads will be deleted.<span class="float-right text-warning">*Mandatory Fields</span>
            </p>
            <div class="border-top pl-5 pr-5"></div>
            <div class="row">
               <div class="col-sm-8">
               <div class="row mt-4">
                     <div class="col-sm-6">
                     <?php
                        require_once 'dbconnect.php';
                        $parent_id= $_GET["Category"];
                        $subcategory = $_GET["Subcategory"];
                        $db     = new DB();
                        $sql    = "SELECT * FROM `category` WHERE `parent_id` = $parent_id"; 
                        $result = $db->executeQuery($sql);
                        while ($fetch = mysqli_fetch_assoc($result)) {
                           if($subcategory == $fetch['id']){
                              echo "<h5 class='d-inline'><strong>" .$fetch['source']. "</strong></h5>";
                           }
                        }
                     ?>
                  
                        <div class="mt-4">
                        <label for="comment">Change Category</label>
                        <!-- <a class="ml-4" href="#">Change Category</a> -->
                        <select name="education" class="custom-select mb-3">
                        <?php
                           $db     = new DB();
                           $sql    = "SELECT * FROM `category` WHERE `parent_id` = $parent_id"; 
                           $result = $db->executeQuery($sql);
                           while ($fetch = mysqli_fetch_assoc($result)) {
                              if($subcategory == $fetch['id']){
                              echo '<option value="'.$fetch['category_title'].'" selected>'.$fetch['category_title'].'</option>';
                           }else{
                              echo '<option value="'.$fetch['category_title'].'">'.$fetch['category_title'].'</option>';
                           }
                           }
                        ?>
                        </select>
                        </div>
                     </div>
                     <div class="col-sm-6"></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <h5>Ad Information</h5>
                        <div class="form-group d-block mt-4">
                           <label for="comment">Title for your ad*</label>
                           <textarea class="form-control" rows="1" name="title" placeholder="write title..."></textarea>
                           <span>(Minimum 4 words)</span>
                        </div>
                        <div class="form-group d-block mt-4">
                           <label for="comment">Ad Description*</label>
                           <textarea class="form-control" rows="5" name="description"placeholder="write description here..."></textarea>
                           <span>(Minimum 8 words)</span>
                        </div>
                        <span class="backcolor">Free Ads have a <span class="text-danger">25 words</span> limit, To remove
                        this limit please opt for our Premium or Gold Ads</span>
                        <div class="border mt-2">
                           <div class="set-padd">
                              <h5>Photos for your ad</h5>
                              <div class="custom-file">
                                 <!-- <input type="file" name="image[]" class="custom-file-input" id="customFile" multiple>
                                 <label class="custom-file-label" for="customFile">Choose file</label> -->
                                 <div class="field custom-file" style="display:none;">
                                 <label class="custom-file-label" name="Photos" for="files">Choose file</label>
                                 <input type="file" id="files" name="image[]" class="" multiple />
                                 </div>
                                 <div class="dotted-box d-flex justify-content-start remove1" id="remove1">
                                    <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                                    <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                                    <span for="files"><p><i class="fas fa-camera"></i><br>Upload Photos</p></span>
                                    <input type="hidden" id="files1" >
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-5">
                     <div class="col-sm-6">
                        <select name="board" class="custom-select mb-3">
                           <option selected>School Board*</option>
                           <option value="volvo">CBSC</option>
                           <option value="fiat">ICSC</option>
                           <option value="audi">IGCSE</option>
                           <option value="audi">ISC</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <select name="subject" class="custom-select mb-3">
                           <option selected>Subjects*</option>
                           <option value="volvo">Accounting</option>
                           <option value="fiat">Biology</option>
                           <option value="audi">Business Studies</option>
                           <option value="audi">Chemistry</option>
                           <option value="audi">Computer science</option>
                        </select>
                     </div>
                  </div>
                  <div class="row mt-5">
                     <div class="col-sm-6">
                        <select name="standard" class="custom-select mb-3">
                           <option selected>Standard*</option>
                           <option value="volvo">CBSC</option>
                           <option value="fiat">ICSC</option>
                           <option value="audi">IGCSE</option>
                           <option value="audi">ISC</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <select name="delivery_mode" class="custom-select mb-3">
                           <option selected>Delivery Mode*</option>
                           <option value="volvo">Accounting</option>
                           <option value="fiat">Biology</option>
                           <option value="audi">Business Studies</option>
                           <option value="audi">Chemistry</option>
                           <option value="audi">Computer science</option>
                        </select>
                     </div>
                  </div>
                  <div class="row mt-5">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="usr">Monthly Tuition Fees*</label>
                           <input type="number" class="form-control" name="fees">
                           <input type="hidden" name="Parent" value="<?php echo $parent_id; ?>" class="form-control">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="usr">Institute Name*</label>
                           <input type="text" class="form-control" name="institute_name">
                        </div>
                     </div>
                  </div>
                  <div class="row mt-5">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="usr">Institute Address*</label>
                           <input type="text" class="form-control" name="institute_adderess">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="usr">Institute Website*</label>
                           <input type="text" class="form-control" name="institute_website">
                        </div>
                     </div>
                  </div>
                  <div class="row align-set">
                     <h6>Launching Offer Zone</h6>
                     <p>Run Offers & Discounts on your AD and get more responses</p>
                     <div class="col-sm-4"><span>*Receive more responses<br>
                        from the Users</span>
                     </div>
                     <div class="col-sm-4"><span>*AD highlighted with offer<br>
                        tag for a fixed duration</span>
                     </div>
                     <div class="col-sm-4"><span>*Offer Ads featured<br>
                        in a separate section</span>
                     </div>
                     <div class="col-sm-6 mt-4">
                        <div class="form-group">
                           <label for="usr">Discount Percentage*</label>
                           <input type="text" class="form-control" name="discount_percentage" placeholder="i.e  23%">
                        </div>
                     </div>
                     <div class="col-sm-6 mt-4">
                        <label>Duration( in days)</label>
                        <select name="duration" class="custom-select mb-3">
                           <option selected>Delivery Mode*</option>
                           <option value="volvo">Accounting</option>
                           <option value="fiat">Biology</option>
                           <option value="audi">Business Studies</option>
                           <option value="audi">Chemistry</option>
                           <option value="audi">Computer science</option>
                        </select>
                     </div>
                     <div class="border-top border-primary pl-5 pr-5"></div>
                     <div class="form-check-inline mt-3">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="enable_offer_zone" value="Yes">Enable Offer Zone <span
                           class="text-muted ml-2">This feature is only available for Premium or Gold Ads</span>
                        </label>
                     </div>
                  </div>
                  <h4 class="mt-3">Location Details</h4>
                  <div class="row mt-3">
                     <div class="col-sm-6">
                        <select name="locality" class="custom-select mb-3">
                           <option selected>Your Locality*</option>
                           <option value="volvo">Accounting</option>
                           <option value="fiat">Biology</option>
                           <option value="audi">Business Studies</option>
                           <option value="audi">Chemistry</option>
                           <option value="audi">Computer science</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                     </div>
                  </div>
                  <h4 class="mt-3">User Information</h4>
                  <div class="row mt-2">
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="usr">Your Name*</label>
                           <input type="text" class="form-control" name="name" placeholder="Enter Name">
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="usr">Your Email ID*</label>
                           <input type="email" class="form-control" name="email" placeholder="Enter Email">
                        </div>
                     </div>
                  
                  
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="usr">Your Mobile*</label>
                           <input type="number" class="form-control" name="mobile" placeholder="Enter Email">
                        </div>
                     </div>
                        </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-check-inline mt-3">
                           <label class="form-check-label">
                           <input type="checkbox" class="form-check-input text-justify" name="maintain_my_privacy" value="Yes">Maintain my Privacy
                           (Buyers will first contact you via MyClickOnline Chat, you can decide when to reveal your phone
                           number to them)
                           </label>
                        </div>
                        <div class="form-check-inline mt-3">
                           <label class="form-check-label">
                           <input type="checkbox" class="form-check-input text-justify" name="send_me_email" value="Yes">Send me MyClickOnline
                           Email/SMS Alerts for people looking for Schools & School Tuitions in Trichy
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4"></div>
            </div>
            <div class="border-top mt-4"></div>
            <div class="row">
               <div class="col-sm-12">
                  <h4 class="mt-4">Make your Ad Premium</h4>
                  <table class="table table-hover table-bordered  mt-4">
                     <thead>
                        <tr>
                           <th>Premium Ad Benefits</th>
                           <th>Free Ad<br>₹0 / Month</th>
                           <th>Premium <br>Ad₹708 / Month</th>
                           <th> Gold Ad <br>₹1416 / 7 Days</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>Ad Visibility</td>
                           <td>Below Premium Ads & Gold Ads</td>
                           <td>Above all Regular Ads</td>
                           <td>Above all Premium Ads</td>
                        </tr>
                        <tr>
                           <td>Fixed Ad Position</td>
                           <td>No</td>
                           <td>No</td>
                           <td>Always among top 3 Ads for 7 days</td>
                        </tr>
                        <tr>
                           <td>Ad Validity</td>
                           <td>2 Week free</td>
                           <td>4 Week Premium + 2 Week Free</td>
                           <td>1 Week Gold + 4 Week Premium + 2 Week Fre</td>
                        </tr>
                        <tr>
                           <td>No. of Ad Views</td>
                           <td>50 Views</td>
                           <td>500 Views</td>
                           <td>Unlimited</td>
                        </tr>
                        </tr>
                        <tr>
                           <td>No. of photos Allowed</td>
                           <td>1 Photo per Ad</td>
                           <td>Unlimited</td>
                           <td>Unlimited</td>
                        </tr>
                        </tr>
                        <tr>
                           <td>Word limit in Ad Description</td>
                           <td>25 Words</td>
                           <td>Unlimited</td>
                           <td>Unlimited</td>
                        </tr>
                        </tr>
                        <tr>
                           <td>Interested Uesr Details</td>
                           <td>No</td>
                           <td>No</td>
                           <td>Yes (via SMS & Email)</td>
                        </tr>
                        <tr>
                           <td>Ad Renewal</td>
                           <td>Not allowed</td>
                           <td>Renewal after 4 Week (with 25% discount)</td>
                           <td>Renewal after 1 Week (with 50% discount)</td>
                        </tr>
                        <tr>
                           <td>Please Select Premium </td>
                           <td>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" value="Free" name="ad_plan">Select
                                 </label>
                              </div>
                           </td>
                           <td>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" value="Premium" name="ad_plan">Select
                                 </label>
                              </div>
                           </td>
                           <td>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" value="Gold" name="ad_plan">Select
                                 </label>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <p class="mt-4">By clicking "Post", you agree to our <a href="t&c.php" alt="any">Terms of Use</a> and <a
                     href="policies.php" alt="policy">Privacy Policy</a> and acknowledge that you are the rightful owner
                     of this item and using MyClickOnline to find a genuine buyer.
                  </p>
                  <div class="text-center p-5">
                  <button type='submit' value='Upload' name='upload' class="btn btn-danger w-50 btn-custom" style="font-size:20px;">POST</button>
                  </div>
               </div>
            </div>
         </form>
      </section>
      <!----------New section created----------->
      <div class="container">
         <div class="row py-3">
            <div class="col-sm-12 exshadow border download-via-dv p-0">
               <div style="background-image: url('assets/images/23.jpg'); background-size:cover;width:100%;">
                  <div class="row">
                     <div class="col-sm-2"></div>
                     <div class="col-lg-5 mt-4">
                        <a class="text-decoration-none pl-2 text-center" href="#" alt="any">
                           <h4>Download The MyClickOnline App</h4>
                        </a>
                        <p class="text-center">Buy or Sell anything using the app on your mobile. Find Jobs,
                           Homes, Services and more.
                        </p>
                        <div class="ml-5">
                           <div class="row flex-wrap social-icons my-3">
                           <h6>Download The App :
                           <a href="#" class="fa fa-apple font1"></a>
                           <a href="#" class="fa fa-android font2"></a>
                           <a href="#" class="fa fa-windows font3"></a>
                  </h6>
               </div>
                        </div>
                     </div>
                     <div class="col-lg-5 mt-3">
                        <h3 class="text-primary">Download via SMS</h3>
                        <form class="form-inline">
                           <input type="number" class="form-control bdr_radius" placeholder="EnterMobile Number">
                           <button class="btn btn-danger bdr_radius ml-3" type="submit">Submit</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="border-top"></div>
  <!-- FOOTER -->
<?php
include 'footer.php'
?>

<script>
   $(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
       $("#remove1>span").hide();
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files1");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
            if($(".pip").length){
            }
            else{
               $("#remove1>span").show();

            }
          });
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
  $('.remove1>span').click(function() {
   $("#files").click();
    });
});
   </script>
   </body>
</html>