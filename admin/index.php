<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:pages/auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('header.php'); 
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3>1<sup style="font-size: 20px">Home</sup></h3>

                <p>Home</p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
              <a href="<?php echo $site_url ?>/admin/index.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3><?php require_once(__DIR__.'/../dbconnect.php');$db=new DB();$sql="with cterc as (SELECT COUNT(*) as rn FROM jobpostform UNION ALL SELECT COUNT(*) FROM postfreeadform UNION ALL SELECT COUNT(*) FROM postadform) SELECT SUM(rn) as total from cterc";$result=$db->executeQuery($sql);$total=mysqli_fetch_assoc($result);echo $total['total']; ?><sup style="font-size: 20px"> Ads</sup></h3>

                <p>Ads Details</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo $site_url ?>/admin/pages/tables/ads.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><h3><?php require_once(__DIR__.'/../dbconnect.php');$db=new DB();$sql="SELECT * FROM `users`";$result=$db->executeQuery($sql);$total_sql="SELECT count(*) as `count` FROM `users`";$total=$db->executeQuery($total_sql);$total=mysqli_fetch_assoc($total);echo $total['count']; ?><sup style="font-size: 20px"> Users</sup></h3></h3>

                <p>User Details</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="<?php echo $site_url ?>/admin/pages/users/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><h3><h3><?php require_once(__DIR__.'/../dbconnect.php');$db=new DB();$sql="SELECT * FROM `users`";$result=$db->executeQuery($sql);$total_sql="SELECT count(*) as `count` FROM `users`";$total=$db->executeQuery($total_sql);$total=mysqli_fetch_assoc($total);echo $total['count']; ?><sup style="font-size: 20px"> Profile</sup></h3></h3></h3>

                <p>Profile View</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="<?php echo $site_url ?>/admin/pages/users/profile.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('footer.php'); ?>