<?php
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ad Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Ad Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <?php
        $db = new DB();
        $id = $_GET['ID'];
        $P = $_GET['P'];
        if ($P<6 or $P>12) {
            $T = "postadform";
        }
        elseif ($P=="6") {
            $T = "jobpostform";
        }else{
            $T = "postfreeadform";
        }
        $sql = "SELECT * FROM `$T` WHERE id = '$id'";
        $result = $db->executeQuery($sql);
        $data = mysqli_fetch_array($result);
        if ($P<6 or $P>12) {?>
        <div class="row">
        <div class="col-md-2"> </div>
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Ad Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="edit-ad-sub.php" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" id="Title" name="Title" value="<?=$data['title'];?>">
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Price">Price</label>
                    <input type="price" class="form-control" id="Price" name="Price" value="<?=$data['price'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="GSTNo">GST No.</label>
                    <input type="text" class="form-control" id="GSTNo" name="GSTNo" value="<?=$data['gst_number'];?>">
                  </div>
                </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Condition">Condition</label>
                    <input type="text" class="form-control" id="Condition" name="Condition" value="<?=$data['condition'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Quantity">Quantity</label>
                    <input type="number" class="form-control" id="Quantity" name="Quantity" value="<?=$data['quantity'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Saler">Saler Type</label>
                    <input type="text" class="form-control" id="Saler" name="Saler" value="<?=$data['saler_type'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Pincode">Pincode</label>
                    <input type="number" class="form-control" id="Pincode" name="Pincode" value="<?=$data['pincode'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Mobile">Mobile</label>
                    <input type="number" class="form-control" id="Mobile" name="Mobile" value="<?=$data['mobile'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="email" class="form-control" id="Email" name="Email" value="<?=$data['email'];?>">
                    <input type="hidden" name="ID" value="<?=$data['id'];?>">
                    <input type="hidden" name="P" value="<?=$data['parent_id'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="Description">Description</label>
                    <textarea class="form-control" id="Description" name="Description" rows="5"><?=$data['description'];?></textarea>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        </div><!-- /.row -->
        <?php
        }
        elseif ($P=="6") {?>
        <div class="row">
        <div class="col-md-2"> </div>
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Ad Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="edit-ad-sub.php" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="job_title">Title</label>
                    <input type="text" class="form-control" id="job_title" name="job_title" value="<?=$data['job_title'];?>">
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="company_name">Company</label>
                    <input type="price" class="form-control" id="company_name" name="company_name" value="<?=$data['company_name'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="role">Role</label>
                    <input type="text" class="form-control" id="role" name="role" value="<?=$data['role'];?>">
                  </div>
                </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="salary_min">Salary Min</label>
                    <input type="price" class="form-control" id="salary_min" name="salary_min" value="<?=$data['salary_min'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="salary_max">Salary Max</label>
                    <input type="text" class="form-control" id="salary_max" name="salary_max" value="<?=$data['salary_max'];?>">
                  </div>
                </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="experience_min">Experience Min</label>
                    <input type="text" class="form-control" id="experience_min" name="experience_min" value="<?=$data['experience_min'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="experience_max">Experience Max</label>
                    <input type="number" class="form-control" id="experience_max" name="experience_max" value="<?=$data['experience_max'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" class="form-control" id="city" name="city" value="<?=$data['city'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="locality">Locality</label>
                    <input type="text" class="form-control" id="locality" name="locality" value="<?=$data['locality'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="number">Mobile</label>
                    <input type="number" class="form-control" id="number" name="number" value="<?=$data['number'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?=$data['email'];?>">
                    <input type="hidden" name="ID" value="<?=$data['id'];?>">
                    <input type="hidden" name="P" value="<?=$data['parent_id'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="5"><?=$data['description'];?></textarea>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        </div><!-- /.row -->
      <?php
        }else{?>
        <div class="row">
        <div class="col-md-2"> </div>
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Ad Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="edit-ad-sub.php" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="<?=$data['title'];?>">
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="board">Board</label>
                    <input type="text" class="form-control" id="board" name="board" value="<?=$data['board'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="delivery_mode">Delivery Mode</label>
                    <input type="text" class="form-control" id="delivery_mode" name="delivery_mode" value="<?=$data['delivery_mode'];?>">
                  </div>
                </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="standard">Standard</label>
                    <input type="text" class="form-control" id="standard" name="standard" value="<?=$data['standard'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control" id="subject" name="subject" value="<?=$data['subject'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="fees">Fees</label>
                    <input type="text" class="form-control" id="fees" name="fees" value="<?=$data['fees'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="discount_percentage">Discount %</label>
                    <input type="text" class="form-control" id="discount_percentage" name="discount_percentage" value="<?=$data['discount_percentage'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="duration">Duration</label>
                    <input type="text" class="form-control" id="duration" name="duration" value="<?=$data['duration'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="locality">Locality</label>
                    <input type="text" class="form-control" id="locality" name="locality" value="<?=$data['locality'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="institute_name">Institute Name</label>
                    <input type="text" class="form-control" id="institute_name" name="institute_name" value="<?=$data['institute_name'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="institute_adderess">Institute Adderess</label>
                    <input type="text" class="form-control" id="institute_adderess" name="institute_adderess" value="<?=$data['institute_adderess'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="institute_website">Institute Website</label>
                    <input type="text" class="form-control" id="institute_website" name="institute_website" value="<?=$data['institute_website'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?=$data['email'];?>">
                    <input type="hidden" name="ID" value="<?=$data['id'];?>">
                    <input type="hidden" name="P" value="<?=$data['parent_id'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?=$data['name'];?>">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="number" class="form-control" id="mobile" name="mobile" value="<?=$data['mobile'];?>">
                  </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="5"><?=$data['description'];?></textarea>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        </div><!-- /.row -->
      <?php
        }
        ?>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php
include '../../footer.php';
 ?>