<?php
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ads</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Ads</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <?php
                $db = new DB();
                $sql = "SELECT * FROM `category` WHERE `parent_id` = 0 AND `id`!=160";
                $result = $db->executeQuery($sql);
                ?>
              <div class="card-header">
                <h3 class="card-title">Categories</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Categories</th>
                    <th>Active</th>
                    <th>Inactive</th>
                    <th style="width: 40px">Total</th>
                  </tr>
                  <?php
                  while ($category = mysqli_fetch_array($result)) {
                    $parent_id = $category['id'];
                    if ($parent_id<6 or $parent_id>12) {
                      $table = "postadform";
                    } elseif ($parent_id=="6") {
                      $table = "jobpostform";
                    }else{
                      $table = "postfreeadform";
                    }
                    $sqlactive = "SELECT * FROM `$table` WHERE `parent_id` = '$parent_id' AND `is_active` = 1";
                    $sqlinactive = "SELECT * FROM `$table` WHERE `parent_id` = '$parent_id' AND `is_active` = 0";
                    $resultactive = $db->executeQuery($sqlactive);
                    $resultinactive = $db->executeQuery($sqlinactive);
                    $total = $resultactive->num_rows + $resultinactive->num_rows;
                    ?>
                  <tr>
                    <td><?=$category['id'];?>.</td>
                    <td><a href="ads-details.php?P=<?=$parent_id;?>"><?=$category['category_title'];?></a></td>
                    <td><?=$resultactive->num_rows;?></td>
                    <td><?=$resultinactive->num_rows;?></td>
                    <td><?=$total;?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include '../../footer.php';
?>