<?php
error_reporting(0);
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ads Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Ads Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <?php
                $A = $_GET['A'];
                $I = $_GET['I'];
                $P = $_GET['P'];
                if ($P<6) {
                  $T = "postadform";
                }
                elseif ($P=="6") {
                    $T = "jobpostform";
                }else{
                    $T = "postfreeadform";
                }
                $db = new DB();
                $sql = "SELECT * FROM `$T` WHERE `parent_id` = '$P'";
                $result = $db->executeQuery($sql);
                $cn = "SELECT * FROM `category` WHERE `parent_id` = '$P'";
                $rcn = $db->executeQuery($cn);
                $dcn = mysqli_fetch_array($rcn);
                ?>
              <div class="card-header">
                <h3 class="card-title"><?=(!empty($dcn['source']))?$dcn['source']:"Categories";?></h3>
              </div>
              <?php 
                if ($P<6 or $P>12) {?>
             <!-- /.card-header -->
              <div class="card-body p-0" style="overflow-x: scroll;">
                <table class="table table-striped text-center">
                  <tr>
                    <th style="width: 10px;">#</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Condition</th>
                    <th>Quantity</th>
                    <th>GST No.</th>
                    <th>Description</th>
                    <th>Saler Type</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Pincode</th>
                    <th>Posted</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Disable</th>
                  </tr>
                  <?php
                  while ($data = mysqli_fetch_array($result)) {?>
                  <tr>
                    <td><?=$data['id'];?>.</td>
                    <td><?=$data['ad_category'];?></td>
                    <td><?=$data['title'];?></td>
                    <td><?=$data['price'];?></td>
                    <td><?=$data['condition'];?></td>
                    <td><?=$data['quantity'];?></td>
                    <td><?=$data['gst_number'];?></td>
                    <td><?=$data['description'];?></td>
                    <td><?=$data['saler_type'];?></td>
                    <td><?=$data['mobile'];?></td>
                    <td><?=$data['email'];?></td>
                    <td><?=$data['pincode'];?></td>
                    <td><?=$data['posted_at'];?></td>
                    <td><a href="edit-ad.php?ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"></a></td>
                    <td><a onclick="adsDelete('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1214/1214594.svg" width="20" height="20"></a></td>
                    <?php
                    if($data['is_active']==1){
                      ?>
                      <td><a onclick="adBlock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827842.svg" width="20" height="20"></a></td>
                    <?php    
                    }
                    else{
                      ?>
                      <td><a onclick="adUnblock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827844.svg" width="20" height="20"></a></td>
                      <?php
                       }

                    ?>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
               <?php } elseif ($P==6) {?>
             <!-- /.card-header -->
              <div class="card-body p-0" style="overflow-x: scroll;">
                <table class="table table-striped text-center">
                  <tr>
                    <th style="width: 10px;">#</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Role</th>
                    <th>Salary Min</th>
                    <th>Salary Max</th>
                    <th>Experience Min</th>
                    <th>Experience Max</th>
                    <th>City</th>
                    <th>Locality</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Posted</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Disable</th>
                  </tr>
                  <?php
                  while ($data = mysqli_fetch_array($result)) {?>
                  <tr>
                    <td><?=$data['id'];?>.</td>
                    <td><?=$data['job_type'];?></td>
                    <td><?=$data['job_title'];?></td>
                    <td><?=$data['role'];?></td>
                    <td><?=$data['salary_min'];?></td>
                    <td><?=$data['salary_max'];?></td>
                    <td><?=$data['experience_min'];?></td>
                    <td><?=$data['experience_max'];?></td>
                    <td><?=$data['city'];?></td>
                    <td><?=$data['locality'];?></td>
                    <td><?=$data['company_name'];?></td>
                    <td><?=$data['email'];?></td>
                    <td><?=$data['number'];?></td>
                    <td><?=$data['posted_at'];?></td>
                    <td><a href="edit-ad.php?ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"></a></td>
                    <td><a onclick="adsDelete('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1214/1214594.svg" width="20" height="20"></a></td>
                    <?php
                    if($data['is_active']==1){
                      ?>
                      <td><a onclick="adBlock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827842.svg" width="20" height="20"></a></td>
                    <?php    
                    }
                    else{
                      ?>
                      <td><a onclick="adUnblock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827844.svg" width="20" height="20"></a></td>
                      <?php
                       }

                    ?>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
               <?php } else{?>
             <!-- /.card-header -->
              <div class="card-body p-0" style="overflow-x: scroll;">
                <table class="table table-striped text-center">
                  <tr>
                    <th style="width: 10px;">#</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Board</th>
                    <th>Subject</th>
                    <th>Standard</th>
                    <th>Delivery Mode</th>
                    <th>Duration</th>
                    <th>Fees</th>
                    <th>Institute Name</th>
                    <th>Institute Adderess</th>
                    <th>Institute Website</th>
                    <th>Discount %</th>
                    <th>Description</th>
                    <th>Locality</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Disable</th>
                  </tr>
                  <?php
                  while ($data = mysqli_fetch_array($result)) {?>
                  <tr>
                    <td><?=$data['id'];?>.</td>
                    <td><?=$data['education'];?></td>
                    <td><?=$data['title'];?></td>
                    <td><?=$data['board'];?></td>
                    <td><?=$data['subject'];?></td>
                    <td><?=$data['standard'];?></td>
                    <td><?=$data['delivery_mode'];?></td>
                    <td><?=$data['duration'];?></td>
                    <td><?=$data['fees'];?></td>
                    <td><?=$data['institute_name'];?></td>
                    <td><?=$data['institute_adderess'];?></td>
                    <td><?=$data['institute_website'];?></td>
                    <td><?=$data['discount_percentage'];?></td>
                    <td><?=$data['description'];?></td>
                    <td><?=$data['locality'];?></td>
                    <td><?=$data['name'];?></td>
                    <td><?=$data['email'];?></td>
                    <td><?=$data['mobile'];?></td>
                    <td><?=$data['posted_at'];?></td>
                    <td><a href="edit-ad.php?ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"></a></td>
                    <td><a onclick="adsDelete('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1214/1214594.svg" width="20" height="20"></a></td>
                    <?php
                    if($data['is_active']==1){
                      ?>
                      <td><a onclick="adBlock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827842.svg" width="20" height="20"></a></td>
                    <?php    
                    }
                    else{
                      ?>
                      <td><a onclick="adUnblock('ID=<?=$data['id'];?>&P=<?=$data['parent_id'];?>')"><img src="https://image.flaticon.com/icons/svg/1827/1827844.svg" width="20" height="20"></a></td>
                      <?php
                       }

                    ?>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
               <?php 
               }
               ?>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
  function adsDelete(id) {
    if(confirm("Are you sure you want to Delete?")){
      del="update-ad.php?A=1&"+id;
      window.location.href = del;
    }
  }
  function adBlock(id) {
    if(confirm("Are you sure you want to Block?")){
      block="update-ad.php?A=2&"+id;
      window.location.href = block;
    }
  
  }
  function adUnblock(id) {
    if(confirm("Are you sure you want to Unblock?")){
      unblock="update-ad.php?A=3&"+id;
      window.location.href = unblock;
    }
  }
</script>
<?php
include '../../footer.php';
?>