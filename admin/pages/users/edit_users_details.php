<?php
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registered Users Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_users_details_sub.php">
                <table>
  
                <?php 
                require_once '../../../dbconnect.php';
                $db = new DB();
                $id = $_GET['id'];
                $sql = "SELECT * FROM users WHERE id = '$id'";
                $result = $db->executeQuery($sql);
                $user_data = mysqli_fetch_array($result);
                ?>


                  <tr>
                  <td><label class="m-2" for="">ID</label></td>
                  <td><input type="text" name="id" value="<?php echo $user_data['id']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Username</label></td>
                  <td><input type="text" name="username" value="<?php echo $user_data['username']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Password</label></td>
                  <td><input type="text" name="password" value="<?php echo $user_data['password']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Alias</label></td>
                  <td><input type="text" name="alias" value="<?php echo $user_data['alias']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Role</label></td>
                  <td><input type="text" name="role" value="<?php echo $user_data['role']; ?>"></td>
                </tr>
                <input type="hidden" value="<?php echo $user_data['id']; ?>" name="id">
                </table>

                <div><input class="btn btn-block bg-gradient-primary btn-sm" style="width: 88px;margin-left: 182px;" type="submit" value="Update"></div>
              </form>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php
include '../../footer.php';
 ?>