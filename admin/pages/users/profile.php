<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('../../header.php'); 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile View</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profile View</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <?php 
                require_once("../../../dbconnect.php");
                $db = new DB();
                $sql = "SELECT * FROM users";
                $result = $db->executeQuery($sql);
                $n = 1;
                while ($user_data = mysqli_fetch_array($result)) {
                ?>
                  <tr style="<?php if(isset($_GET['id'])){
                    if($_GET['id'] == $user_data['id']){
                      echo 'background: #00FFFF';
                    };
                  }; ?>">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/user4-128x128.jpg"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?php echo $user_data['alias']; ?></h3>

                <p class="text-muted text-center"><?php echo $user_data['role']; ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Serial No.</b> <a class="float-right"><?php echo $n++ ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Allotted ID</b> <a class="float-right"><?php echo $user_data['id']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Registered</b> <a class="float-right"><?php echo $user_data['created_at']; ?></a>
                  </li>
                </ul>
                <a href="edit_users_details.php?id=<?php echo $user_data['id']; ?>" class="btn btn-primary btn-block"><b>Edit</b></a>
                <td><a onclick="userDelete(<?php echo $user_data['id'];?>)" class="btn btn-block btn-danger"><b>Delete</b></a></td>
              </div>
              <!-- /.card-body -->
            </div>
            <script>
                function userDelete(id) {
                  if(confirm("Are you sure you want to delete?")){
                    del="delete_profile.php?id="+id; 
                    window.location.href = del;
                  }
                
                }
                </script>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <?php
                }
          ?>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include '../../footer.php';
 ?>