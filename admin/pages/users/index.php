<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('../../header.php'); 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registered Users Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table class="table table-hover">
                <tr>
                  <th>Serial No.</th>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Alias</th>
                  <th>Role</th>
                  <th>Created at</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  <th>Block/Unblock</th>
                </tr>
                <?php 
                require_once("../../../dbconnect.php");
                $db = new DB();
                $sql = "SELECT * FROM users";
                $result = $db->executeQuery($sql);
                $n = 1;
                while ($user_data = mysqli_fetch_array($result)) {
                ?>
                  <tr style="<?php if(isset($_GET['id'])){
                    if($_GET['id'] == $user_data['id']){
                      echo 'background: #00FFFF';
                    };
                  }; ?>">
                    <td><?php echo $n++ ?></td>
                    <td><?php echo $user_data['id']; ?></td>
                    <td><?php echo $user_data['username']; ?></td>
                    <td><?php echo $user_data['alias']; ?></td>
                    <td><?php echo $user_data['role']; ?></td>
                    <td><?php echo $user_data['created_at']; ?></td>
                    <td><a href="edit_users_details.php?id=<?php echo $user_data['id']; ?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"></a></td>
                    <td><a onclick="userDelete(<?php echo $user_data['id'];?>)"><img src="https://image.flaticon.com/icons/svg/1214/1214594.svg" width="20" height="20"></a></td>
                    <?php
                    if($user_data['is_active']==1){
                      ?>
                      <td><a onclick="userBlock(<?php echo $user_data['id'];?>)"><img src="https://image.flaticon.com/icons/svg/564/564699.svg" width="20" height="20"></a></td>
                    <?php    
                    }
                    else{
                      ?>
                      <td><a onclick="userUnblock(<?php echo $user_data['id'];?>)"><img src="https://image.flaticon.com/icons/svg/25/25331.svg" width="20" height="20"></a></td>
                      <?php
                       }

                    ?>
                    
                  </tr>

                <?php }; ?>

              </table>
              <script>
                function userDelete(id) {
                  if(confirm("Are you sure you want to Delete?")){
                    del="delete_user.php?id="+id; 
                    window.location.href = del;
                  }
                
                }
                function userBlock(id) {
                  if(confirm("Are you sure you want to Block?")){
                    block="block_user.php?id="+id+"&action=block" ; 
                    window.location.href = block;
                  }
                
                }
                function userUnblock(id) {
                  if(confirm("Are you sure you want to Unblock?")){
                    unblock="block_user.php?id="+id+"&action=unblock" ; 
                    window.location.href = unblock;
                  }
                
                }
                </script>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php
include '../../footer.php';
 ?>