<?php
include 'header.php'
?>
<title>Career</title>
   <div class="border-top"></div>
   <div>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <div class="" style="background-image:url('assets/images/career1.jpg');">
                  <div class="text-center text-white text_set">
                     <h4>MyClickOnline is About</h4>
                     <h1 class="display-3 ">FLEXIBILITY</h1>
                  </div>
               </div>
            </div>
            <div class="carousel-item">
               <div class=" " style="background-image:url('assets/images/career2.jpg');">
                  <div class="text-center text-white text_set">
                     <h4>MyClickOnline is About</h4>
                     <h1 class="display-3 ">INNOVATION</h1>
                  </div>
               </div>
            </div>
            <div class="carousel-item">
               <div class="  " style="background-image:url('assets/images/career31.jpg');">
                  <div class="text-center text-white text_set">
                     <h4>MyClickOnline is About</h4>
                     <h1 class="display-3 ">TRANSPARENCY</h1>
                  </div>
               </div>
            </div>
            <div class="carousel-item">
               <div class="  " style="background-image:url('assets/images/career4.jpg');">
                  <div class="text-center text-white text_set">
                     <h4>MyClickOnline is About</h4>
                     <h1 class="display-3 ">FRUGALITY</h1>
                  </div>
               </div>
            </div>
            <div class="carousel-item">
               <div class=" " style="background-image:url('assets/images/career5.jpg');">
                  <div class="text-center text-white text_set">
                     <h4>MyClickOnline is About</h4>
                     <h1 class="display-3 ">SPEED</h1>
                  </div>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
   <!-- end carousel -->
   <section>
      <div class=""
         style="background-image:url('assets/images/career7.PNG');background-size:cover;">
         <div class="text_set1 p-3">
            <!-- <a href="index.php" alt="img not found"><img class="set_img d-inline" style="width:15%;" src="assets/images/myclick_logo.png"
                  alt=""></a> -->
            <h5 class="text-white">What Makes Us MyClickOnline-A Snapshot of an existing workspace</h5>
            <h4 class="text-white">What Makes Us MyClickOnline-A Snapshot of an existing workspace</h4>
         </div>
         <div class="icon-bar1">
            <a href="#" aly="any" class="btn btn-default border border-white exshadow">
               <i class="fa fa-youtube fa-2x text-white my-2"></i>
               <p style="font-size:20px;color:white;float:right;margin:13px;">Play Video</p>
            </a>
         </div>
      </div>
   </section>
   <section class="">
      
      <div><img class="set_backgroung3" src="assets/images/career8.PNG" alt="">
      </div>
   </section>
   <section class="set_backgroung4">
      <div class="container">
         <div class="row">
            <div class="mx-auto my-3">
               <h1 class="text-center text-white">Why Join Us?</h1>
            </div>
            <div class="mx-auto m-5">
               <img src="assets/images/career9.PNG" alt="img not found">
            </div>
         </div>
      </div>
   </section>
   <section class="container">
      <div class="row my-2" style="background-color:rgb(206,212,218)">
         <div class="mx-auto">
            <h1 class="text-dark p-3">People Speak</h1>
         </div>
      </div>
      <div class="row my-2">
      <div class="col-sm-12 col-md-12 col-lg-12 col-12">
         <!-- slick sider -->
         <div class="rio-promos">
            <!--Profile Card -->
            <div class="card profile-card-5">
               <div class="card-img-block">
                  <img class="card-img-top" src="assets/images/career10.jpg" alt="Card image cap">
               </div>
               <div class="card-body mx-auto">
                  <h5 class="card-title text-center">Gautam Reghunath</h5>
                  <h6 class="text-center text-dark">Business Director – Digital, Madison World</h6>
                  <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were very
                     responsive and went out of their way to ensure maximum value for us. Campaigns were set up under
                     tight deadlines and they were were measured and tweaked to ensure best results.
                  </p>
               </div>
            </div>
            <!--Profile Card -->
            <div class="card profile-card-5">
               <div class="card-img-block">
                  <img class="card-img-top" src="assets/images/career12.jpg" alt="Card image cap">
               </div>
               <div class="card-body">
                  <h5 class="card-title text-center">Ranjit Wilson</h5>
                  <h6 class="text-center text-dark">Business Director – Digital, Madison World
                  </h6>
                  <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were very
                     responsive and went out of their way to ensure maximum value for us. Campaigns were set up under
                     tight deadlines and they were were measured and tweaked to ensure best results."
                  </p>
               </div>
            </div>
            <!--Profile Card -->
            <div class="card profile-card-5">
               <div class="card-img-block">
                  <img class="card-img-top" src="assets/images/career13.jpg" alt="Card image cap">
               </div>
               <div class="card-body">
                  <h5 class="card-title text-center">Deepak Yohannan</h5>
                  <h6 class="text-center text-dark">Founder, MyInsuranceClub</h6>
                  <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were very
                     responsive and went out of their way to ensure maximum value for us. Campaigns were set up under
                     tight deadlines and they were were measured and tweaked to ensure best results.
                  </p>
               </div>
            </div>
            <!--Profile Card -->
            <div class="card profile-card-5">
               <div class="card-img-block">
                  <img class="card-img-top" src="assets/images/career14.jpg" alt="Card image cap">
               </div>
               <div class="card-body">
                  <h5 class="card-title text-center">Amit Duggal</h5>
                  <h6 class="text-center text-dark">Vice President, Omnicom Media Group</h6>
                     <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were
                        very responsive and went out of their way to ensure maximum value for us. Campaigns were set up
                        under tight deadlines and they were were measured and tweaked to ensure best results.
                     </p>
               </div>
            </div>
            <div class="card profile-card-5">
               <div class="card-img-block">
                  <img class="card-img-top" src="assets/images/career15.jpg" alt="Card image cap">
               </div>
               <div class="card-body">
                  <h5 class="card-title text-center">Amit Duggal</h5>
                  <h6 class="text-center text-dark">Vice President, Omnicom Media Group</h6>
                     <p class="card-text text-justify">"It was great to work with the Quikr Ad Sales team. They were
                        very responsive and went out of their way to ensure maximum value for us. Campaigns were set up
                        under tight deadlines and they were were measured and tweaked to ensure best results.
                     </p>
               </div>
            </div>
         </div>
         </div>
      </div>
      <!-- end slick slider -->
   </section>
   <section>
      <div class="d-block w-100"
         style="background-image:url('assets/images/career6.jpg');background-size:cover; ">
         <div class="container">
            <div class="row">
               <div class="set_border">
                  <!-- Modal Header -->
                  <div class="modal-header">
                     <h4 class="modal-title text-muted">Get In Touch</h4>
                  </div>
                  <!-- Modal body -->
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-sm-7">
                           <span><i class="fas fa-map-marker-alt fa-2x "></i></span>
                           <address class="text-muted text-justify text-left">
                              Address:
                              Gunta No. 106, Sub No. 5, 6, 7, 8 & 9,<br>Rachenahalli, (No. 167, Dr. S.R.K. Nagar
                              Post),<br> Krishnarajapuram Hobli,
                              Bangalore - 560045
                           </address>
                        </div>
                        <div class="col-sm-5">
                           <span><i class="far fa-envelope fa-2x "></i></span>
                           <div class="text-muted">
                              <h4>Send us your resume</h4>
                              <a href="#" alt=""> careers@myclickonline.com</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="row">
                        <div class=""></div>
                        <h3>Get In Touch</h3>
                        </div>
                        
                        <div class="row">
                        <div class="col-sm-1">
                           <i class="fas fa-map-marker-alt fa-2x"></i>
                        
                        </div>
                        <div class="col-sm-6">
                           <address>
                                 Address<br>
                                 Gunta No. 106, Sub No. 5, 6, 7, 8 & 9,<br> Rachenahalli, ( No. 167, Dr. S.R.K. Nagar Post ),<br> Krishnarajapuram Hobli, 
                                 Bangalore - 560045</address>
                        </div>
                        <div class="col-sm-5"></div>
                        </div> -->
               </div>
            </div>
         </div>
   </section>
   <?php
include 'footer.php'
?>
   </section>
   <script>
      $(document).ready(function () {
         $('.carousel').carousel({
            interval: 1000 * 1
         });
      });

   </script>
</body>

</html>