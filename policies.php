<?php
include 'header.php'
?>
<title>Policy</title>
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12"
            style="background-image: url('assets/images/19.jpg'); background-size:cover;height:250px;width:100%;">
            <h1 style="text-align:center;padding-top:100px;color:white">Policies</h1>
         </div>
      </div>
   </div>
   <section class="bg-white">
   <div class="container py-2">
      <nav class="navbar navbar-expand-sm static-nav anav">
         <ul class="navbar-nav flex-wrap">
            <li class="nav-item"><a class="" href="aboutus.php">About Us</a></li>
            <li class="nav-item"><a class="" href="contactus.php">Contact Us</a></li>
            <li class="nav-item"><a class="" href="policies.php">Policies</a></li>
            <li class="nav-item active"><a class="active" href="t&c.php">Terms & Conditions</a></li>
            <li class="nav-item"><a class="" href="help.php">Help Center</a></li>
         </ul>
      </nav>
   </div>
   </section>
   <div class="border-top"></div>
   <section class="container mt-3">
      <div class="row">
         <div class="col-lg-4 col-md-4 col-sm-4 col-6 bg-white p-3">
            <h3 class="text-muted">Policies</h3>
            <h5 class="mt-4 text-muted">MyClickOnline Links</h5>
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
               <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
                  aria-controls="v-pills-home" aria-selected="true">Listing Policy</a>
               <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
                  aria-controls="v-pills-profile" aria-selected="false">Privacy policy</a>
               <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab"
                  aria-controls="v-pills-messages" aria-selected="false">Mobile Policy</a>
               <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab"
                  aria-controls="v-pills-settings" aria-selected="false">Pets Policy</a>
            </div>
         </div>
         <div class="col-lg-8 col-md-8 col-sm-8 col-6 bg-white p-3">
            <div class="tab-content" id="v-pills-tabContent">
               <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                  aria-labelledby="v-pills-home-tab">
                  <h1 id="list-item-1" class="text-muted">Listing Policy</h1>
                  <section class="static-content-section text-justify">
                     <p>For use of our Site and other services, you confirm and declare that you shall not list or post
                        or provide information in relation to the sale or purchase or exchange of goods and services,
                        content or information that are illegal under the laws of the Republic of India and/or are not
                        permitted as per the prohibited items policy listed below.</p>
                  </section>
                  <div>
                     <p class="policy-sub-titles"></p>
                     <span>1.</span><strong>Prohibited Items Policy.</strong>
                     </p>
                  </div>
                  <section class="static-content-section text-justify">
                     <p class="policy-sub-titles">
                        <span>A. </span><strong> We specifically prohibit any listing or posting of classifieds or
                           information in relation to the following items:</strong>
                     </p>
                     <ul>
                        <li>Alcoholic Beverages, Liquor, tobacco products, drugs, psychotropic substances, narcotics,
                           intoxicants of any description, medicines, palliative/curative substances nor shall you
                           provide link directly or indirectly to or include descriptions of items, goods or services
                           that are prohibited under any applicable law for the time being in force including but not
                           limited to the Drugs and Cosmetics Act, 1940, the Drugs And Magic Remedies (Objectionable
                           Advertisements) Act, 1954 Narcotic Drug and Prohibited Substances Act and the Indian Penal
                           Code, 1860.</li>
                        <li>Living, dead person and/or the whole or any part of any human which has been kept or
                           preserved by any means whether artificial or natural including any blood, bodily fluids and/
                           or body parts</li>
                        <li>Prostitution or any other service in the nature there of that purports to violate the
                           provisions of Immoral Act or Indecent representation of women which violates the contemporary
                           standards of morality and decency in Indian society.</li>
                        <li>Religious items, including books, artifacts, etc. or any information, description of any
                           such item that is likely to affect the religious sentiments of any person or group</li>
                        <li>Mature Audiences Policy includes films which do not have a certificate for public exhibition
                           issued by the Central Board of Film Certification and or described and depict or otherwise
                           deal with matters which are revolting or repulsive and or tend to deprave a persons mind in
                           such a way that they tend to offend against the standards of morality, decency and propriety
                           generally accepted by reasonable adults</li>
                        <li>Obscene Items includes items which contain an indecent representation of women within the
                           meaning of the Indecent Representation of Women (Prohibition) Act, 1986; Any publication or
                           film or item that describes or depicts a minor who is, or who appears to be, under 18
                           (whether the minor is engaged in sexual activity or not) and any computer games not suitable
                           for minor that are unsuitable for a minor to see or play.</li>
                        <li>Offensive Material intended for use in a sexual setting (including "bondage" and "fetish"
                           items,) displaying sexual activity or portraying human genitalia in a "life-like" or
                           realistic fashion</li>
                        <li>Information that is fraudulent, misrepresenting as to the nature and use of the goods or the
                           services.</li>
                        <li>Counterfeit, Pirated and stolen goods or unauthorized illegal services (services for which
                           you are not licensed or permitted to do or do not have the authority to under take).</li>
                        <li>Items, goods and services that infringe or attempt to pass off any third parties
                           intellectual property or rights of publicity or moral rights and or purport's to breach any
                           persons right to privacy.</li>
                        <li>Electronically transmitting through any medium computer viruses of any type or any computer
                           program that facilitates hacking of a computer system which the intent to damage a computer
                           or computer network or intercept any personal data.</li>
                        <li>Your information shall not include any hate content, that is derogatory or slanderous in
                           nature that may directed to any individual or group or advocate violence against any users
                           individuals and or animals.</li>
                        <li>Hazardous chemicals and pesticides and/ or items in violation of Hazardous Chemicals Act,
                           1985.</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>B. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>"Securities" within the meaning of the Securities Contract Regulation Act, 1956, including
                           shares, bonds, debentures, etc. and/or any other financial instruments/assets of any
                           description</li>
                        <li>Living, dead creatures and/or the whole or any part of any animal which has been kept or
                           preserved by any means whether artificial or natural including rugs, skins, specimens of
                           animals, antlers, horns, hair, feathers, nails, teeth, musk, eggs, nests, other animal
                           products of any description the sale and purchase of which is prevented or restricted in any
                           manner by applicable laws (including those prohibited under The Wildlife Protection Act, 1972
                           and/ or The Environment Protection Act, 1986)</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>C. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>Shall not be defamatory, trade libelous, unlawfully threatening or unlawfully harassing.
                           Further shall not be fraudulent, misrepresenting, misleading or pertain to the sale of any
                           illegal, counterfeit, stolen goods and or services which do not belong to you or you do not
                           have the authority for. Further still shall not infringe any intellectual property, trade
                           secret, or other proprietary rights or rights of publicity or privacy of any third party.
                        </li>
                        <li>Shall not contain any viruses, Trojan horses, worms, time bombs, cancel bots, easter eggs or
                           other computer programming routines that may damage, detrimentally interfere with,
                           surreptitiously intercept or expropriate any system, data or personal information.</li>
                        <li>Shall not be allowed to libel anyone or include hate, derogatory, slanderous speech directed
                           at individuals or groups. You should not advocate violence against other users or individuals
                           or groups.</li>
                     </ul>
                     <div>
                        <span>2. </span><strong> In addition to the above and for the purposes of clarity all Users
                           shall be expected to adhere to and comply with the following Policies while listing of
                           items:</strong>
                     </div>
                     <ul>
                        <li><strong>Restricted Item Policy:</strong> In addition to the above prohibited items policy
                           users shall also adhere to and comply with the restricted items policy while listing, posting
                           or providing information in relation to any goods or services.</li>
                        <li><strong>Duplicate Ad listings are not allowed.</strong> Any ad posted more than once with
                           the same content or Title in the same city and category would be considered as a Duplicate
                           Ad. We advise you to post multiple ads only if you have different items or services for sale.
                           All duplicate ads would be deleted and posters penalized if the problem persists.</li>
                        <li><strong>Mature Audience/Sexually oriented material: </strong> relating to items that
                           includes items intended for use in sexual activity would not be permitted. (An example of
                           such classifieds relating to an item not normally permitted would be a classified for the
                           sale of a vibrator). Please also be aware that titles with graphic adult language are
                           inappropriate, regardless of the item contained in the listing itself.</li>
                        <div>
                     </ul>
                     <span>3.</span><strong>Consequences of Breach of Listing Policy</strong>
                     <ul>
                        <li><strong>Users who violate the prohibited items policy and or the restricted items policy may
                              be subject to the following actions</li>
                        </strong>
                     </ul>
                     <ol>
                        <li><span>1. </span>Suspension or termination of membership.</li>
                        <li><span>2. </span> Permanent blocking of access to the site.</li>
                        <li><span>3. </span> Reporting to Law Enforcement or Appropriate Authorities.</li>
                     </ol>
                  </section>
               </div>
               <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <h1 id="list-item-2" class="text-muted">Privacy policy</h1>
                  <section class="static-content-section">
                     <p>For use of our Site and other services, you confirm and declare that you shall not list or post
                        or provide information in relation to the sale or purchase or exchange of goods and services,
                        content or information that are illegal under the laws of the Republic of India and/or are not
                        permitted as per the prohibited items policy listed below.</p>
                  </section>
                  <div>
                     <p class="policy-sub-titles"></p>
                     <span>1.</span><strong>Prohibited Items Policy.</strong>
                     </p>
                  </div>
                  <section class="static-content-section">
                     <p class="policy-sub-titles">
                        <span>A. </span><strong> We specifically prohibit any listing or posting of classifieds or
                           information in relation to the following items:</strong>
                     </p>
                     <ul>
                        <li>Alcoholic Beverages, Liquor, tobacco products, drugs, psychotropic substances, narcotics,
                           intoxicants of any description, medicines, palliative/curative substances nor shall you
                           provide link directly or indirectly to or include descriptions of items, goods or services
                           that are prohibited under any applicable law for the time being in force including but not
                           limited to the Drugs and Cosmetics Act, 1940, the Drugs And Magic Remedies (Objectionable
                           Advertisements) Act, 1954 Narcotic Drug and Prohibited Substances Act and the Indian Penal
                           Code, 1860.</li>
                        <li>Living, dead person and/or the whole or any part of any human which has been kept or
                           preserved by any means whether artificial or natural including any blood, bodily fluids and/
                           or body parts</li>
                        <li>Prostitution or any other service in the nature there of that purports to violate the
                           provisions of Immoral Act or Indecent representation of women which violates the contemporary
                           standards of morality and decency in Indian society.</li>
                        <li>Religious items, including books, artifacts, etc. or any information, description of any
                           such item that is likely to affect the religious sentiments of any person or group</li>
                        <li>Mature Audiences Policy includes films which do not have a certificate for public exhibition
                           issued by the Central Board of Film Certification and or described and depict or otherwise
                           deal with matters which are revolting or repulsive and or tend to deprave a persons mind in
                           such a way that they tend to offend against the standards of morality, decency and propriety
                           generally accepted by reasonable adults</li>
                        <li>Obscene Items includes items which contain an indecent representation of women within the
                           meaning of the Indecent Representation of Women (Prohibition) Act, 1986; Any publication or
                           film or item that describes or depicts a minor who is, or who appears to be, under 18
                           (whether the minor is engaged in sexual activity or not) and any computer games not suitable
                           for minor that are unsuitable for a minor to see or play.</li>
                        <li>Offensive Material intended for use in a sexual setting (including "bondage" and "fetish"
                           items,) displaying sexual activity or portraying human genitalia in a "life-like" or
                           realistic fashion</li>
                        <li>Information that is fraudulent, misrepresenting as to the nature and use of the goods or the
                           services.</li>
                        <li>Counterfeit, Pirated and stolen goods or unauthorized illegal services (services for which
                           you are not licensed or permitted to do or do not have the authority to under take).</li>
                        <li>Items, goods and services that infringe or attempt to pass off any third parties
                           intellectual property or rights of publicity or moral rights and or purport's to breach any
                           persons right to privacy.</li>
                        <li>Electronically transmitting through any medium computer viruses of any type or any computer
                           program that facilitates hacking of a computer system which the intent to damage a computer
                           or computer network or intercept any personal data.</li>
                        <li>Your information shall not include any hate content, that is derogatory or slanderous in
                           nature that may directed to any individual or group or advocate violence against any users
                           individuals and or animals.</li>
                        <li>Hazardous chemicals and pesticides and/ or items in violation of Hazardous Chemicals Act,
                           1985.</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>B. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>"Securities" within the meaning of the Securities Contract Regulation Act, 1956, including
                           shares, bonds, debentures, etc. and/or any other financial instruments/assets of any
                           description</li>
                        <li>Living, dead creatures and/or the whole or any part of any animal which has been kept or
                           preserved by any means whether artificial or natural including rugs, skins, specimens of
                           animals, antlers, horns, hair, feathers, nails, teeth, musk, eggs, nests, other animal
                           products of any description the sale and purchase of which is prevented or restricted in any
                           manner by applicable laws (including those prohibited under The Wildlife Protection Act, 1972
                           and/ or The Environment Protection Act, 1986)</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>C. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>Shall not be defamatory, trade libelous, unlawfully threatening or unlawfully harassing.
                           Further shall not be fraudulent, misrepresenting, misleading or pertain to the sale of any
                           illegal, counterfeit, stolen goods and or services which do not belong to you or you do not
                           have the authority for. Further still shall not infringe any intellectual property, trade
                           secret, or other proprietary rights or rights of publicity or privacy of any third party.
                        </li>
                        <li>Shall not contain any viruses, Trojan horses, worms, time bombs, cancel bots, easter eggs or
                           other computer programming routines that may damage, detrimentally interfere with,
                           surreptitiously intercept or expropriate any system, data or personal information.</li>
                        <li>Shall not be allowed to libel anyone or include hate, derogatory, slanderous speech directed
                           at individuals or groups. You should not advocate violence against other users or individuals
                           or groups.</li>
                     </ul>
                     <div>
                        <span>2. </span><strong> In addition to the above and for the purposes of clarity all Users
                           shall be expected to adhere to and comply with the following Policies while listing of
                           items:</strong>
                     </div>
                     <ul>
                        <li><strong>Restricted Item Policy:</strong> In addition to the above prohibited items policy
                           users shall also adhere to and comply with the restricted items policy while listing, posting
                           or providing information in relation to any goods or services.</li>
                        <li><strong>Duplicate Ad listings are not allowed.</strong> Any ad posted more than once with
                           the same content or Title in the same city and category would be considered as a Duplicate
                           Ad. We advise you to post multiple ads only if you have different items or services for sale.
                           All duplicate ads would be deleted and posters penalized if the problem persists.</li>
                        <li><strong>Mature Audience/Sexually oriented material: </strong> relating to items that
                           includes items intended for use in sexual activity would not be permitted. (An example of
                           such classifieds relating to an item not normally permitted would be a classified for the
                           sale of a vibrator). Please also be aware that titles with graphic adult language are
                           inappropriate, regardless of the item contained in the listing itself.</li>
                        <div>
                     </ul>
                     <span>3.</span><strong>Consequences of Breach of Listing Policy</strong>
                     <ul>
                        <li><strong>Users who violate the prohibited items policy and or the restricted items policy may
                              be subject to the following actions</li>
                        </strong>
                     </ul>
                     <ol>
                        <li><span>1. </span>Suspension or termination of membership.</li>
                        <li><span>2. </span> Permanent blocking of access to the site.</li>
                        <li><span>3. </span> Reporting to Law Enforcement or Appropriate Authorities.</li>
                     </ol>
                  </section>
               </div>
               <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                  <h1 id="list-item-2" class="text-muted">Mobile Policy</h1>
                  <section class="static-content-section">
                     <p>For use of our Site and other services, you confirm and declare that you shall not list or post
                        or provide information in relation to the sale or purchase or exchange of goods and services,
                        content or information that are illegal under the laws of the Republic of India and/or are not
                        permitted as per the prohibited items policy listed below.</p>
                  </section>
                  <div>
                     <p class="policy-sub-titles"></p>
                     <span>1.</span><strong>Prohibited Items Policy.</strong>
                     </p>
                  </div>
                  <section class="static-content-section">
                     <p class="policy-sub-titles">
                        <span>A. </span><strong> We specifically prohibit any listing or posting of classifieds or
                           information in relation to the following items:</strong>
                     </p>
                     <ul>
                        <li>Alcoholic Beverages, Liquor, tobacco products, drugs, psychotropic substances, narcotics,
                           intoxicants of any description, medicines, palliative/curative substances nor shall you
                           provide link directly or indirectly to or include descriptions of items, goods or services
                           that are prohibited under any applicable law for the time being in force including but not
                           limited to the Drugs and Cosmetics Act, 1940, the Drugs And Magic Remedies (Objectionable
                           Advertisements) Act, 1954 Narcotic Drug and Prohibited Substances Act and the Indian Penal
                           Code, 1860.</li>
                        <li>Living, dead person and/or the whole or any part of any human which has been kept or
                           preserved by any means whether artificial or natural including any blood, bodily fluids and/
                           or body parts</li>
                        <li>Prostitution or any other service in the nature there of that purports to violate the
                           provisions of Immoral Act or Indecent representation of women which violates the contemporary
                           standards of morality and decency in Indian society.</li>
                        <li>Religious items, including books, artifacts, etc. or any information, description of any
                           such item that is likely to affect the religious sentiments of any person or group</li>
                        <li>Mature Audiences Policy includes films which do not have a certificate for public exhibition
                           issued by the Central Board of Film Certification and or described and depict or otherwise
                           deal with matters which are revolting or repulsive and or tend to deprave a persons mind in
                           such a way that they tend to offend against the standards of morality, decency and propriety
                           generally accepted by reasonable adults</li>
                        <li>Obscene Items includes items which contain an indecent representation of women within the
                           meaning of the Indecent Representation of Women (Prohibition) Act, 1986; Any publication or
                           film or item that describes or depicts a minor who is, or who appears to be, under 18
                           (whether the minor is engaged in sexual activity or not) and any computer games not suitable
                           for minor that are unsuitable for a minor to see or play.</li>
                        <li>Offensive Material intended for use in a sexual setting (including "bondage" and "fetish"
                           items,) displaying sexual activity or portraying human genitalia in a "life-like" or
                           realistic fashion</li>
                        <li>Information that is fraudulent, misrepresenting as to the nature and use of the goods or the
                           services.</li>
                        <li>Counterfeit, Pirated and stolen goods or unauthorized illegal services (services for which
                           you are not licensed or permitted to do or do not have the authority to under take).</li>
                        <li>Items, goods and services that infringe or attempt to pass off any third parties
                           intellectual property or rights of publicity or moral rights and or purport's to breach any
                           persons right to privacy.</li>
                        <li>Electronically transmitting through any medium computer viruses of any type or any computer
                           program that facilitates hacking of a computer system which the intent to damage a computer
                           or computer network or intercept any personal data.</li>
                        <li>Your information shall not include any hate content, that is derogatory or slanderous in
                           nature that may directed to any individual or group or advocate violence against any users
                           individuals and or animals.</li>
                        <li>Hazardous chemicals and pesticides and/ or items in violation of Hazardous Chemicals Act,
                           1985.</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>B. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>"Securities" within the meaning of the Securities Contract Regulation Act, 1956, including
                           shares, bonds, debentures, etc. and/or any other financial instruments/assets of any
                           description</li>
                        <li>Living, dead creatures and/or the whole or any part of any animal which has been kept or
                           preserved by any means whether artificial or natural including rugs, skins, specimens of
                           animals, antlers, horns, hair, feathers, nails, teeth, musk, eggs, nests, other animal
                           products of any description the sale and purchase of which is prevented or restricted in any
                           manner by applicable laws (including those prohibited under The Wildlife Protection Act, 1972
                           and/ or The Environment Protection Act, 1986)</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>C. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>Shall not be defamatory, trade libelous, unlawfully threatening or unlawfully harassing.
                           Further shall not be fraudulent, misrepresenting, misleading or pertain to the sale of any
                           illegal, counterfeit, stolen goods and or services which do not belong to you or you do not
                           have the authority for. Further still shall not infringe any intellectual property, trade
                           secret, or other proprietary rights or rights of publicity or privacy of any third party.
                        </li>
                        <li>Shall not contain any viruses, Trojan horses, worms, time bombs, cancel bots, easter eggs or
                           other computer programming routines that may damage, detrimentally interfere with,
                           surreptitiously intercept or expropriate any system, data or personal information.</li>
                        <li>Shall not be allowed to libel anyone or include hate, derogatory, slanderous speech directed
                           at individuals or groups. You should not advocate violence against other users or individuals
                           or groups.</li>
                     </ul>
                     <div>
                        <span>2. </span><strong> In addition to the above and for the purposes of clarity all Users
                           shall be expected to adhere to and comply with the following Policies while listing of
                           items:</strong>
                     </div>
                     <ul>
                        <li><strong>Restricted Item Policy:</strong> In addition to the above prohibited items policy
                           users shall also adhere to and comply with the restricted items policy while listing, posting
                           or providing information in relation to any goods or services.</li>
                        <li><strong>Duplicate Ad listings are not allowed.</strong> Any ad posted more than once with
                           the same content or Title in the same city and category would be considered as a Duplicate
                           Ad. We advise you to post multiple ads only if you have different items or services for sale.
                           All duplicate ads would be deleted and posters penalized if the problem persists.</li>
                        <li><strong>Mature Audience/Sexually oriented material: </strong> relating to items that
                           includes items intended for use in sexual activity would not be permitted. (An example of
                           such classifieds relating to an item not normally permitted would be a classified for the
                           sale of a vibrator). Please also be aware that titles with graphic adult language are
                           inappropriate, regardless of the item contained in the listing itself.</li>
                        <div>
                     </ul>
                     <span>3.</span><strong>Consequences of Breach of Listing Policy</strong>
                     <ul>
                        <li><strong>Users who violate the prohibited items policy and or the restricted items policy may
                              be subject to the following actions</li>
                        </strong>
                     </ul>
                     <ol>
                        <li><span>1. </span>Suspension or termination of membership.</li>
                        <li><span>2. </span> Permanent blocking of access to the site.</li>
                        <li><span>3. </span> Reporting to Law Enforcement or Appropriate Authorities.</li>
                     </ol>
                  </section>
               </div>
               <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                  <h1 id="list-item-2" class="text-muted">Pets Policy</h1>
                  <section class="static-content-section">
                     <p>For use of our Site and other services, you confirm and declare that you shall not list or post
                        or provide information in relation to the sale or purchase or exchange of goods and services,
                        content or information that are illegal under the laws of the Republic of India and/or are not
                        permitted as per the prohibited items policy listed below.</p>
                  </section>
                  <div>
                     <p class="policy-sub-titles"></p>
                     <span>1.</span><strong>Prohibited Items Policy.</strong>
                     </p>
                  </div>
                  <section class="static-content-section">
                     <p class="policy-sub-titles">
                        <span>A. </span><strong> We specifically prohibit any listing or posting of classifieds or
                           information in relation to the following items:</strong>
                     </p>
                     <ul>
                        <li>Alcoholic Beverages, Liquor, tobacco products, drugs, psychotropic substances, narcotics,
                           intoxicants of any description, medicines, palliative/curative substances nor shall you
                           provide link directly or indirectly to or include descriptions of items, goods or services
                           that are prohibited under any applicable law for the time being in force including but not
                           limited to the Drugs and Cosmetics Act, 1940, the Drugs And Magic Remedies (Objectionable
                           Advertisements) Act, 1954 Narcotic Drug and Prohibited Substances Act and the Indian Penal
                           Code, 1860.</li>
                        <li>Living, dead person and/or the whole or any part of any human which has been kept or
                           preserved by any means whether artificial or natural including any blood, bodily fluids and/
                           or body parts</li>
                        <li>Prostitution or any other service in the nature there of that purports to violate the
                           provisions of Immoral Act or Indecent representation of women which violates the contemporary
                           standards of morality and decency in Indian society.</li>
                        <li>Religious items, including books, artifacts, etc. or any information, description of any
                           such item that is likely to affect the religious sentiments of any person or group</li>
                        <li>Mature Audiences Policy includes films which do not have a certificate for public exhibition
                           issued by the Central Board of Film Certification and or described and depict or otherwise
                           deal with matters which are revolting or repulsive and or tend to deprave a persons mind in
                           such a way that they tend to offend against the standards of morality, decency and propriety
                           generally accepted by reasonable adults</li>
                        <li>Obscene Items includes items which contain an indecent representation of women within the
                           meaning of the Indecent Representation of Women (Prohibition) Act, 1986; Any publication or
                           film or item that describes or depicts a minor who is, or who appears to be, under 18
                           (whether the minor is engaged in sexual activity or not) and any computer games not suitable
                           for minor that are unsuitable for a minor to see or play.</li>
                        <li>Offensive Material intended for use in a sexual setting (including "bondage" and "fetish"
                           items,) displaying sexual activity or portraying human genitalia in a "life-like" or
                           realistic fashion</li>
                        <li>Information that is fraudulent, misrepresenting as to the nature and use of the goods or the
                           services.</li>
                        <li>Counterfeit, Pirated and stolen goods or unauthorized illegal services (services for which
                           you are not licensed or permitted to do or do not have the authority to under take).</li>
                        <li>Items, goods and services that infringe or attempt to pass off any third parties
                           intellectual property or rights of publicity or moral rights and or purport's to breach any
                           persons right to privacy.</li>
                        <li>Electronically transmitting through any medium computer viruses of any type or any computer
                           program that facilitates hacking of a computer system which the intent to damage a computer
                           or computer network or intercept any personal data.</li>
                        <li>Your information shall not include any hate content, that is derogatory or slanderous in
                           nature that may directed to any individual or group or advocate violence against any users
                           individuals and or animals.</li>
                        <li>Hazardous chemicals and pesticides and/ or items in violation of Hazardous Chemicals Act,
                           1985.</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>B. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>"Securities" within the meaning of the Securities Contract Regulation Act, 1956, including
                           shares, bonds, debentures, etc. and/or any other financial instruments/assets of any
                           description</li>
                        <li>Living, dead creatures and/or the whole or any part of any animal which has been kept or
                           preserved by any means whether artificial or natural including rugs, skins, specimens of
                           animals, antlers, horns, hair, feathers, nails, teeth, musk, eggs, nests, other animal
                           products of any description the sale and purchase of which is prevented or restricted in any
                           manner by applicable laws (including those prohibited under The Wildlife Protection Act, 1972
                           and/ or The Environment Protection Act, 1986)</li>
                     </ul>
                     <p class="policy-sub-titles">
                        <span>C. </span><strong> Your listing, information, Advertisement</strong>
                     </p>
                     <ul>
                        <li>Shall not be defamatory, trade libelous, unlawfully threatening or unlawfully harassing.
                           Further shall not be fraudulent, misrepresenting, misleading or pertain to the sale of any
                           illegal, counterfeit, stolen goods and or services which do not belong to you or you do not
                           have the authority for. Further still shall not infringe any intellectual property, trade
                           secret, or other proprietary rights or rights of publicity or privacy of any third party.
                        </li>
                        <li>Shall not contain any viruses, Trojan horses, worms, time bombs, cancel bots, easter eggs or
                           other computer programming routines that may damage, detrimentally interfere with,
                           surreptitiously intercept or expropriate any system, data or personal information.</li>
                        <li>Shall not be allowed to libel anyone or include hate, derogatory, slanderous speech directed
                           at individuals or groups. You should not advocate violence against other users or individuals
                           or groups.</li>
                     </ul>
                     <div>
                        <span>2. </span><strong> In addition to the above and for the purposes of clarity all Users
                           shall be expected to adhere to and comply with the following Policies while listing of
                           items:</strong>
                     </div>
                     <ul>
                        <li><strong>Restricted Item Policy:</strong> In addition to the above prohibited items policy
                           users shall also adhere to and comply with the restricted items policy while listing, posting
                           or providing information in relation to any goods or services.</li>
                        <li><strong>Duplicate Ad listings are not allowed.</strong> Any ad posted more than once with
                           the same content or Title in the same city and category would be considered as a Duplicate
                           Ad. We advise you to post multiple ads only if you have different items or services for sale.
                           All duplicate ads would be deleted and posters penalized if the problem persists.</li>
                        <li><strong>Mature Audience/Sexually oriented material: </strong> relating to items that
                           includes items intended for use in sexual activity would not be permitted. (An example of
                           such classifieds relating to an item not normally permitted would be a classified for the
                           sale of a vibrator). Please also be aware that titles with graphic adult language are
                           inappropriate, regardless of the item contained in the listing itself.</li>
                        <div>
                     </ul>
                     <span>3.</span><strong>Consequences of Breach of Listing Policy</strong>
                     <ul>
                        <li><strong>Users who violate the prohibited items policy and or the restricted items policy may
                              be subject to the following actions</li>
                        </strong>
                     </ul>
                     <ol>
                        <li><span>1. </span>Suspension or termination of membership.</li>
                        <li><span>2. </span> Permanent blocking of access to the site.</li>
                        <li><span>3. </span> Reporting to Law Enforcement or Appropriate Authorities.</li>
                     </ol>
                  </section>
               </div>
            </div>
         </div>
   </section>
   <div class="mt-3"></div>
 <!-- FOOTER -->
<?php
include 'footer.php'
?>
   <script>
      $('.nav-pills > a').hover(function () {
         $(this).tab('show');
      });
   </script>
</body>

</html>