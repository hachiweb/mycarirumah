<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}  
    if(empty($_SESSION["username"])){
        header("location:index.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        include 'header.php';
    }
 
?>
<title>Profile</title>
<div class="border-top"></div>
<section class="container mt-2">
    <div class="row">
        <div class="col-sm-4">
            <div class="nav flex-column nav-pills border bg-white" id="v-pills-tab" role="tablist"
                aria-orientation="vertical">
                <a class="nav-link border-bottom <?php if(!isset($_POST['Category'])) echo "active";?>"
                    id="v-pills-Account-tab" data-toggle="pill" href="#v-pills-Account" role="tab"
                    aria-controls="v-pills-Account"
                    aria-selected="<?php if(isset($_POST['Category'])) echo "false"; else echo "true" ?>">My Account</a>
                <a class="nav-link border-bottom <?php if(isset($_POST['Category'])) echo "active";?>"
                    id="v-pills-Ads-tab" data-toggle="pill" href="#v-pills-Ads" role="tab" aria-controls="v-pills-Ads"
                    aria-selected="<?php if(isset($_POST['Category'])) echo "true"; else echo "false" ?>">My Ads</a>
                <a class="nav-link border-bottom " id="v-pills-Chat-tab" data-toggle="pill" href="#v-pills-Chat"
                    role="tab" aria-controls="v-pills-Chat" aria-selected="false">My Chat</a>
                <a class="nav-link border-bottom" id="v-pills-Payment-tab" data-toggle="pill" href="#v-pills-Payment"
                    role="tab" aria-controls="v-pills-Payment" aria-selected="false">Order & Payment Details</a>
                <a class="nav-link border-bottom" id="v-pills-Credits-tab" data-toggle="pill" href="#v-pills-Credits"
                    role="tab" aria-controls="v-pills-Credits" aria-selected="false">Ads Credits Details</a>
                <a class="nav-link border-bottom" id="v-pills-Verify-tab" data-toggle="pill" href="#v-pills-Verify"
                    role="tab" aria-controls="v-pills-Verify" aria-selected="false">Verify Your Account</a>
                <a class="nav-link border-bottom" id="v-pills-Education-tab" data-toggle="pill"
                    href="#v-pills-Education" role="tab" aria-controls="v-pills-Education"
                    aria-selected="false">Education Leads</a>
            </div>

        </div>
        <div class="col-sm-8">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade <?php if(!isset($_POST['Category'])) echo "show active";?>"
                    id="v-pills-Account" role="tabpanel" aria-labelledby="v-pills-Account-tab">
                    <div class="row border bg-white">
                        <?php
                        $db     = new DB();
                        $sql    = "SELECT * FROM `users` WHERE username='$username'"; 
                        $result = $db->executeQuery($sql);
                        if($result->num_rows > 0){ 
                        while ($fetch = mysqli_fetch_assoc($result)) { ?>
                        <div class="col-sm-2 pl-0 p-2">
                            <img src="<?=empty($fetch['picture'])?"assets/images/user1.png":$fetch['picture']; ?>"
                                style="border-radius:100px;" class="text-center" alt="">
                        </div>
                        <div class="col-sm-8 pl-0 mt-3">
                            <span class="h3 text-dark"><?=empty($fetch['alias'])?"Your Name":$fetch['alias']; ?></span>
                            <small>(<?=empty($fetch['city'])?"City":$fetch['city']; ?>)</small>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p><?=empty($fetch['phone'])?"Phone Number":$fetch['phone']; ?><br></p>
                                </div>
                                <div class="col-sm-4">
                                    <p><?=empty($fetch['username'])?"Your Email":$fetch['username']; ?><br></p>
                                </div>
                                <div class="col-sm-3">
                                    <p>
                                    </p>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                        </div>
                        <div class="col-sm-2 mt-4">
                            <button type="button" style="width:90%;" class="btn btn-outline-danger bdr_radius"
                                id="edit_button">Edit</button>
                        </div>
                    </div>
                    <form action="sub-edit-profile.php" method="POST">
                        <div class="row border bg-white mt-3 pb-4" style="display:none;" id="hide_form">
                            <div class="col-sm-2 mt-2">
                                <img src="<?=$fetch['picture']; ?>" style="width:70%;" alt="">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group mt-2">
                                    <label for="name">Name:</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                        value="<?=$fetch['alias']; ?>">
                                    <input type="hidden" name="id" class="form-control" id="id"
                                        value="<?=$fetch['id']; ?>">
                                    <input type="hidden" name="source" class="form-control" id="id" value="Profile">
                                </div>
                                <h6 class="font-weight-normal">CONTACT NUMBER</h6>
                                <div class="form-group mt-2">
                                    <label for="name">Number:</label>
                                    <input type="number" name="phone" class="form-control" id="number"
                                        value="<?=$fetch['phone']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Select Gender:</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <?php
                                            if ($fetch['gender'] == "Male") {
                                                echo "<option selected value='Male'>Male</option>
                                                <option value='Female'>Female</option>";
                                            } else {
                                                echo "<option value='Male'>Male</option>
                                                <option selected value='Female'>Female</option>";
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-4">
                                <div class="form-group mt-2">
                                    <label for="city">Select City:</label>
                                    <select class="form-control" name="city">
                                        <option selected value='<?=$fetch['city']; ?>'>
                                            <?php if (empty($fetch['city'])) {
                                                    echo "Select City";
                                                } else {
                                                    echo $fetch['city'];
                                                }
                                                ?>
                                        </option>
                                        <option value='Delhi'>Delhi</option>
                                        <option value='Mumbai'>Mumbai</option>
                                        <option value='Bangalore'>Bangalore</option>
                                        <option value='Hyderabad'>Hyderabad</option>
                                        <option value='Ahmedabad'>Ahmedabad</option>
                                        <option value='Chennai'>Chennai</option>
                                        <option value='Kolkata'>Kolkata</option>
                                        <option value='Dehradun'>Dehradun</option>
                                    </select>
                                </div>
                                <h6 class="font-weight-normal">EMAIL ADDRESSES</h6>
                                <div class="form-group mt-2">
                                    <label for="emal">Primary:</label>
                                    <input type="email" name="email" class="form-control" id="email"
                                        value="<?=$fetch['username']; ?>">
                                </div>
                                <div class="form-group mt-2">
                                    <label for="age">Age:</label>
                                    <input type="number" name="age" class="form-control" id="age"
                                        value="<?=$fetch['age'];?>">
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2">
                                <button type="submit" style="width:130px;" class="btn btn-primary">Update
                                    Profile</button>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" style="width:80px;margin-left:20px;"
                                    class="btn btn-outline-primary" id="cancle">cancel</button>
                            </div>
                            <div class="col-sm-2">

                                <button type="button" style="width:160px;" class="btn btn-outline-primary">Change
                                    Password</button>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </form>
                    <div class="row border bg-white my-2 text-dark">
                        <?php
                            if ($fetch) {
                                $fetch['house'] = empty($fetch['house'])?"House":$fetch['house'];
                                $fetch['address_type'] = empty($fetch['address_type'])?"Address":$fetch['address_type'];
                                $fetch['locality'] = empty($fetch['locality'])?"Locality":$fetch['locality'];
                                $fetch['landmark'] = empty($fetch['landmark'])?"Landmark":$fetch['landmark'];
                                $fetch['city'] = empty($fetch['city'])?"City":$fetch['city'];
                                $fetch['pincode'] = empty($fetch['pincode'])?"Pincode":$fetch['pincode'];
                            ?>
                        <div class="col-sm-6 my-3">
                            <span class="h5"><?=$fetch['address_type'];?><br>
                                <p><?=$fetch['house'].", ".$fetch['locality'].", ".$fetch['landmark'].", ".$fetch['city'].", ".$fetch['pincode'];?>
                                </p>
                            </span>
                        </div>
                        <?php
                            }
                            ?>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-2 mt-4">
                            <button type="button" style="width:90%;" class="btn btn-outline-danger bdr_radius"
                                id="update_address">Edit</button>
                        </div>
                    </div>
                    <form action="sub-edit-profile.php" method="POST">
                        <div class="row border bg-white mt-4 pb-4" style="display:none" id="address_show">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-5">
                                <h6 class="font-weight-normal mt-3">Add Address</h6>
                                <div class="form-group mt-4">
                                    <label for="pincode">Pincode:</label>
                                    <input type="number" name="pincode" class="form-control" id="Pincode"
                                        value="<?=$fetch['pincode']; ?>">
                                    <input type="hidden" name="id" class="form-control" id="id"
                                        value="<?=$fetch['id']; ?>">
                                </div>
                                <div class="form-group mt-2">
                                    <label for="Adderess">Adderess or Area / Street*</label>
                                    <input type="text" name="locality" class="form-control" id="Adderess"
                                        value="<?=$fetch['locality']; ?>">
                                </div>
                                <div class="form-group mt-2">
                                    <label for="landmark">Landmark*</label>
                                    <input type="text" name="landmark" class="form-control" id="landmark"
                                        value="<?=$fetch['landmark']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-0"></div>
                            <div class="col-sm-5">
                                <div class="form-group" style="margin-top:56px;">
                                    <label for="city">Select City:</label>
                                    <select class="form-control" name="city">
                                        <option selected value='<?=$fetch['city']; ?>'>
                                            <?php if (empty($fetch['city'])) {
                                                echo "Select City";
                                            } else {
                                                echo $fetch['city'];
                                            }
                                            ?>
                                        </option>
                                        <option value='Delhi'>Delhi</option>
                                        <option value='Mumbai'>Mumbai</option>
                                        <option value='Bangalore'>Bangalore</option>
                                        <option value='Hyderabad'>Hyderabad</option>
                                        <option value='Ahmedabad'>Ahmedabad</option>
                                        <option value='Chennai'>Chennai</option>
                                        <option value='Kolkata'>Kolkata</option>
                                        <option value='Dehradun'>Dehradun</option>
                                    </select>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="House">House/Flat/Society Details*</label>
                                    <input type="text" name="house" class="form-control" id="House"
                                        value="<?=$fetch['house']; ?>">
                                </div>
                                <h6 class="font-weight-normal mt-3">Save As</h6>
                                <div class="form-check-inline mt-2">
                                    <label class="form-check-label">
                                        <input <?php if($fetch["address_type"] == "Home"){ echo "checked"; } ?>
                                            type="radio" class="form-check-input" name="address_type" value="Home"
                                            id="Home">Home
                                    </label>
                                </div>
                                <div class="form-check-inline mt-2">
                                    <label class="form-check-label">
                                        <input <?php if($fetch["address_type"] == "Office"){ echo "checked"; } ?>
                                            type="radio" class="form-check-input" name="address_type" value="Office"
                                            id="Office">Office
                                    </label>
                                </div>
                                <div class="form-check-inline mt-2">
                                    <label class="form-check-label">
                                        <input <?php if($fetch["address_type"] == "Other"){ echo "checked"; } ?>
                                            type="radio" class="form-check-input" name="address_type" value="Other"
                                            id="Other">Other
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-3 mt-3">
                                <button type="submit" style="width:130px;" class="btn btn-primary">Update</button>
                            </div>
                            <div class="col-sm-3 mt-3">
                                <button type="button" style="width:130px;" class="btn btn-outline-primary"
                                    id="cancle1">Cancel</button>
                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-3"></div>
                        </div>
                    </form>
                    <div class="row border bg-white py-3">
                        <?php
                            $db     = new DB(); 
                            $sql2    = "SELECT * FROM `postadform` WHERE (`username` = '$username') AND `is_active` =1";
                            $result2 = $db->executeQuery($sql2);
                            $sql3    = "SELECT * FROM `jobpostform` WHERE (`username` = '$username') AND `is_active` =1";
                            $result3 = $db->executeQuery($sql3);
                            $sql4    = "SELECT * FROM `postfreeadform` WHERE (`username` = '$username') AND `is_active` =1";
                            $result4 = $db->executeQuery($sql4);
                            $activeads = $result2->num_rows+$result3->num_rows+$result4->num_rows;
                            $sql5    = "SELECT * FROM `postadform` WHERE (`username` = '$username') AND `is_active` =0";
                            $result5 = $db->executeQuery($sql5);
                            $sql6    = "SELECT * FROM `jobpostform` WHERE (`username` = '$username') AND `is_active` =0";
                            $result6 = $db->executeQuery($sql6);
                            $sql7    = "SELECT * FROM `postfreeadform` WHERE (`username` = '$username') AND `is_active` =0";
                            $result7 = $db->executeQuery($sql7);
                            $inactiveads = $result5->num_rows+$result6->num_rows+$result7->num_rows;
                            ?>
                        <div class="col-sm-4">
                            <div class="border bg-white">
                                <h5 class="text-center border-bottom px-4 py-4">My Free Ads</h5>
                                <p
                                    style="height:70px;width:70px;background-color:yellow;border-radius:50%;color:red;margin-left:70px;margin-top:50px;font-size:25px; padding-left:30px;padding-top:17px;">
                                    <?=$activeads+$inactiveads ;?></p>
                                <div class="border-bottom" style="margin-top:30%;">
                                    <p class="d-inline ml-2">My Active Ads</p>
                                    <span class="float-right mr-2"><?=$activeads ;?></span>
                                </div>
                                <div class="border-bottom" style="margin-top:10%;">
                                    <p class="d-inline ml-2">My Expired Ads</p>
                                    <span class="float-right mr-2"><?=$inactiveads ;?></span>
                                </div>
                                <div class="border-bottom text-center" style="margin-top:6%;">
                                    <button type="button" style="width:80%;" class="btn btn-success mb-2"
                                        onclick="document.getElementById('v-pills-Ads-tab').click();">Manage My
                                        Ads</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="border bg-white">
                                <h5 class="text-center border-bottom px-4 py-4">My Premium Ads</h5>
                                <p
                                    style="height:70px;width:70px;background-color:yellow;border-radius:50%;color:red;margin-left:70px;margin-top:50px;font-size:25px; padding-left:30px;padding-top:17px;">
                                    0</p>
                                <div class="border-bottom" style="margin-top:30%;">
                                    <p class="d-inline ml-2"><a class="text-decoration-none" href="#">My Payment
                                            Details</a></p>
                                </div>
                                <div class="border-bottom" style="margin-top:10%;">
                                    <p class="d-inline ml-2"><a class="text-decoration-none" href="#">My Ad Credit
                                            Details</a></p>
                                </div>
                                <div class="border-bottom text-center" style="margin-top:6%;">
                                    <button type="button" class="btn btn-primary btn-long mb-2"
                                        onclick="document.getElementById('v-pills-Ads-tab').click();">Manage Premium
                                        Ads</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="border bg-white">
                                <h5 class="text-center border-bottom px-4 py-4">Total Active Packs</h5>
                                <p
                                    style="height:70px;width:70px;background-color:yellow;border-radius:50%;color:red;margin-left:70px;margin-top:50px;font-size:25px; padding-left:30px;padding-top:17px;">
                                    0</p>
                                <div class="border-bottom" style="margin-top:30%;">
                                    <p class="d-inline ml-2">Total Credits</p>
                                    <span class="float-right mr-2">0</span>
                                </div>
                                <div class="border-bottom" style="margin-top:10%;">
                                    <p class="d-inline ml-2">Credits Remaining</p>
                                    <span class="float-right mr-2">0</span>
                                </div>
                                <div class="border-bottom text-center" style="margin-top:6%;">
                                    <a href="purchase_ad_credit.php" style="width:80%;" class="btn btn-info mb-2">Buys
                                        Ads
                                        Credits</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <?php
                    }
                }
                ?>
                </div>
                <div class="tab-pane fade <?php if(isset($_POST['Category'])) echo "show active";?>" id="v-pills-Ads"
                    role="tabpanel" aria-labelledby="v-pills-Ads-tab">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">Active</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Inactive</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="container tab-pane active bg-white"><br>
                            <?php if ($activeads>0) { ?>
                            <div class="dropdown">
                                <form action="" method="POST">
                                    <?php
                                    $subcategory = isset($_POST['Category'])?$_POST['Category']:'160';
                                    $db = new DB();
                                    $sql = "SELECT * FROM `category` WHERE `parent_id` = 0";
                                    $result = $db->executeQuery($sql);
                                    ?>
                                    <select onchange="this.form.submit()" name="Category" class="custom-select mb-3">
                                        <?php
                                            while ($fetch = mysqli_fetch_assoc($result)) {
                                            if($subcategory == $fetch['id']){
                                                echo '<option value="'.$fetch['id'].'" selected>'.$fetch['category_title'].'</option>';
                                            }else{
                                                echo '<option value="'.$fetch['id'].'">'.$fetch['category_title'].'</option>';
                                            }
                                            }
                                            ?>
                                    </select>
                                </form>
                                <div class="container">
                                    <?php
                                        $id = isset($_POST["Category"])?$_POST["Category"]:'160';
                                        $db = new DB();
                                        $sql = "SELECT * FROM `category` WHERE `id` = $id"; 
                                        $result = $db->executeQuery($sql);
                                        while ($fetch = mysqli_fetch_assoc($result)) {
                                            if($id == $fetch['id']){ ?>
                                    <p style="color:black;">Showing results for
                                        <strong><?=$fetch['category_title']; ?></strong></p>
                                    <?php
                                            }
                                        }
                                        $db = new DB();
                                        $id = isset($_POST["Category"])?$_POST["Category"]:'160';
                                        if($id=='6'){
                                            $sql1    = "SELECT * FROM `jobpostform` WHERE `username` = '$username' AND `is_active` =1 ORDER BY `id` DESC"; 
                                            $result1 = $db->executeQuery($sql1);
                                            echo '<div class="row">';
                                            while ($fetch1 = mysqli_fetch_assoc($result1)) {
                                                echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                echo '<div class="border exshadow px-3">';
                                                echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch1['job_title'] . " - " . $fetch1['company_name'] . "</strong></h6>";
                                                echo "<a><strong>Salary:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['salary_min'] . " - " . $fetch1['salary_max'] . "</a><br>";
                                                echo "<a><strong>Job Type:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['job_type'] . "</a><br>";
                                                echo "<a><strong>Compony:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['company_name'] . "</a><br>";
                                                echo "<a><strong>Experience:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['experience_min'] . " - " . $fetch1['experience_max'] . "</a><br>";
                                                echo "<a><strong>Role:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['role'] . "</a><br>";
                                                echo "<a><strong>Location:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['city'] . " - " . $fetch1['locality'] . "</a><br>";
                                                echo "<a><strong>Contact Number:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['number'] . "</a><br>";
                                                echo "<a><strong>Email address:&nbsp;</strong></a>";
                                                echo "<a>" . $fetch1['email'] . "</a><br>"; ?>
                                    <a href="ad_details.php?A=<?=$fetch1['id']?>&P=6">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                echo '</div>';
                                                echo '</div>';
                                            }
                                            echo '</div>';
                                        }
                                        elseif ($id=='1' or $id=='2' or $id=='3' or $id=='4' or $id=='5'){
                                            $sql2    = "SELECT * FROM `postadform` WHERE parent_id = $id AND  `username` = '$username' AND `is_active` =1 ORDER BY `id` DESC";
                                            $result2 = $db->executeQuery($sql2);
                                            echo '<div class="row">';
                                                while ($fetch2 = mysqli_fetch_assoc($result2)) {
                                                    echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                    echo '<div class="border exshadow px-3">';
                                                    echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch2['title'] ."</strong></h6>";
                                                    echo "<a><strong>Condition:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['condition'] . "</a><br>";
                                                    echo "<a><strong>Price:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['price'] . "</a><br>";
                                                    echo "<a><strong>Mobile:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['mobile'] . "</a><br>";
                                                    echo "<a><strong>Email:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['email'] . "</a><br>";
                                                    echo "<a><strong>Pincode:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['pincode'] . "</a><br>";
                                                    echo "<a><strong>Saler Type:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['saler_type'] . "</a><br>";
                                                    echo "<a><strong>GST Number:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['gst_number'] . "</a><br>";
                                                    echo "<a><strong>Product Material:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['product_material'] . "</a><br>";
                                                    echo "<a><strong>Quantity:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['quantity'] . "</a><br>";
                                                    echo "<a><strong>Brand:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['brand'] . "</a><br>";
                                                    echo "<a><strong>Description:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch2['description'] . "</a><br>";
                                                        if (empty($fetch2['imgname'])) {
                                                            # code...
                                                        } else {
                                                            echo "<a><strong>Product Images:&nbsp;</strong></a>"; ?>
                                    <img src="<?php echo "upload/".$fetch2['imgname']; ?>" alt="Image">
                                    <?php
                                                        }
                                                        ?>
                                    <a href="ad_details.php?A=<?=$fetch2['id']?>&P=<?=$fetch2['parent_id']?>">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                        echo '</div>';
                                                        echo '</div>';
                                                }
                                            echo '</div>';
                                            }
                                            else{
                                                $sql3    = "SELECT * FROM `postfreeadform` WHERE parent_id = $id AND  `username` = '$username' AND `is_active` =1 ORDER BY `id` DESC";
                                                    $result3 = $db->executeQuery($sql3);
                                                    echo '<div class="row">';
                                                    while ($fetch3 = mysqli_fetch_assoc($result3)) {
                                                        echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                        echo '<div class="border exshadow px-3">';
                                                        echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch3['title'] ."</strong></h6>";
                                                        echo "<a><strong>Type:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['education'] . "</a><br>";
                                                        echo "<a><strong>Board:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['board'] . "</a><br>";
                                                        echo "<a><strong>Subject:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['subject'] . "</a><br>";
                                                        echo "<a><strong>Standard:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['standard'] . "</a><br>";
                                                        echo "<a><strong>Delivery Mode:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['delivery_mode'] . "</a><br>";
                                                        echo "<a><strong>Fees:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['fees'] . "</a><br>";
                                                        echo "<a><strong>Institute Name:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['institute_name'] . "</a><br>";
                                                        echo "<a><strong>Institute Adderess:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['institute_adderess'] . "</a><br>";
                                                        echo "<a><strong>Institute Website:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['institute_website'] . "</a><br>";
                                                        echo "<a><strong>Discount Percentage:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['discount_percentage'] . "</a><br>";
                                                        echo "<a><strong>Description:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['description'] . "</a><br>";
                                                        echo "<a><strong>Locality:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['locality'] . "</a><br>";
                                                        echo "<a><strong>Name:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['name'] . "</a><br>";
                                                        echo "<a><strong>Email:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['email'] . "</a><br>";
                                                        echo "<a><strong>Contact:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch3['mobile'] . "</a><br>";
                                                            if (empty($fetch3['imgname'])) {
                                                                # code...
                                                            } else {
                                                                echo "<a><strong>Product Images:&nbsp;</strong></a>"; ?>
                                    <img src="<?php echo "upload/".$fetch3['imgname']; ?>" alt="Image">
                                    <?php
                                                            }?>
                                    <a href="ad_details.php?A=<?=$fetch3['id']?>&P=<?=$fetch3['parent_id']?>">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                            echo '</div>';
                                                            echo '</div>';
                                                    }
                                                    echo '</div>';
                                                }
                                                ?>
                                </div>
                            </div>
                            <?php
                            } else{ ?>
                            <img style="width:60%;margin-left:30%;" src="assets/images/active.png" alt="file not found">
                            <p style="margin-left:40%;">Oops! No Ads to display<br>
                                You do not have any Active ads, to post an ad <a href="#">click here</a></p>
                            <?php
                            }
                            ?>
                        </div>
                        <div id="menu1" class="container tab-pane fade bg-white"><br>
                            <?php if ($inactiveads>0) { ?>
                            <div class="dropdown">
                                <form action="" method="POST">
                                    <?php
                                            $subcategory = isset($_POST['Category'])?$_POST['Category']:'160';
                                            $db = new DB();
                                            $sql = "SELECT * FROM `category` WHERE `parent_id` = 0";
                                            $result = $db->executeQuery($sql);
                                            ?>
                                    <select onchange="this.form.submit()" name="Category" class="custom-select mb-3">
                                        <?php
                                        while ($fetch = mysqli_fetch_assoc($result)) {
                                            if($subcategory == $fetch['id']){
                                                echo '<option value="'.$fetch['id'].'" selected>'.$fetch['category_title'].'</option>';
                                            }else{
                                                echo '<option value="'.$fetch['id'].'">'.$fetch['category_title'].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </form>
                                <div class="container">
                                    <?php
                                            $id = isset($_POST["Category"])?$_POST["Category"]:'160';
                                            $db = new DB();
                                            $sql = "SELECT * FROM `category` WHERE `id` = $id"; 
                                            $result = $db->executeQuery($sql);
                                            while ($fetch = mysqli_fetch_assoc($result)) {
                                                if($id == $fetch['id']){ ?>
                                    <p style="color:black;">Showing results for
                                        <strong><?=$fetch['category_title']; ?></strong></p>
                                    <?php
                                                }
                                            }
                                            $db = new DB();
                                            $id = isset($_POST["Category"])?$_POST["Category"]:'160';
                                            if($id=='6'){
                                                $sql1    = "SELECT * FROM `jobpostform` WHERE `username` = '$username' AND `is_active` =0 ORDER BY `id` DESC"; 
                                                $result1 = $db->executeQuery($sql1);
                                                echo '<div class="row">';
                                                while ($fetch1 = mysqli_fetch_assoc($result1)) {
                                                    echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                    echo '<div class="border exshadow px-3">';
                                                    echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch1['job_title'] . " - " . $fetch1['company_name'] . "</strong></h6>";
                                                    echo "<a><strong>Salary:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['salary_min'] . " - " . $fetch1['salary_max'] . "</a><br>";
                                                    echo "<a><strong>Job Type:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['job_type'] . "</a><br>";
                                                    echo "<a><strong>Compony:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['company_name'] . "</a><br>";
                                                    echo "<a><strong>Experience:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['experience_min'] . " - " . $fetch1['experience_max'] . "</a><br>";
                                                    echo "<a><strong>Role:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['role'] . "</a><br>";
                                                    echo "<a><strong>Location:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['city'] . " - " . $fetch1['locality'] . "</a><br>";
                                                    echo "<a><strong>Contact Number:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['number'] . "</a><br>";
                                                    echo "<a><strong>Email address:&nbsp;</strong></a>";
                                                    echo "<a>" . $fetch1['email'] . "</a><br>"; ?>
                                    <a href="ad_details.php?A=<?=$fetch1['id']?>&P=6">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                    echo '</div>';
                                                    echo '</div>';
                                                }
                                                echo '</div>';
                                            }
                                            elseif ($id=='1' or $id=='2' or $id=='3' or $id=='4' or $id=='5'){
                                                $sql2    = "SELECT * FROM `postadform` WHERE parent_id = $id AND  `username` = '$username' AND `is_active` =0 ORDER BY `id` DESC";
                                                $result2 = $db->executeQuery($sql2);
                                                echo '<div class="row">';
                                                    while ($fetch2 = mysqli_fetch_assoc($result2)) {
                                                        echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                        echo '<div class="border exshadow px-3">';
                                                        echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch2['title'] ."</strong></h6>";
                                                        echo "<a><strong>Condition:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['condition'] . "</a><br>";
                                                        echo "<a><strong>Price:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['price'] . "</a><br>";
                                                        echo "<a><strong>Mobile:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['mobile'] . "</a><br>";
                                                        echo "<a><strong>Email:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['email'] . "</a><br>";
                                                        echo "<a><strong>Pincode:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['pincode'] . "</a><br>";
                                                        echo "<a><strong>Saler Type:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['saler_type'] . "</a><br>";
                                                        echo "<a><strong>GST Number:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['gst_number'] . "</a><br>";
                                                        echo "<a><strong>Product Material:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['product_material'] . "</a><br>";
                                                        echo "<a><strong>Quantity:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['quantity'] . "</a><br>";
                                                        echo "<a><strong>Brand:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['brand'] . "</a><br>";
                                                        echo "<a><strong>Description:&nbsp;</strong></a>";
                                                        echo "<a>" . $fetch2['description'] . "</a><br>";
                                                            if (empty($fetch2['imgname'])) {
                                                                # code...
                                                            } else {
                                                                echo "<a><strong>Product Images:&nbsp;</strong></a>"; ?>
                                    <img src="<?php echo "upload/".$fetch2['imgname']; ?>" alt="Image">
                                    <?php
                                                            }
                                                            ?>
                                    <a href="ad_details.php?A=<?=$fetch2['id']?>&P=<?=$fetch2['parent_id']?>">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                            echo '</div>';
                                                            echo '</div>';
                                                    }
                                                echo '</div>';
                                                }
                                                else{
                                                    $sql3    = "SELECT * FROM `postfreeadform` WHERE parent_id = $id AND  `username` = '$username' AND `is_active` =0 ORDER BY `id` DESC";
                                                        $result3 = $db->executeQuery($sql3);
                                                        echo '<div class="row">';
                                                        while ($fetch3 = mysqli_fetch_assoc($result3)) {
                                                            echo '<div class="col-sm-6 col-md-6 mb-4">';
                                                            echo '<div class="border exshadow px-3">';
                                                            echo "<h6 class='mt-2' style=color:dodgerblue><strong>" . $fetch3['title'] ."</strong></h6>";
                                                            echo "<a><strong>Type:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['education'] . "</a><br>";
                                                            echo "<a><strong>Board:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['board'] . "</a><br>";
                                                            echo "<a><strong>Subject:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['subject'] . "</a><br>";
                                                            echo "<a><strong>Standard:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['standard'] . "</a><br>";
                                                            echo "<a><strong>Delivery Mode:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['delivery_mode'] . "</a><br>";
                                                            echo "<a><strong>Fees:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['fees'] . "</a><br>";
                                                            echo "<a><strong>Institute Name:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['institute_name'] . "</a><br>";
                                                            echo "<a><strong>Institute Adderess:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['institute_adderess'] . "</a><br>";
                                                            echo "<a><strong>Institute Website:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['institute_website'] . "</a><br>";
                                                            echo "<a><strong>Discount Percentage:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['discount_percentage'] . "</a><br>";
                                                            echo "<a><strong>Description:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['description'] . "</a><br>";
                                                            echo "<a><strong>Locality:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['locality'] . "</a><br>";
                                                            echo "<a><strong>Name:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['name'] . "</a><br>";
                                                            echo "<a><strong>Email:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['email'] . "</a><br>";
                                                            echo "<a><strong>Contact:&nbsp;</strong></a>";
                                                            echo "<a>" . $fetch3['mobile'] . "</a><br>";
                                                                if (empty($fetch3['imgname'])) {
                                                                    # code...
                                                                } else {
                                                                    echo "<a><strong>Product Images:&nbsp;</strong></a>"; ?>
                                    <img src="<?php echo "upload/".$fetch3['imgname']; ?>" alt="Image">
                                    <?php
                                                                }?>
                                    <a href="ad_details.php?A=<?=$fetch3['id']?>&P=<?=$fetch3['parent_id']?>">
                                        <button type=button class="btn btn-primary mb-10" style="float:right">View
                                            Now</button><br><br></a>
                                    <?php
                                                                echo '</div>';
                                                                echo '</div>';
                                                        }
                                                        echo '</div>';
                                                    }
                                                    ?>
                                </div>
                            </div>
                            <?php
                                } else{ ?>
                            <img style="width:60%;margin-left:15%;" src="assets/images/inactive.png"
                                alt="file not found">
                            <p style="margin-left:40%;">Oops! No Ads to display<br>
                                You do not have any InActive ads</p>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-Chat" role="tabpanel" aria-labelledby="v-pills-Chat-tab">
                    <h4 id="list-item-2" class="text-muted d-inline">My Chats & Replies</h4>
                    <div class="dropdown float-right">
                        <button type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown">shoukeenrao@gmail.com</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Link 1</a>
                            <a class="dropdown-item" href="#">Link 2</a>
                            <a class="dropdown-item" href="#">Link 3</a>
                        </div>
                    </div>
                    <h6 class="text-center mt-5 text-muted">No Chats are available for this account</h6>
                </div>
                <div class="tab-pane fade" id="v-pills-Payment" role="tabpanel" aria-labelledby="v-pills-Payment-tab">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">BAZAAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Pick Up Drop Off</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu2">HOMES</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu3">Cars & Bikes</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="container tab-pane bg-white active pb-3"><br>
                            <img style="width:40%;margin-left:30%;" src="assets/images/order_payment.png"
                                alt="file not found">
                            <p style="margin-left:38%;">Oops! No Order placed yet.</p>
                        </div>
                        <div id="menu1" class="container tab-pane fade bg-white pb-3"><br>
                            <img style="width:40%;margin-left:30%;" src="assets/images/order_payment.png"
                                alt="file not found">
                            <p style="margin-left:38%;">Oops! No Order placed yet.</p>
                        </div>
                        <div id="menu2" class="container tab-pane fade bg-white pb-3"><br>

                            <img style="width:40%;margin-left:30%;" src="assets/images/order_payment.png"
                                alt="file not found">
                            <p style="margin-left:38%;">Oops! No Order placed yet.</p>
                        </div>
                        <div id="menu3" class="container tab-pane fade bg-white pb-3"><br>

                            <img style="width:40%;margin-left:30%;" src="assets/images/order_payment.png"
                                alt="file not found">
                            <p style="margin-left:38%;">Oops! No Order placed yet.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-Credits" role="tabpanel" aria-labelledby="v-pills-Credits-tab">
                    <div class="border">
                        <p class="mt-2">No Ad Credit Deatils are available for this account. <a
                                href="purchase_ad_credit.php">Buy Ads pack</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include 'footer.php'
?>

<?php
if (isset($_GET['BySideBar'])) {
    ?>
<script>
document.getElementById('v-pills-Ads-tab').click();
</script>
<?php
}
?>
<script>
$(document).ready(function() {
    $("#edit_button").click(function() {
        $("#hide_form").show('slow');
    });
    $("#cancle").click(function() {
        $("#hide_form").hide('slow');
    });
    $("#update_address").click(function() {
        $("#address_show").show('slow');
    });
    $("#cancle1").click(function() {
        $("#address_show").hide('slow');
    });
});
</script>
</body>

</html>