$(document).ready(function(){
  $(".signout").click(function(){
    signOut();
  }); 
  /** 
	Preview image
	*/
	


  //end document
});
  function reloadPage(){
    img_src = $("#sidebar_profile_pic").attr("src");
    img_src = img_src.trim();
    if(img_src=='assets/images/user1.png')
    {
      location.reload(true);
    }
  }
    function saveData(userData){
        console.log(userData);
        $.ajax({
            type: "POST",
            url: "user-data.php",
            data: userData,
            cache: false,
            success: function(data){
               console.log(data);
               $("#signin-success").val(1);
            },
            fail: function(xhr, textStatus, errorThrown){
                alert('textStatus');
             }
          });
    }
    window.googleSignin = false;
    function onSignIn(googleUser)
    {
        window.googleSignin = true;
        var profile=googleUser.getBasicProfile();
        $(".g-signin2").css("display","none");
        $(".data").css("display","block");
        $(".data2").css("display","none");
        $("#pic").attr('src',profile.getImageUrl());
        $("#email").text(profile.getEmail());
        $("#pic1").attr('src',profile.getImageUrl());
        $("#email1").text(profile.getEmail());
        $("#name").text(profile.getName());
        userData = {name:profile.getName(),oauth_provider:'Google',id:profile.Eea,email:profile.getEmail(),picture:profile.getImageUrl()}
        // console.log(userData);
        saveData(userData);
        document.getElementById("close").click();
        reloadPage();
    }
    function signOut()
    {
        if ( window.custome_login == true && window.googleSignin != true) {
          window.custome_login = false;
          window.location.assign("logout.php");
        }
         else{
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function(){
              alert("You have been succesfully signed out");
              $(".g-signin2").css("display","block");
              $(".data").css("display","none");
              $(".data2").css("display","block");
              window.location.assign("logout.php");
          });
          
         }
    }
    function customLogin() {
             
      
      data = {
           name : $("#customUser").val(),
           pswd : $("#customPswd").val()
      };
      $.ajax({
          method: "POST",
          url: "custom-login.php",
          data: data
          })
         .done(function( res ) {
           var response = JSON.parse(res);
        //  alert( "Data Saved: " + response.status );
        if(response.status == "200")
        {
         
         $(".g-signin2").css("display","none");
         $(".data").css("display","block");
         $(".data2").css("display","none");
        //  $("#pic").attr('src',profile.getImageUrl());
         $("#email").text(response.email);
        //  $("#pic1").attr('src',profile.getImageUrl());
         $("#email1").text(response.email);
         $("#name").text(response.name);
               
        }
        if(response.status == "404")
        {
         alert("Username or password is inncorrect");
               
        }
        if(response.status == "400")
        {
         alert("Please verify your Email");
               
        }
          }).fail(function() {
              alert( "something went wrong" );
            })
    
    return false;
    }
    function is_customRegValidation() {
        var name = $("#reg-name").val();
        var email = $("#reg-email").val();
        var phone = $("#reg-phone").val();
        var pswd = $("#reg-password").val();
        var conPswd = $("#reg-confirm-password").val();
    
       
    
        if (email == "" || phone == "" || pswd == "" || conPswd == "" || name == "") {
       alert("All fields are require");
          return false;
        }
        // if (!(/^[a-zA-Z\-]+$/.test(name)))
        // {
        //   alert( "Only Character are allowed in Username");
        //   return false;
        // }
        else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))
        {
       alert("Please Enter a Valid Email Format");
          return false;
        }
        else if (!(/^[0-9]*$/.test(phone)))
        {
          alert( "Phone Number must be Numeric");
          return false;
        }
        else if (!(/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/.test(pswd)))
        {
            alert("Password must be at least 8 characters and must containat at least one lower case letter,one upper and one digit");
          return false;
        }
        else if (pswd != conPswd) {
        
              alert("You entered a different password in password confirmation box");
          return false;
        }
        else {
        return true;
        }
        }
    
         function customReg() {
             
            if (is_customRegValidation()) {
                data = {
                     name : $("#reg-name").val(),
                     email : $("#reg-email").val(),
                     phone : $("#reg-phone").val(),
                     pswd : $("#reg-password").val()
                };
                $.ajax({
                    method: "POST",
                    url: "custom-reg.php",
                    data: data
                    })
                   .done(function( res ) {
                     var response = JSON.parse(res);
                  //  alert( "Data Saved: " + response.status );
                  if(response == 200)
                  {
                    alert("We sent a verification link to your gmail");
                         
                  }
                  if(response == 422)
                  {
                   alert("This email already exists");
                         
                  }
                  if(response == 423)
                  {
                   alert("Please choose a different username");
                         
                  }
                  if(response == 500)
                  {
                   alert("internal server error");
                         
                  }
                    }).fail(function() {
                        alert( "something went wrong" );
                      })
            }
            return false;
         }